package ej5;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		/*
		 * Haz un programa que lea un carácter por la 
		 * entrada estándar y me diga si es una
		   vocal o no
		 */
		
		Scanner sc = new Scanner(System.in);
		
		char x;
		
		System.out.println("Introduce un caracter: ");
		x = sc.next().charAt(0);
		
		if(x == 'a' || x == 'e' || x == 'i' || x == 'o' || x == 'u') {
			System.out.println("Es una vocal");
		}else {
			System.out.println("No es una vocal");
		}

	}

}
