package ej6;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		/*
		 *  Haz un programa que a partir del precio de un 
		 *  producto y un precio máximo, visualice el precio 
		 *  total del producto con el descuento de un 10% 
		 *  si supera ese precio máximo o un 5% de descuento 
		 *  en caso contrario. El precio del producto y
			precio máximo tienen que ser precios correctos.
		 */
		
		 float precioProd;
		 float precioMax = 20.00f;
		 float desc,res;
		 int porcentajeDesc = 5;
		 DecimalFormat df = new DecimalFormat("0.00");
		 
		 Scanner sc = new Scanner(System.in);
		 System.out.println("Precio del producto: ");
		 precioProd = sc.nextFloat();
		 
		 if(precioProd > precioMax) {
			 porcentajeDesc = 10;
		 }
		 
		 desc = precioProd * porcentajeDesc / 100;
		 res = precioProd - desc;
		 
		 System.out.printf("Tras aplicarle un "+porcentajeDesc+" por ciento el producto vale: "+df.format(res));

	}

}
