package ej2;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int tabla;
		
		System.out.print("Tabla de multiplicar: ");
		tabla = sc.nextInt();
		
		for(int i=10; i>=0; i--) {
			System.out.println(tabla+" x "+i+" = "+tabla*i);
		}

	}

}
