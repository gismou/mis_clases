package ej3;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		/*
		 *  Realizar un programa 
		 *  que lea 3 números y nos diga cuál es el mayor
		 */
		
		Scanner sc = new Scanner(System.in);
		int n1,n2,n3,mayor;
		
		System.out.print("Num 1: ");
		n1 = sc.nextInt();
		
		System.out.print("Num 2: ");
		n2 = sc.nextInt();

		System.out.print("Num 3: ");
		n3 = sc.nextInt();
		
		if(n1 > n2) {
			mayor = n1;
		}else {
			mayor = n2;
		}
		
		if(n3 > mayor) {
			mayor = n3;
		}

		
		System.out.println("De entre el "+n1+", "+n2+" y "+n3+" el mayor es el "+mayor);
		
	}

}
