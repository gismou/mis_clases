package ej4;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		/*
		 * Haz un programa que nos diga si dos números 
		 * enteros son divisibles.
		 */
		
		Scanner sc = new Scanner(System.in);
		
		int n1,n2;
		
		System.out.println("Numero 1: ");
		n1 = sc.nextInt();
		
		System.out.println("Numero 2: ");
		n2 = sc.nextInt();
		
		if(n1 % n2 == 0) {
			System.out.println("Son divisibles");
		}else {
			System.out.println("No son divisibles");
		}
	}

}
