package ej1;

import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		/*
		 * Realizar un programa que lea dos números y 
		 * nos diga si son o no iguales
		 */
		
		Scanner sc = new Scanner (System.in);
		
		int n1,n2;
		
		System.out.print("Num 1: ");
		n1 = sc.nextInt();
		
		System.out.print("Num 2: ");
		n2 = sc.nextInt();
		
		if(n1 == n2) {
			System.out.println(n1+" y "+n2+" son iguales");
		}else {
			System.out.println(n1+" y "+n2+" NO son iguales");
		}

	}

}
