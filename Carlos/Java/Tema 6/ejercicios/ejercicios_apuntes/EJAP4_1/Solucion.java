import java.util.Scanner;
public class Solucion {
    /*
     * Programa que solicita un número positivo diferente a cero por teclado y nos dice si es par o
       impar.
     */
    public static void main (String[] args) {
        //Aquí es necesario usar el nombre de la clase que estáis creando.
        Solucion programa = new Solucion();
        programa.inicio();
    }

    public void inicio() {
        //Instrucciones del método principal (problema general)
        int numero = solicitarNumero();
        esPar(numero);
    }

    public int solicitarNumero(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Escribe un numero diferente a 0: ");
        return sc.nextInt();
    }

    public void esPar(int num){
        if(num % 2 == 0){
            System.out.println("Es par");
        }else{
            System.out.println("Es impar");
        }
    }
}