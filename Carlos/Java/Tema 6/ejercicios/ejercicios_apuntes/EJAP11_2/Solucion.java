public class Solucion {
    /*
     * Crea una función recursiva para calcular la potencia de un número elevado a otro (x^n).
     */
    public static void main (String[] args) {
        Solucion programa = new Solucion();
        programa.inicio();
    }

    public void inicio(){
        System.out.println(factorial(5));
    }

    public int factorial(int num){
        if(num <= 1){
            return num;
        }

        return num * factorial(num-1);
    }

}
