package matematicas;

public class Numeros {

    public int sumaEnteros(int n1, int n2){
        return n1 + n2;
    }

    public double sumaDecimal(double n1, double n2){
        return n1 + n2;
    }

};
