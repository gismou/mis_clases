import matematicas.Numeros;
import matematicas.Texto;

public class Solucion {

    public static void main (String[] args) {

        Numeros n = new Numeros();
        Texto t = new Texto();

        int sum1 = n.sumaEnteros(10, 20);
        double sum2 = n.sumaDecimal(3.5, 4.6);
        String con1 = t.concatenaCaracteres('c', 'd');
        String con2 = t.concatenarStrings("Hola ", "Mundo");

        System.out.println(sum1);
        System.out.println(sum2);
        System.out.println(con1);
        System.out.println(con2);

    }

}
