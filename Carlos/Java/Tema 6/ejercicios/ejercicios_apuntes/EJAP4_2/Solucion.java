import java.util.Scanner;
public class Solucion {
    /*
     * Programa que solicita un número positivo diferente a cero y nos dice si es primero o no
     */
    public static void main (String[] args) {
        //Aquí es necesario usar el nombre de la clase que estáis creando.
        Solucion programa = new Solucion();
        programa.inicio();
    }

    public void inicio() {
        //Instrucciones del método principal (problema general)
        int numero = solicitarNumero();
        esPrimo(numero);
    }

    public int solicitarNumero(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Escribe un numero diferente a 0: ");
        return sc.nextInt();
    }

    public void esPrimo(int num){
        boolean primo = true;
        for(int i=2; i<num; i++){
            if(num%i == 0){
                primo = false;
            }
        }
        if(primo){
            System.out.println("Es primo");
        }else{
            System.out.println("No es primo");
        }
    }
}