public class Solucion {
    /*
     * Crea una función recursiva para calcular la potencia de un número elevado a otro (x^n).
     */
    public static void main (String[] args) {
        Solucion programa = new Solucion();
        programa.inicio();
    }

    public void inicio(){
        System.out.println(potencia(2.0,2));
    }

    public double potencia(double base, int exp){
        double res = 0;
        
        if(exp == 0){
            res = 1.0;
        }else{
            if (exp > 0){
                res = base * potencia(base,exp-1);
            }else{
                if(exp < 0){
                    res = base * potencia(base,exp+1);
                }
            }
        }

        return res;
    }

}
