import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		/*
		 * Dado un alumno, queremos obtener el número de clase que se le ha asignado, el nombre del
		   alumno y la nota que ha obtenido en un examen. A continuación aparecerá en pantalla 
		   la información introducida por el usuario
		 */
		
		String nombre;
		int clase;
		double nota;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Nombre del alumno: ");
		nombre = sc.nextLine();
		System.out.println();
		
		System.out.print("Numero de clase: ");
		clase = sc.nextInt();
		System.out.println();
		
		System.out.print("Nota del alumno: ");
		nota = sc.nextDouble();
		System.out.println();
		
		System.out.println("El alumno se llama "+nombre+", es de la clase numero "+clase+" y ha sacado un "+nota);

	}

}
