import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		/*
		 * Calcular la media aritmética de tres números decimales

		 */
		
		Scanner sc = new Scanner(System.in);
		
		float n1,n2,n3;
		float media;
		
		System.out.print("Introduce el número 1: ");
		n1 = sc.nextFloat();
		System.out.println();
		
		System.out.print("Introduce el número 2: ");
		n2 = sc.nextFloat();
		System.out.println();
		
		System.out.print("Introduce el número 3: ");
		n3 = sc.nextFloat();
		System.out.println();
		
		media = (n1+n2+n3)/3;
		//System.out.println("La media de "+n1+", "+n2+" y "+n3+" es "+media);
		System.out.printf("La media de %3.2f, %3.2f y %3.2f es %3.2f ", n1,n2,n3,media);

	}

}
