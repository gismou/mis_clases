/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa;


public class Enrollment {

    
    public static void main(String[] args) {
    	
    	//System.out.println("\n\n Ejercicio pendiente de realizar \n\n");
    	
    	System.err.println("Student's list:\n");
    	
    	// Creamos Estudiantes
    	Student st1 = new Student("Monica Geller","A00234");
    	st1.addCredits(24);
    	Student st2 = new Student("Joe Tribiani","C22044");
    	st2.addCredits(56);
    	Student st3 = new Student("Chandler Bing","A12003");
    	st3.addCredits(6);
    	Student st4 = new Student("Rachel Green","B66003");
    	st4.addCredits(12);
    	
    	// Mostramos Alumnos por pantalla
    	st1.print();
    	st2.print();
    	st3.print();
    	st4.print();
    	
    	// Creamos Clases de Laboratorio
    	LabClass lab1 = new LabClass(2);
    	lab1.setInstructor("Eli");
    	lab1.setTime("Miercoles, 15h");
    	lab1.setRoom("Aulario 2");
    	LabClass lab2 = new LabClass(1);
    	lab2.setInstructor("Jose Antonio");
    	lab2.setTime("Miercoles, 17h");
    	lab2.setRoom("Aulario 2");
    	System.out.println();
    	
    	// Metemos Alumnos a las clases de laboratorio
    	System.err.println("Enroll students (Eli):");
    	lab1.enrollStudent(st1);
    	lab1.enrollStudent(st2);
    	System.out.println();
    	System.err.println("Enroll students (Jose Antonio):");
    	lab2.enrollStudent(st3);
    	lab2.enrollStudent(st4);
    	
    	// Mostramos por pantalla las clases de laboratorio
    	lab1.printList();
    	lab2.printList();
    	
    }
    
}
