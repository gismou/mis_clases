/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa;


public class Dibujar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    	//System.out.println("\n\n Ejercicio pendiente de realizar \n\n");
    	
    	// ------  EL SOL -----------
        Circle sol = new Circle();
        sol.makeVisible();
        sol.changeColor("yellow");
    	
        // ------- TEJADO 1 ---------
        Triangle tej1 = new Triangle();
        tej1.makeVisible();
        tej1.changeColor("red");
        tej1.moveHorizontal(-80);
        tej1.moveVertical(20);
        
        // ------- FACHADA 1 --------
        Square fach1 = new Square();
        fach1.makeVisible();
        fach1.changeColor("blue");
        fach1.moveHorizontal(-210);
        fach1.moveVertical(100);
        
        // ------- PUERTA 1 --------
        Square puer1 = new Square();
        puer1.makeVisible();
        puer1.changeColor("magenta");
        puer1.changeSize(25);
        puer1.moveHorizontal(-190);
        puer1.moveVertical(135);
        
        // ------- PERSONA ----------
        Person pers = new Person();
        pers.makeVisible();
        pers.moveHorizontal(-40);
        pers.moveVertical(45);
        
        // ------- TEJADO 2 ---------
        Triangle tej2 = new Triangle();
        tej2.makeVisible();
        tej2.changeColor("red");
        tej2.moveHorizontal(140);
        tej2.moveVertical(20);
        
        // ------- FACHADA 2 --------
        Square fach2 = new Square();
        fach2.makeVisible();
        fach2.changeColor("green");
        fach2.moveHorizontal(10);
        fach2.moveVertical(100);
        
        // ------- PUERTA 2 --------
        Square puer2 = new Square();
        puer2.makeVisible();
        puer2.changeColor("black");
        puer2.changeSize(25);
        puer2.moveHorizontal(25);
        puer2.moveVertical(135);
        
        
    }
    
}
