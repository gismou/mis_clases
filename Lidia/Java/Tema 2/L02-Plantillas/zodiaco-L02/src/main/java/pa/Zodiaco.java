
package pa;


public class Zodiaco {
    private int dia;
    private int mes;
    private String signo;
    
    public Zodiaco(int dia, int mes) {
        this.dia = dia;
        this.mes = mes;
        signo = "Desconocido";
    }

    public int getDia() {
        return dia;
    }

    public int getMes() {
        return mes;
    }
    
    /**
     * Devuelve cierto si la fecha es válida ¨(día: 1..31, mes:1..12), y
       falso en caso contrario   
    */  
    private boolean fecha_valida() {
       boolean res = false;
       switch(this.mes){
       	case 1:
    	   if(this.dia > 0 && this.dia <= 31) {
    		   res = true;
    	   }else {
    		   System.out.println("Enero no puede tener "+this.dia+" dias");
    	   }
    	   break;
       	case 2:
     	   if(this.dia > 0 && this.dia <= 28) {
     		   res = true;
     	   }else {
     		   System.out.println("Febrero no puede tener "+this.dia+" dias");
     	   }
     	   break;
       	case 3:
     	   if(this.dia > 0 && this.dia <= 31) {
     		   res = true;
     	   }else {
     		   System.out.println("Marzo no puede tener "+this.dia+" dias");
     	   }
     	   break;
       	case 4:
     	   if(this.dia > 0 && this.dia <= 30) {
     		   res = true;
     	   }else {
     		   System.out.println("Abril no puede tener "+this.dia+" dias");
     	   }
     	   break;
       	case 5:
     	   if(this.dia > 0 && this.dia <= 31) {
     		   res = true;
     	   }else {
     		   System.out.println("Mayo no puede tener "+this.dia+" dias");
     	   }
     	   break;
       	case 6:
     	   if(this.dia > 0 && this.dia <= 30) {
     		   res = true;
     	   }else {
     		   System.out.println("Junio no puede tener "+this.dia+" dias");
     	   }
     	   break;
       	case 7:
     	   if(this.dia > 0 && this.dia <= 31) {
     		   res = true;
     	   }else {
     		   System.out.println("Julio no puede tener "+this.dia+" dias");
     	   }
     	   break;
       	case 8:
     	   if(this.dia > 0 && this.dia <= 31) {
     		   res = true;
     	   }else {
     		   System.out.println("Agosto no puede tener "+this.dia+" dias");
     	   }
     	   break;
       	case 9:
     	   if(this.dia > 0 && this.dia <= 30) {
     		   res = true;
     	   }else {
     		   System.out.println("Septiembre no puede tener "+this.dia+" dias");
     	   }
     	   break;
       	case 10:
     	   if(this.dia > 0 && this.dia <= 31) {
     		   res = true;
     	   }else {
     		   System.out.println("Octubre no puede tener "+this.dia+" dias");
     	   }
     	   break;
       	case 11:
     	   if(this.dia > 0 && this.dia <= 30) {
     		   res = true;
     	   }else {
     		   System.out.println("Noviembre no puede tener "+this.dia+" dias");
     	   }
     	   break;
       	case 12:
     	   if(this.dia > 0 && this.dia <= 31) {
     		   res = true;
     	   }else {
     		   System.out.println("Diciembre no puede tener "+this.dia+" dias");
     	   }
     	   break;
     	default:
     		System.out.println("El mes introducido no es correcto, debe ser un numero entre el 1 y el 12");
       }
       
       return res;
    }
 
    /**
     * Devuelve el nombre del signo asociado al día y mes de nacimiento, o el mensaje "Fecha inválida"   
    */
    public String obtener_signo() {
        
    	if(this.fecha_valida()) {
    		
    		switch(this.mes) {
    			case 1:
    				if(this.dia >= 1 && this.dia < 21) this.signo = "Capricornio";
    				else this.signo = "Acuario";
    				break;
    			case 2:
    				if(this.dia >= 1 && this.dia <= 19) this.signo = "Acuario";
    				else this.signo = "Piscis";
    				break;
    			case 3:
    				if(this.dia >= 1 && this.dia <= 20) this.signo = "Piscis";
    				else this.signo = "Aries";
    				break;
    			case 4:
    				if(this.dia >= 1 && this.dia <= 20) this.signo = "Aries";
    				else this.signo = "Tauro";
    				break;
    			case 5:
    				if(this.dia >= 1 && this.dia <= 21) this.signo = "Tauro";
    				else this.signo = "Geminis";
    				break;
    			case 6:
    				if(this.dia >= 1 && this.dia <= 21) this.signo = "Geminis";
    				else this.signo = "Cancer";
    				break;
    			case 7:
    				if(this.dia >= 1 && this.dia <= 23) this.signo = "Cancer";
    				else this.signo = "Leo";
    				break;
    			case 8:
    				if(this.dia >= 1 && this.dia <= 23) this.signo = "Leo";
    				else this.signo = "Virgo";
    				break;
    			case 9:
    				if(this.dia >= 1 && this.dia <= 23) this.signo = "Virgo";
    				else this.signo = "Libra";
    				break;
    			case 10:
    				if(this.dia >= 1 && this.dia <= 23) this.signo = "Libra";
    				else this.signo = "Escorpio";
    				break;
    			case 11:
    				if(this.dia >= 1 && this.dia <= 22) this.signo = "Escorpio";
    				else this.signo = "Sagitario";
    				break;
    			case 12:
    				if(this.dia >= 1 && this.dia <= 22) this.signo = "Sagitario";
    				else this.signo = "Capricornio";
    				break;
    		}
    		
    		System.out.println("El signo es "+this.signo);
    		
    	}
    	
        return this.signo;
    }    
}
