/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa;


public class PruebaMaquinas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    	//System.out.println("\n\n Ejercicio pendiente de realizar \n\n");
    	
    	// Crear máquinas de tickets
    	TicketMachine tick1 = new TicketMachine(20);
    	TicketMachine tick2 = new TicketMachine(30);
    	TicketMachine tick3 = new TicketMachine(40);
    	
    	// Impresión de máquina de tickets 1
    	System.out.println("Maquina 1. Precio ticket = "+tick1.getPrice());
    	System.out.println("Maquina 2. Precio ticket = "+tick2.getPrice());
    	System.out.println("Maquina 3. Precio ticket = "+tick3.getPrice());
    	
    	System.out.println();
    	
    	tick1.insertMoney(10);
    	System.out.println("Maquina 1. Insertamos 10");
    	tick1.insertMoney(5);
    	System.out.println("Maquina 1. Insertamos 5");
    	System.out.println("Valor de monedas insertadas: "+tick1.getBalance());
    	System.out.println("Maquina 1. Solicitamos ticket");
    	tick1.printTicket();
    	
    	System.out.println();
    	
    	// Impresión de máquina de tickets 2
    	tick2.insertMoney(10);
    	System.out.println("Maquina 2. Insertamos 10");
    	tick2.insertMoney(30);
    	System.out.println("Maquina 2. Insertamos 30");
    	System.out.println("Valor de monedas insertadas: "+tick2.getBalance());
    	System.out.println("Maquina 2. Solicitamos ticket");
    	tick2.printTicket();
    	
    	System.out.println();
    	
    	// Impresión de máquina de tickets 3
    	tick3.insertMoney(20);
    	System.out.println("Maquina 3. Insertamos 20");
    	tick3.insertMoney(20);
    	System.out.println("Maquina 3. Insertamos 20");
    	System.out.println("Valor de monedas insertadas: "+tick3.getBalance());
    	System.out.println("Maquina 3. Solicitamos ticket");
    	tick3.printTicket();
    	
    	// Impresión de máquina de tickets 2, segunda vez
    	tick2.insertMoney(20);
    	System.out.println("Maquina 2. Insertamos 20");
    	System.out.println("Valor de monedas insertadas: "+tick2.getBalance());
    	System.out.println("Maquina 2. Solicitamos ticket");
    	tick2.printTicket();
    	
    	// Recaudación de máquinas
    	System.out.println("Total recaudado por Maquina 1: "+tick1.getTotal());
    	System.out.println("Total recaudado por Maquina 2: "+tick2.getTotal());
    	System.out.println("Total recaudado por Maquina 3: "+tick3.getTotal());
    	
    	System.out.println();
    	
    	// Impresión de máquina de tickets 2, tercera vez
    	tick2.insertMoney(80);
    	System.out.println("Maquina 2. Insertamos 80");
    	System.out.println("Valor de monedas insertadas: "+tick2.getBalance());
    	tick2.refundBalance();
    	System.out.println("Maquina 2. Solicitamos devolucion");
    	
    	System.out.println();
    	
    	// Recaudación de máquinas, segunda vez
    	System.out.println("Total recaudado por Maquina 1: "+tick1.getTotal());
    	System.out.println("Total recaudado por Maquina 2: "+tick2.getTotal());
    	System.out.println("Total recaudado por Maquina 3: "+tick3.getTotal());
    	
    }
    
}
