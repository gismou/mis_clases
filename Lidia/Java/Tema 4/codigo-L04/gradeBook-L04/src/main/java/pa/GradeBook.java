package pa;

public class GradeBook {

	// Atributos
	private String courseName;
	private int[] grades;
	
	// Constructor
	public GradeBook(String courseName, int[] grades) {
		this.courseName = courseName;
		this.grades = grades;
	}
	
	// Getter y Setter
	public String getCourseName() {
		return this.courseName;
	}
	
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	
	// Otros Métodos
	public int getMinimun() {
		int minGrade = 100;
		for(int grade : this.grades) {
			if(grade < minGrade) minGrade = grade;
		}
		return minGrade;
	}
	
	public int getMaximun() {
		int maxGrade = 0;
		for (int grade : this.grades) {
			if(grade > maxGrade) maxGrade = grade;
		}
		return maxGrade;
	}
	
	public Double getAverage() {
		Double max = 0.00;
		for (int grade : this.grades) {
			max += grade;
		}
		return  max / this.grades.length;
	}
	
	public void printBarChart() {
		String col1 = "";
		String col2 = "";
		String col3 = "";
		String col4 = "";
		String col5 = "";
		String col6 = "";
		String col7 = "";
		String col8 = "";
		String col9 = "";
		String col10 = "";
		String col11 = "";
		for(int grade : this.grades) {
			if (grade >= 0 && grade <= 9) col1 += "*";
			if (grade >= 10 && grade <= 19) col2 += "*";
			if (grade >= 20 && grade <= 29) col3 += "*";
			if (grade >= 30 && grade <= 39) col4 += "*";
			if (grade >= 40 && grade <= 49) col5 += "*";
			if (grade >= 50 && grade <= 59) col6 += "*";
			if (grade >= 60 && grade <= 69) col7 += "*";
			if (grade >= 70 && grade <= 79) col8 += "*";
			if (grade >= 80 && grade <= 89) col9 += "*";
			if (grade >= 90 && grade <= 99) col10 += "*";
			if (grade == 100) col11 += "*";
		}
		System.out.println("\t 0- 9: "+col1);
		System.out.println("\t10-19: "+col2);
		System.out.println("\t20-29: "+col3);
		System.out.println("\t30-39: "+col4);
		System.out.println("\t40-49: "+col5);
		System.out.println("\t50-59: "+col6);
		System.out.println("\t60-69: "+col7);
		System.out.println("\t70-79: "+col8);
		System.out.println("\t80-89: "+col9);
		System.out.println("\t90-99: "+col10);
		System.out.println("\t100: "+col11);
		System.out.println();
	}
	
	public void printGrades() {
		int cont = 0;
		for(int grade : this.grades) {
			System.out.printf("%5d",grade);
			cont++;
			if(cont == 5) {
				cont = 0;
				System.out.println();
			}
		}
		System.out.println();
	}
	
	public void processGrades() {
		System.out.println("Listado de notas de la asignatura: "+this.getCourseName()+"\n");
		this.printGrades();
		System.out.printf("\nNota media: %.2f%n",this.getAverage());
		System.out.println("\nIntevalo de notas minima y maxima: ["+this.getMinimun()+", "+this.getMaximun()+"]");
		System.out.println("\nHistograma de notas de la asignatura: "+this.getCourseName()+"\n");
		this.printBarChart();
	}
		
}