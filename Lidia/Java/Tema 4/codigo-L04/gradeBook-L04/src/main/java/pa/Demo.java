package pa;

public class Demo {

	public static void main(String[] args) {
		
		// Array con notas
		int[] notas = {65,10,12,45,55,78,3,99,74,88,0,66,52,43,75,87,45,53,58,51,50,100,19,40};
		
		// Objeto GradeBook
		GradeBook mates = new GradeBook("Mates",notas);
		
		mates.processGrades();
	}

}
