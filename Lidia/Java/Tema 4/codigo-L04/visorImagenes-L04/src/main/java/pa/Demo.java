package pa;

import pa.interfaz.ImageViewer;

/**
 * La clase para cargar la versión demo de la Aplicación
 * <p>
 * Esta clase solo sirve de lanzadera para hacer uso de un objeto
 * de la clase pa.interfaz.ImageViewer, que podrá lanzar a ejecutar
 * lo necesario para poder probarla.
 * @author yo
 */
public class Demo {

	public static void main(String[] args) {
		
		ImageViewer imgv = new ImageViewer();

	}

}
