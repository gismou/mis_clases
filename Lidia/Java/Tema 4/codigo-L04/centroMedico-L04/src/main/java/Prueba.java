import pa.Cita;
import pa.Medico;

public class Prueba {

	public static void main(String[] args) {
		
		int citasProcesadas = 0;
		int citasNoPosibles = 0;
		Medico m1 = new Medico("001","Oftalmologo"); 
		m1.printHorario();
		System.out.println("\nIniciamos el proceso de reserva de Citas ...\n");
		Cita c1 = m1.reservarCita(2, "mañana", "Pedro");
		Cita c2 = m1.reservarCita(2, "mañana", "Maria");
		Cita c3 = m1.reservarCita(2, "mañana", "Lucas");
		Cita c4 = m1.reservarCita(2, "mañana", "Ana");
		Cita c5 = m1.reservarCita(2, "mañana", "Gloria");
		Cita c6 = m1.reservarCita(3, "otro", "Matias");
		Cita c7 = m1.reservarCita(8, "tarde", "Eva");
		Cita c8 = m1.reservarCita(4, "tarde", "Carlos");
		
		if(c1.getHoraConsulta() != "") { 
			c1.imprimirCita(); 
			citasProcesadas++;
		}else {
			System.out.println("Cita no posible para "+c1.getIdPaciente());
			citasNoPosibles++;
			citasProcesadas++;
		}
		
		if(c2.getHoraConsulta() != "") { 
			c2.imprimirCita(); 
			citasProcesadas++;
		}else {
			System.out.println("Cita no posible para "+c2.getIdPaciente());
			citasNoPosibles++;
			citasProcesadas++;
		}
		
		if(c3.getHoraConsulta() != "") { 
			c3.imprimirCita(); 
			citasProcesadas++;
		}else {
			System.out.println("Cita no posible para "+c3.getIdPaciente());
			citasNoPosibles++;
			citasProcesadas++;
		}
		
		if(c4.getHoraConsulta() != "") { 
			c4.imprimirCita(); 
			citasProcesadas++;
		}else {
			System.out.println("Cita no posible para "+c4.getIdPaciente());
			citasNoPosibles++;
			citasProcesadas++;
		}
		
		if(c5.getHoraConsulta() != "") { 
			c5.imprimirCita(); 
			citasProcesadas++;
		}else {
			System.out.println("Cita no posible para "+c5.getIdPaciente());
			citasNoPosibles++;
			citasProcesadas++;
		}
		
		if(c6.getHoraConsulta() != "") { 
			c6.imprimirCita(); 
			citasProcesadas++;
		}else {
			System.out.println("Cita no posible para "+c6.getIdPaciente());
			citasNoPosibles++;
			citasProcesadas++;
		}
		
		if(c7.getHoraConsulta() != "") { 
			c7.imprimirCita(); 
			citasProcesadas++;
		}else {
			System.out.println("Cita no posible para "+c7.getIdPaciente());
			citasNoPosibles++;
			citasProcesadas++;
		}
		
		if(c8.getHoraConsulta() != "") { 
			c8.imprimirCita(); 
			citasProcesadas++;
		}else {
			System.out.println("Cita no posible para "+c8.getIdPaciente());
			citasNoPosibles++;
			citasProcesadas++;
		}
		
		System.out.println("\nTotal de citas procesadas: "+citasProcesadas);
		System.out.println("Total de citas no posibles: "+citasNoPosibles+"\n");
		
		m1.printHorario();
		
	}

}
