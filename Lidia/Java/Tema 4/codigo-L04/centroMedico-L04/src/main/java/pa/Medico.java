	package pa;

/**
 * Clase que representa a un médico
 * 
 */
public class Medico {
	
    /**
     * El identificador del médico es una cadena de caracteres que es única para cada médico,
     * una vez definida no se puede cambiar
     */
    private String idMedico;
    
    /** La especialidad es una cadena de caracteres */
    private String especialidad;
    
    /** El horario es una matriz de tamaño NUM_DIAS X NUM_HORAS 
     *  <p>
     *  Consideraremos 5 días (0=lunes, 1= martes, ..., 4=viernes
     *  y 8 horas por día (9:00h, 10:00h,..,12:00h, 16:00h, 17:00h,...,19h)
     *  <p>
     *  La franja de 9:00h hasta 12:00h se considera de "mañanas"
     *  la franja de 16:00 hasta 19:00h se considera de "tardes"
     */
    private String[][] horario; 
    
    /** NUM_DIAS es una constante que representa el máximo de días
     *  Un médico trabaja 5 días a la semana
     */
    private final int NUM_DIAS=5; 
    
    /** NUM_HORAS es una constante que representa el máximo de horas de trabajo al día
     *  Un médico trabaja 8 horas cada día
     */
    private final int NUM_HORAS=8; //9, 10. 11. 12, 16, 17, 18, 19
    
    /**
     * Creamos un médito indicando su identificador y su especialidad
     * 
     * Cuando creamos un objeto de tipo Medico su horario está "vacío"
     * (el valor de cada "casilla" es null)
     * 
     * @param id identificador alfanumérico único para cada médico
     * @param especialidad : especialidad de ese médico
     */
    public Medico(String id, String especialidad) {
        this.idMedico = id;
        this.especialidad = especialidad; 
        horario = new String[NUM_DIAS][NUM_HORAS];
        //por defecto cada posición de la matriz se inicializa a null
    }
     
    /**
     * método getter
     * @return devuelve el identificador del médico
     */
    public String getIdMedico() {
        return idMedico;
    }

    /**
     * método getter
     * @return devuelve la especialidad del médico
     */
    public String getEspecialidad() {
        return especialidad;
    }

    /**
     * método setter para cambiar la especialidad del médico
     * @param especialidad  cadena de caracteres que representa la especialidad
     */
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
    
    /**
     * Un día es válido si es un entero comprendido entre 0..4
     * @param dia valor entero que queremos validar
     * @return devuelve true si se trata de un día válido, y false en caso contrario
     */
    private boolean diaValido(int dia) {
        if(dia >= 0 && dia <= 4) return true;
        return false;
    }
    
    /**
     * Una franja es válida si tiene como valor "mañana" o "tarde"
     * @param franja cadena de caracteres que representa la franja horaria
     * @return true, si se trata de una franja válida, y false en caso contrario
     */
    private boolean franjaValida(String franja) {
        if(franja == "mañana" || franja == "tarde") return true;
        return false;
    }
    
    /**
     * Método para crear una cita
     * @param dia entero que representa el día que queremos la cita (0..4)
     * @param franja franja horaria en la que queremos la cita ("mañana" o "tarde")
     * La franja de "mañana" se corresponde con el horaro de 9 a 12h
     * La franja de "tarde" se corresponde con el horario de 16 a 19h
     * @param paciente nombre del paciente que solicita la cita
     * @return devuelve un objeto de tipo Cita con el primer hueco libre en el 
     * horario del médico, para el día y franja horaria especificados.
     * Devuelve el valor null si no hay ningún "hueco" disponible en el horario del médico
     * o bien el día especificado es inválido o la franja especificada es inválida 
     */
    public Cita reservarCita(int dia, String franja, String paciente) {
    	
    	if(this.diaValido(dia) && this.franjaValida(franja)) {
    		
    		int horaIni = 0;
    		int horaMax = 3;
    		String horaCita = null;
    		
    		// Declarar horas según franja
    		if(franja == "mañana") {
    			horaIni = 0;
    			horaMax = 3;
    		}else if (franja == "tarde"){
    			horaIni = 4;
    			horaMax = 7;
    		}
    		
    		// Comprobar fecha libre de la semana
			for (int i = horaIni; i <= horaMax; i++){	
				if(this.horario[dia][i] == null) {
					this.horario[dia][i] = paciente;
					horaCita = this.hora(i);
					// Crear objeto
					Cita cita = new Cita(paciente,this.idMedico,this.especialidad,this.dia(dia),horaCita);
		    		return cita;
				}
			}
    		
    		
    	}
    	
        return new Cita(paciente,"","","","");    
    }
    
    /**
     * Método para imprimir por pantalla el horario de un médico
     * Se imprimirá la tabla con el horario del médico en la que se mostrará una 
     * cabecera con la hora de cada columna: 9:00h, 10:00h, ... 19h. También se
     * mostrará una columna de cabecera que indicará los días de la semana: Lunes, 
     * Martes, ...., Viernes. Finalmente, en cada en cada posición del horario se 
     * mostrará el nombre del paciente que ha reservado una cita o bien se mostrará 
     * " --- " indicando que ese día, a esa hora, el médico no tiene 
     * ninguna cita asignada
     */ 
    public void printHorario() {
        
      System.out.println("Horario del doctor: "+this.idMedico+"    Especialidad: "+this.especialidad+"\n");
      System.out.printf("%-11s","");
      // Pintar cabecera horas
      for(int i = 0; i<this.NUM_HORAS;i++) {
    	  System.out.printf("%-7s",this.hora(i));
      }
      System.out.println();
      
      // Pintar línea a línea días y sus citados
      for(int i = 0; i<this.NUM_DIAS; i++) {
    	  for(int j = 0; j<this.NUM_HORAS; j++) {
    		  String citado = (this.horario[i][j] == null || this.horario[i][j] == "") ? "---" : this.horario[i][j];
    	 	  if(j == 0) {
        		  // Cabecera de Días
        		  System.out.printf("%-11s",this.dia(i));
    	 	  }      	
    		  System.out.printf("%-7s",citado);     	  
    	  }
    	  System.out.println();
      }
        
    }
    
    /**
     * Método que devuelve la cadena de caracteres asociada al valor numérico correspondiente
     * @param indice representa un valor entre 0..4
     * @return "Lunes" o "Martes", ..., o "Viernes", dependiendo de si el día es 0 ó 1 ... ó 4
     * Si el valor numérico introducido no se corresponde con ningún día se devuelve una cadena vacía
     */
    private String dia(int indice) {
    	String res = "";
        switch(indice) {
            case 0 ->  res = "Lunes";
            case 1 -> res = "Martes";
            case 2 -> res = "Miercoles";
            case 3 -> res = "Jueves";
            case 4 -> res = "Viernes";
            default -> res = "";
        }
        return res;
    }
    /**
     * Método que devuelve la cadena de caracteres asociada al valor numérico correspondiente
     * @param indice representa un valor entre 0..7
     * @return "9:00h " o "10:00h", ..., o "19:00h", dependiendo de si el día es 0 ó 1 ... ó 7
     * Si el valor numérico introducido no se corresponde con ningún día se devuelve una cadena vacía
     */
    private String hora(int indice) {
        String res = "";
        switch(indice) {
        	case 0 -> res = "9:00";
        	case 1 -> res = "10:00";
        	case 2 -> res = "11:00";
        	case 3 -> res = "12:00";
        	case 4 -> res = "16:00";
        	case 5 -> res = "17:00";
        	case 6 -> res = "18:00";
        	case 7 -> res = "19:00";
        	default -> res = "";
        }
        return res;
    }
        
}
