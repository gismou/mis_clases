package hospital.facultativos;

import java.util.ArrayList;

public class Doctor {
	private String nombre;
	private String especialidad;
	//private int numPacientes; --> cambiado en una versión mas reciente del ejercicio
	private ArrayList<Paciente> pacientes = new ArrayList<>();
	private boolean atiendeUrgencias;
	
	// El constructor permite generar objetos de la clase, creandolos con los valores que se requiera
	// pudiendo incluso Sobrecargarlo, creando mas constructores pero con diferente número de parámetros
	public Doctor(String nombre, String especialidad, ArrayList<Paciente> pacientes, boolean atiendeUrgencias) {
		this.nombre = nombre;
		this.especialidad = especialidad;
		//this.numPacientes = numPacientes; 
		this.pacientes = pacientes;
		this.atiendeUrgencias = atiendeUrgencias;
	}
	
	// Constructor Sobrecargado para crear objetos de tipo doctor sin pacientes
	public Doctor(String nombre, String especialidad, boolean atiendeUrgencias) {
		this.nombre = nombre;
		this.especialidad = especialidad;
		this.atiendeUrgencias = atiendeUrgencias;
	};
	
	// Los Getter y Setter
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	
	public String getEspecialidad() {
		return this.especialidad;
	}
	
	/*--> cambiado en una versión mas reciente del ejercicio
	public void setNumPacientes(int numPacientes) {
		this.numPacientes = numPacientes;
	}*/
	
	/*--> cambiado en una versión mas reciente del ejercicio
	public int getNumPacientes() {
		return this.numPacientes;
	}*/
	
	public void setPacientes(ArrayList<Paciente> paciente) {
		this.pacientes = paciente;
	}
	
	public ArrayList<Paciente> getPacientes() {
		return this.pacientes;
	}
	
	public void setAtiendeUrgencias(boolean atiendeUrgencias) {
		this.atiendeUrgencias = atiendeUrgencias;
	}
	
	public boolean getAtiendeUrgencias() {
		return this.atiendeUrgencias;
	}

	// Métodos
	public void add_paciente(Paciente paciente) {
		this.pacientes.add(paciente);
	}
	
	public void remove_paciente(String id_paciente) {
		boolean borrado = false;
		for(int i = 0; i<this.pacientes.size(); i++) {
			if(this.pacientes.get(i).getIdentificador().equals(id_paciente)) {
				this.pacientes.remove(i);
				borrado = true;
			}
		}
		if(borrado) {
			System.out.println("Borrado paciente "+id_paciente);
		}else {
			System.out.println("Error: El paciente con identificador "+id_paciente+" no existe");
		}
	}
	
	public void listarPacientes() {
		for(Paciente paciente : this.pacientes) {
			System.out.println("- "+paciente.getIdentificador());
		}
	}
	
}
