import java.util.ArrayList;

import hospital.facultativos.Doctor;
import hospital.facultativos.Paciente;

public class Test2 {

	public static void main(String[] args) {
		
		// Creamos con un bucle for para crear 20 pacientes y añadirlos a un objeto Doctor
		Doctor doc1 = new Doctor("Pepe","Pediatra",false);
		for(int i=1; i<=20; i++) {
			String id_paciente = "";
			if(i < 10) {
				id_paciente = "pac00"+i;
			}else {
				id_paciente = "pac0"+i;
			}
			Paciente paciente = new Paciente(id_paciente);
			doc1.add_paciente(paciente);
		}
		
		// Mostramos los pacientes del doctor (en listar uso el forEach)
		System.out.println("Pacientes del doctor "+doc1.getNombre());
		doc1.listarPacientes();
		System.out.println();
		
		// Borramos pacientes 
		doc1.remove_paciente("pac005");
		doc1.remove_paciente("pac013");
		doc1.remove_paciente("pac017");
		doc1.remove_paciente("pac050");
		doc1.remove_paciente("pac076");

		// Mostramos de nuevo a los pacientes del doctor
		System.out.println("\nPacientes del doctor "+doc1.getNombre());
		doc1.listarPacientes();
		
	}

}
