

import java.util.ArrayList;

import hospital.facultativos.Doctor;
import hospital.facultativos.Paciente;

public class Test1 {

	public static void main(String[] args) {
		
		// Creamos dos doctores diferentes
		//Doctor doc1 = new Doctor("Pepe", "Traumatologo", 25, true); --> cambiado en una versión mas reciente del ejercicio
		//Doctor doc2 = new Doctor("Cristina", "Pediatra", 134, false); --> cambiado en una versión mas reciente del ejercicio
		Doctor doc1 = new Doctor("Pepe","Traumatologo",true);
		Doctor doc2 = new Doctor("Cristina","Pediatra",false);
		
		// Imprimimos por pantalla el valor de los dos objetos doctor
		System.out.println("---- DOCTOR 1 ----");
		System.out.println("Nombre: "+doc1.getNombre());
		System.out.println("Especialidad: "+doc1.getEspecialidad());
		//System.out.println("Num. Pacientes: "+doc1.getNumPacientes()); --> cambiado en una versión mas reciente del ejercicio
		System.out.println("Atiende Urgencias: "+doc1.getAtiendeUrgencias());
		System.out.println();
		
		System.out.println("---- DOCTOR 2 ----");
		System.out.println("Nombre: "+doc2.getNombre());
		System.out.println("Especialidad: "+doc2.getEspecialidad());
		//System.out.println("Num. Pacientes: "+doc2.getNumPacientes()); --> cambiado en una versión mas reciente del ejercicio
		System.out.println("Atiende Urgencias: "+doc2.getAtiendeUrgencias());
		System.out.println();
		
		// Cambiamos algunos valores de los dos doctores
		System.out.println("Cambiamos algunos valores de los dos doctores...\n");
		doc1.setAtiendeUrgencias(true);
		doc1.setEspecialidad("Quiropractico");
		doc2.setNombre("Rosalinda");
		//doc2.setNumPacientes(234); --> cambiado en una versión mas reciente del ejercicio
		
		// Imprimimos los doctores con los cambios realizados
		System.out.println("---- DOCTOR 1 ----");
		System.out.println("Nombre: "+doc1.getNombre());
		System.out.println("Especialidad: "+doc1.getEspecialidad());
		//System.out.println("Num. Pacientes: "+doc1.getNumPacientes()); --> cambiado en una versión mas reciente del ejercicio
		System.out.println("Atiende Urgencias: "+doc1.getAtiendeUrgencias());
		System.out.println();
		
		System.out.println("---- DOCTOR 2 ----");
		System.out.println("Nombre: "+doc2.getNombre());
		System.out.println("Especialidad: "+doc2.getEspecialidad());
		//System.out.println("Num. Pacientes: "+doc2.getNumPacientes()); --> cambiado en una versión mas reciente del ejercicio
		System.out.println("Atiende Urgencias: "+doc2.getAtiendeUrgencias());
		System.out.println();
		
	}

}
