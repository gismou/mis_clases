package pa.hospital.conInterfaz;

public interface IGestionSalasHospital {
	void reservar();
	void anularReserva();
}
