package pa.hospital.sinInterfaz;

public class SalaVideo {
	
	private int id;
	
	public SalaVideo(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void reservar() {
		System.out.println("Reservando sala de video "+id);
	}
	
	public void anularReserva() {
		System.out.println("Anulando reserva de sala de video "+id);
	}
	
}
