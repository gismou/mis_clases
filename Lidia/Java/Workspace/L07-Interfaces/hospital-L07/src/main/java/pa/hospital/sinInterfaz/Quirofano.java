package pa.hospital.sinInterfaz;

public class Quirofano {
	
	private String id;
	
	public Quirofano(String id) {
		this.id = id;
	}

	public String getId() {
		return this.id;
	}
	
	public void reservar() {
		System.out.println("Reservando quirofano "+id);
	}
	
	public void anularReserva() {
		System.out.println("Anulando reserva de quirofano "+id);
	}

	
}
