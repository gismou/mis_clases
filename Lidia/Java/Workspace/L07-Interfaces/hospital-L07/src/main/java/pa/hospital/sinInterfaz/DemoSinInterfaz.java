package pa.hospital.sinInterfaz;

public class DemoSinInterfaz {

	public static void main(String[] args) {
		
		GestionSalasHospital gs = new GestionSalasHospital();
		Quirofano q1 = new Quirofano("Q1");
		Quirofano q2 = new Quirofano("Q2");
		SalaVideo s1 = new SalaVideo(1);
		SalaVideo s2 = new SalaVideo(2);
		
		gs.reservarQurifano(q1);
		gs.reservarQurifano(q2);
		gs.anularReservaQuirofano(q1);
		gs.anularReservaQuirofano(q2);
		gs.reservarSalaVideo(s1);
		gs.reservarSalaVideo(s2);
		gs.anularReservaSalaVideo(s1);
		gs.anularReservaSalaVideo(s2);
		
	}

}
