package pa.hospital.conInterfaz;

public class Quirofano implements IGestionSalasHospital{
	
	private String id;
	private static int contador = 0;
	private final String prefijo = "quirofano-";
	
	public Quirofano() {
		this.id = prefijo+contador;
		this.contador++;
	}

	public String getId() {
		return this.id;
	}
	
	@Override
	public void reservar() {
		System.out.println("    Reserva del quirofano: "+id);
	}
	
	@Override
	public void anularReserva() {
		System.out.println("    Anulada reserva del quirofano: "+id);
	}

}
