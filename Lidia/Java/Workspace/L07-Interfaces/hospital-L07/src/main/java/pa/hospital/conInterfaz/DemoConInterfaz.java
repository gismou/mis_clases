package pa.hospital.conInterfaz;

public class DemoConInterfaz {

	public static void main(String[] args) {
		
		//crear quirofanos y salas video y ponerlas en un array
		IGestionSalasHospital[] quirofanos = new IGestionSalasHospital[5];
		IGestionSalasHospital[] salasVideo = new IGestionSalasHospital[10];
		for(int i=0; i<quirofanos.length; i++) {
			quirofanos[i] = new Quirofano();
		}
		for(int i=0; i<salasVideo.length; i++) {
			salasVideo[i] = new SalaVideo();
		}
		
		//reserva quirofanos
		for(IGestionSalasHospital q : quirofanos){
			q.reservar();
		}
		
		//reserva salas de video
		for(IGestionSalasHospital s : salasVideo) {
			s.reservar();
		}
		
		// anular reservas quirofano
		for(IGestionSalasHospital q : quirofanos) {
			q.anularReserva();
		}
		
		// anular reservas salas de video
		for(IGestionSalasHospital s : salasVideo) {
			s.anularReserva();
		}
		
	}

}
