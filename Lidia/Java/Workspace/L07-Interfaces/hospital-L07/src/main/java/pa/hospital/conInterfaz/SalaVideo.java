package pa.hospital.conInterfaz;

public class SalaVideo implements IGestionSalasHospital{
	
	private int id;
	private static int contador = 100;
	
	public SalaVideo() {
		this.id = contador;
		this.contador++;
	}
	
	public int getId() {
		return this.id;
	}
	
	@Override
	public void reservar() {
		System.out.println("---> Reserva de la sala de video "+id);
	}
	
	@Override
	public void anularReserva() {
		System.out.println("---> Anulada reserva de sala de video "+id);
	}
	
}
