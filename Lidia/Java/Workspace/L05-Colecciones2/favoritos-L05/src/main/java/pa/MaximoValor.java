package pa;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class MaximoValor {

	public static void main(String[] args) {
		
		System.out.println("Introduzca los datos con o sin decimales separados con espacios.");
		System.out.println("Teclee una letra para finalizar: ");
		Scanner sc = new Scanner(System.in);
		sc.useLocale(Locale.ENGLISH);
		
		// Ir metiendo en una Lista los valores que el usuario introduce hasta que dejen de ser 
		// valores Double.
		ArrayList<Double> valores = new ArrayList<>();
		do {
			try {
				Double valor = sc.nextDouble();
				valores.add(valor);
			}catch(Exception e) {
				System.out.println(e);
			}
		}while(sc.hasNextDouble());
		
		sc.close();
		
		// Buscamos el elemento con mayor valor y guardamos su valor
		Double mayor = 0.0;
		for(Double valorIterado : valores) {
			if(valorIterado > mayor) {
				mayor = valorIterado;
			}
		}
		
		// Recorremos e imprimimos por pantalla todos los números de la Lista,  indicando en texto
		// cual es el mayor.
		System.out.println("Valores numericos y maximo valor: ");
		for(Double valorIterado : valores) {
			if(valorIterado.equals(mayor)) {
				System.out.println(valorIterado+" <== maximo valor");
			}else {
				System.out.println(valorIterado);
			}
		}
		
	}

}
