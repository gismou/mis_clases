package pa.coffee;
import pa.excepciones.TooHotException;	
import pa.excepciones.TooColdException;
import pa.coffee.CoffeeCup;

public class VirtualPerson {
	private final int TOO_COLD = 35;
	private final int TOO_HOT = 85;
	private String name;
	
	public VirtualPerson() {
		this.name = "anonimo";
	};
	
	public VirtualPerson(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
		
	public void drinkCoffee(CoffeeCup cup) throws TooColdException, TooHotException {
		System.out.println("\t"+this.name+": me voy a tomar un cafe");
		if(cup.getTemperature() <= TOO_COLD) {
			System.out.println("\t"+this.name+": el cafe esta a "+cup.getTemperature()+" grados: Demasiado FRIO");
			throw new TooColdException();
		}else if(cup.getTemperature() >= TOO_HOT) {
			System.out.println("\t"+this.name+": el cafe esta a "+cup.getTemperature()+" grados: Demasiado CALIENTE");
			throw new TooHotException();
		}
	};
}
