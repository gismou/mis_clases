package pa.coffee;

public class CoffeeCup {
	private int temperature;
	
	public CoffeeCup(int temperature) {
		this.temperature = temperature;
	}

	public int getTemperature() {
		return this.temperature;
	}
}
