package pa.coffee;
import pa.excepciones.TooHotException;
import pa.excepciones.TooColdException;

import java.util.ArrayList;

import pa.coffee.CoffeeCup;
import pa.coffee.VirtualPerson;

public class VirtualCafe {
	private String name;
	private ArrayList<VirtualPerson> clients;
	private double ganancias;
	private final double PRECIO_CAFE = 2.00;
	private final double PROPINA = 0.50;
	
	public VirtualCafe(String name) {
		this.name = name;
		this.ganancias = 0.0;
		this.clients = new ArrayList<VirtualPerson>();
		System.out.println("La cafeteria virtual "+this.name+" abre sus puertas");
		System.out.println("-----------------------------------------------------------");
	}

	public String getName() {
		return name;
	}
	
	public ArrayList<VirtualPerson> getClients() {
		return clients;
	}
	
	public double getGanancias() {
		return ganancias;
	}
	
	public int getNumberClients() {
		return clients.size();
	}
	
	private void cobrarCafe(boolean propina) {
		double cobrado = PRECIO_CAFE;
		if(propina) cobrado+= PROPINA;
		System.out.println("\tcobrados: "+cobrado+" euros");
		this.ganancias+=cobrado;
	}
	
	public void addClient(VirtualPerson cliente, boolean nuevoCliente) {
		if(nuevoCliente) {
			System.out.println("El cliente "+cliente.getName()+" ha entrado a la cafeteria");
		}
		this.clients.add(cliente);
	}
	
	public boolean serveCustomer(VirtualPerson cust, CoffeeCup cup) {
		System.out.println("Hola "+cust.getName()+", aqui tiene su cafe");
		try {
			cust.drinkCoffee(cup);
			System.out.println("\t"+cust.getName()+": el cafe esta en su punto. Gracias!!");
			this.cobrarCafe(true);
			return false;
		}catch(TooColdException tc) {
			tc.getMessage();
			System.out.println("\t"+cust.getName()+": Traigame otro cafe");
			return true;
		}catch(TooHotException th) {
			th.getMessage();
			System.out.println("\t"+cust.getName()+" No le dejare propina");
			this.cobrarCafe(false);
			return false;
		}
	}
	
	public VirtualPerson getNextClient() {
		if(this.clients.size() == 0) {
			return null;
		}else {
			VirtualPerson siguienteCliente = clients.get(0);
			clients.remove(0);
			return siguienteCliente;
		}
	}
	
	
}
