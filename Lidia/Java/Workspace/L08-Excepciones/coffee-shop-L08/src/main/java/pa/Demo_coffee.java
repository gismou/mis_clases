package pa;

import java.util.ArrayList;

import pa.coffee.CoffeeCup;
import pa.coffee.VirtualCafe;
import pa.coffee.VirtualPerson;

public class Demo_coffee {

	public static void main(String[] args) {
		
		String temps[] = {"aaa","","20","95","75","18","35.7","55"};
		String names[] = {"", "Roberto", "Veronica", "Alberto","Maria"};
		ArrayList<CoffeeCup> cups = new ArrayList<CoffeeCup>();
		
		// Crear objetos CoffeeCup
		System.out.println("Comenzamos...\n");
		System.out.println("Creamos los cafes que podemos servir: ");
		for(int i=0; i<temps.length; i++) {
			try {
				int temp = Integer.parseInt(temps[i]);
				CoffeeCup cup = new CoffeeCup(temp);
				cups.add(cup);
				System.out.println("\tLa entrada con valor: "+temps[i]+" es correcta");
			}catch(NumberFormatException ne) {
				if(temps[i] == "") {
					System.out.println("\tDetectado un valor de temperatura vacio");
				}else {
					System.out.println("\tEntrada incorrecta: "+temps[i]+" no es un numero entero");
				}
			}
		}
		
		System.out.println();
		
		// Crear cafeteria
		VirtualCafe cafeteria = new VirtualCafe("Dolche Gusto");
		System.out.print("Podemos preparar hasta "+cups.size()+" cafes con diferentes temperaturas: ");
		for(int i=0; i<cups.size(); i++) {
			if(i == cups.size()-2) {
				System.out.print(cups.get(i).getTemperature()+" y ");
			}
			else if(i == cups.size()-1) {
				System.out.print(cups.get(i).getTemperature());
			}else {
				System.out.print(cups.get(i).getTemperature()+", ");
			}	
		}
		
		System.out.println();
		System.out.println();
		
		// Crear clientes
		for(int i=0; i<names.length; i++) {
			VirtualPerson p;
			if(names[i] == "") p = new VirtualPerson();
			else p = new VirtualPerson(names[i]);
			cafeteria.addClient(p, true);
		}
		
		System.out.println("\nTenemos que atender a los siguientes clientes: ");
		for(VirtualPerson p : cafeteria.getClients()) {
			System.out.println("\t- "+p.getName());
		}
		
		System.out.println();
		
		// Servir cafes
		int cont = 0;
		VirtualPerson vp = cafeteria.getNextClient();
		while(vp != null) {
			if(cont == cups.size()) cont = 0;
			CoffeeCup c = cups.get(cont);
			if(cafeteria.serveCustomer(vp, c)) {
				System.out.println("\tNo se preocupe "+vp.getName()+": Ahora le traeremos otro cafe");
				cafeteria.addClient(vp, false);
			}
			vp = cafeteria.getNextClient();
			cont++;
		}
		
		System.out.println("\nYa no quedan clientes a los que servir\n");
		System.out.println("Hoy se ha recaudado un total de "+cafeteria.getGanancias()+" euros");
	
	}

}
