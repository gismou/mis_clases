package pa;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import pa.Peticion;

public class Ascensor {

	private ArrayList<Peticion> peticiones = new ArrayList<Peticion>();
	private ArrayList<Peticion> peticiones_validas = new ArrayList<Peticion>();;
	private int piso_actual;
	private final Scanner lector_peticiones = new Scanner(System.in);
	private final int MAX_PISOS = 20;
	private final int MAX_PERSONAS = 4;
	private IAccesoDatos ad;
	
	public Ascensor(IAccesoDatos ad) {
		this.ad = ad;
	}
	
	private String nuevo_aviso(Peticion p) {
		String mensaje_error ="";
		
		//cada nuevo aviso se añadirá
		if(peticiones_validas.size()+1 > MAX_PERSONAS) mensaje_error += "\t   - El usuario que ha pulsado "+p.getPiso_destino()+" ya no cabe\n";
		if(p.getPiso_destino() < 0 || p.getPiso_destino() > MAX_PISOS) mensaje_error += "\t   - El usuario que ha pulsado "+p.getPiso_destino()+" ha introducido un valor incorrecto\n";
		if(p.getPiso_destino() == piso_actual) mensaje_error += "\t   - El usuario que ha pulsado "+p.getPiso_destino()+" ya está en esa planta\n";
		
		return mensaje_error;
	}
	
	public int leer_peticiones() {
		int peticionesLeidas = 0;
		System.out.println("\n---------------------------------");
		System.out.println("Estoy en el piso: "+this.piso_actual); //aquí añadiremos el piso_actual);
		System.out.println("Puertas abiertas. Espero peticiones: ");
		
		int[] array = ad.obtenerDatos();
		
		for(int i : array) {
			Peticion p = new Peticion(i);
			peticiones.add(p);
			peticionesLeidas++;
		}

		return peticionesLeidas; //aquí devolveremos el número de peticiones que hemos leído	
	}
	
	public int seleccionar_peticiones_validas () {
		String avisos = ""; //aquí vamos concatenando todos los avisos de todas las peticiones no válidas
		int numAvisos = 0;
		//analizamos una a una todas las peticiones pasadas por parámetro
		for(int i=0; i<peticiones.size(); i++) {
			if(peticiones_validas.size() < MAX_PERSONAS && peticiones.get(i).getPiso_destino() != piso_actual && peticiones.get(i).getPiso_destino() >= 0 && peticiones.get(i).getPiso_destino() < 20) {
				peticiones_validas.add(peticiones.get(i));
			}else {
				avisos += nuevo_aviso(peticiones.get(i));
				numAvisos++;
			}
		}
		
		peticiones.clear();
		
		//mostramos los pisos de destino de las personas que han subido al ascensor
		if(peticiones_validas.size() > 0) {
			System.out.print("\tEntran en el ascensor las personas que van a los pisos: ");
			for(Peticion p : peticiones_validas) {
				System.out.print(p.getPiso_destino()+", ");
			}
			System.out.println();
		}
		
	
		//mostramos los avisos, si los hay
		if(numAvisos != 0) {
			System.out.println("\tAVISOS:");
			System.out.print(avisos);
		}
		
	   return numAvisos;  //devolvemos el número de avisos (peticiones no válidas)
	}
	
	public void llevar_personas_a_sus_destinos() {	
		System.out.println("Cerrando puertas. Estamos en el piso: "+piso_actual);
		for(int i=0; i<peticiones_validas.size(); i++) {
			
			Peticion p = peticiones_validas.get(i);
			Peticion p_anterior;
			
			if(p.getPiso_destino() > piso_actual) {
				System.out.println("\tSubiendo a una persona a la planta "+p.getPiso_destino());
				piso_actual = p.getPiso_destino();
			}else{
				if(i != 0) {
					p_anterior = peticiones_validas.get(i-1);
					if(p.getPiso_destino() == p_anterior.getPiso_destino()) {
						System.out.println("\t   La siguiente persona también puede bajar");
					}else {
						System.out.println("\tBajando a una persona a la planta "+p.getPiso_destino());
						piso_actual = p.getPiso_destino();
					}
				}else {
					System.out.println("\tBajando a una persona a la planta "+p.getPiso_destino());
					piso_actual = p.getPiso_destino();
				}
			}
			
				
		}
		
		peticiones_validas.clear();
	}
}
