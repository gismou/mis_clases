package pa;

import java.util.Scanner;

public class EntradaDesdeTeclado implements IAccesoDatos{
	
	private final Scanner lector_peticiones = new Scanner(System.in);

	public int EntradaDesdeTeclado() {
		return lector_peticiones.nextInt();
	};

	@Override
	public int[] obtenerDatos() {
		int cont=0;
		int[] array = new int[100];
		while(lector_peticiones.hasNextInt()) {
			array[cont] = EntradaDesdeTeclado();
			cont++;
		}
		lector_peticiones.nextLine();
		return array;
	}
	
}
