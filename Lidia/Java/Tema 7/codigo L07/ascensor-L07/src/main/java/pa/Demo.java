package pa;

import pa.Ascensor;

public class Demo {

	public static void main(String[] args) {
		
		int num_peticiones, num_peticiones_con_avisos;
		
		IAccesoDatos ad = new EntradaDesdeTeclado();
		if(args[0] == "fichero") ad = new EntradaDesdeFichero();
		if(args[0] == "terminal") ad = new EntradaDesdeTeclado();
		
		Ascensor ascensor = new Ascensor(ad); 
		do {
			num_peticiones= ascensor.leer_peticiones();
			num_peticiones_con_avisos = ascensor.seleccionar_peticiones_validas();
			ascensor.llevar_personas_a_sus_destinos();
		}  while (num_peticiones!= num_peticiones_con_avisos);
       
		System.out.println("\tNumero total de peticiones: "+num_peticiones+", Peticiones rechazadas: "+num_peticiones_con_avisos+"\n");
		System.out.println("\nAscensor inactivo.");      

	}

}
