package pa;

import pa.interfaces.IOrdenable;
import pa.sorting.Country;
import pa.sorting.Grade;
import pa.sorting.Sort;

public class Demo1 {

	public static void main(String[] args) {
		// Objeto Sort
		Sort orden = new Sort();
		
		// Objetos Country
		Country c1 = new Country("Espanya");
		Country c2 = new Country("Venezuela");
		Country c3 = new Country("Francia");
		Country c4 = new Country("Belgica");
		Country c5 = new Country("Holanda");
		IOrdenable[] arrCountry = {c1,c2,c3,c4,c5};
		
		// Objetos Grade
		Grade g1 = new Grade(8.5);
		Grade g2 = new Grade(4.5);
		Grade g3 = new Grade(0.2);
		Grade g4 = new Grade(5.0);
		Grade g5 = new Grade(1.6);
		IOrdenable[] arrGrade = {g1,g2,g3,g4,g5};
		
		System.out.println("Original (Paises)");
		System.out.println("===================");
		for(IOrdenable o : arrCountry) {
			Country pais = (Country) o;
			System.out.print(" "+pais.getName()+" ");
		}
		System.out.println();
		
		// Ordenamos arrCountry
		orden.selectionSort(arrCountry);
		
		System.out.println("\nOrdenado (Paises)");
		System.out.println("===================");
		for(IOrdenable o : arrCountry) {
			Country pais = (Country) o;
			System.out.print(" "+pais.getName()+" ");
		}
		System.out.println();
		
		System.out.println("\nOriginal (Notas)");
		System.out.println("===================");
		for(IOrdenable o : arrGrade) {
			Grade nota = (Grade) o;
			System.out.print(" "+nota.getValue()+" ");
		}
		System.out.println();
		
		// Ordenamos arrNotas
		orden.selectionSort(arrGrade);
		
		System.out.println("\nOrdenado (Notas)");
		System.out.println("===================");
		for(IOrdenable o : arrGrade) {
			Grade nota = (Grade) o;
			System.out.print(" "+nota.getValue()+" ");
		}
		System.out.println();

	}

}
