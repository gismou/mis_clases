package pa;

import pa.music.organizer.MusicOrganizer;

public class PlayerListados {

	public static void main(String[] args) {
		
		MusicOrganizer mo = new MusicOrganizer();
		String path = "mp3/album/";
		String extension = ".mp3";
		String[] titulos = {"tema1A","tema2B", "tema3A", "tema4B", "tema5A", "tema6B", "tema7B"};
		
		System.out.println("Inicializamos nuestro organizador de musica...");
		mo.loadSongs(path, extension, titulos);
		
		System.out.println("Listado de todos los temas:");
		mo.printAllTitleNames();
		
		System.out.println("Listado de ficheros:");
		mo.printAllFileNames();
		
		System.out.print("El titulo de la posicion 3 es: ");
		mo.printTitleName(3);
		System.out.print("El titulo de la posicion -1 es: ");
		mo.printTitleName(-1);
		System.out.print("El titulo de la posicion 8 es: ");
		mo.printTitleName(8);
		
		System.out.println("Anyadimos un tema nuevo (\"tema1C\")...");
		mo.addSong(path, extension, "tema1C");
		System.out.print("El fichero mp3 numero 8 es: ");
		mo.printFileName(8);
		
		System.out.println("Borramos las canciones que contienen \"B\"");
		while(mo.findFirst("B") != -1) {
			int indexEncontrado = mo.findFirst("B");	
			mo.removeSong(indexEncontrado);
		}

		System.out.println("Nuevo listado de nombres de canciones: ");
		mo.printAllTitleNames();
		
		System.out.println("Nuevo listado de ficheros mp3: ");
		mo.printAllFileNames();
		
	}

}
