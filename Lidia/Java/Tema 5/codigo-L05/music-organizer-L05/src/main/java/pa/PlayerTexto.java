package pa;

import java.util.Scanner;

import pa.music.organizer.MusicOrganizer;

public class PlayerTexto {

	public static void main(String[] args) {
		
		MusicOrganizer mo = new MusicOrganizer();
		String[] titulos = {"Best Friend_Sofi Tukker feat NERVO",
				"I Wish_Andrew Rayel-Robbie Seed-Jimmy Chou",
				"Infinitely Falling_Fly By Midnight",
				"Invincible_Christina Novelli_Nash_Tom Rogers_J.Puchler",
				"Just Around The Hill_Sash",
				"Respect-The Promise_When In Rome-Erasure-Kylie Minogue",
				"Robarte un Beso_Carlos Vives-Sebastián Yatra",
				"Save Your Tears_Ariana Grande",
				"Simples Corazones_Fonseca-feat Melendi",
				"Stay With Me_AVIRA-Linney",
				"Vagabundo_Sebastian Yatra-Manuel Turizo-Beele",
				"With My Own Eyes_Sash"};
		String path = "src/main/resources/mp3/";
		String extension = ".mp3";
		String otra = "s";
		Scanner s = new Scanner(System.in);
		
		mo.loadSongs(path, extension, titulos);
		
		while(otra.equals("s")) {
			System.out.println("Cargando las canciones en el reproductor...");
			mo.printAllTitleNames();
			
			int opcion = s.nextInt();
			s.nextLine();
			while(opcion<=0 || opcion > mo.getNumberOfFiles()) {
				mo.printAllTitleNames();
				System.out.println("Entrada erronea. Intente de nuevo");
				opcion = s.nextInt();
				s.nextLine();
			}
			
			System.out.println("Voy a reproducir el tema "+mo.getTitleByIndex(opcion-1));
			mo.startPlaying(opcion);
			
			System.out.println("Pulsa enter para parar...");
			s.nextLine();
			mo.stopPlaying();
			System.out.println("Reproductor detenido");
			
			System.out.println("Quieres escuchar otra cancion? (s/n)");
			otra = s.next();
			s.nextLine();
			while(!otra.equals("s") && !otra.equals("n")) {
				System.out.println("Opcion incorrecta (pulsa 's' o 'n')");
				System.out.println("Quieres escuchar otra cancion? (s/n)");
				otra = s.next();
				s.nextLine();
				System.out.println();
			}
			
		}
		s.close();
	}

}
