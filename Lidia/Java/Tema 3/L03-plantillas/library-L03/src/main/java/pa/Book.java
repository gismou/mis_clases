package pa;
/**
 * A class that maintains information on a book.
 * This might form part of a larger application such
 * as a library system, for instance.
 *
 * @author (Insert your name here.)
 * @version (Insert today's date here.)
 */
public class Book
{
    // The fields.
    private String author;
    private String title;
    private int pages;
    private String refNumber = "";
    private int borrowed = 0;

    /**
     * Set the author and title fields when this object
     * is constructed.
     */
    public Book(String bookAuthor, String bookTitle, int bookPages)
    {
        author = bookAuthor;
        title = bookTitle;
        pages = bookPages;
    }

   public String getAuthor() {
	   return this.author;
   }
   
   public String getTitle() {
	   return this.title;
   }
   
   public int getPages() {
	   return this.pages;
   }
   
   public void setRefNumber(String ref) {
	   if(ref.length() >= 3) {
		   refNumber = ref;
	   }else {
		   System.out.println("ERROR: El número de referencia del libro "+title+" debe contener al menos tres caracteres");
	   }
   }
   
   public String getRefNumber() {
	   return this.refNumber;
   }
   
   public void prestar() {
	   borrowed = borrowed + 1;
   }
   
   public int getBorrowed() {
	   return this.borrowed;
   }
   
   public void mostrarVecesPrestado() {
	   if (borrowed > 0) {
		   if (borrowed == 1){
			   System.out.println("El libro "+title+" se ha prestado 1 vez");
		   }else {
			   System.out.println("El libro "+title+" se ha prestado "+borrowed+" veces");
		   }
	   }else if (borrowed == 0) {
		   System.out.println("El libro "+title+" no se ha prestado ninguna vez");
	   }
   }
   
   public void printDetails() {
	  String msg = "Detalles de "+title+"\n---------------------------------\n";
	  msg = msg + "   --- Autor: "+author+"\n   --- Titulo: "+title+"\n   --- Nº paginas: "+pages+"\n";
	  if(refNumber.length() == 0) {
		  msg = msg + "   --- Nº Referencia: ZZZ\n";
	  }else {
		  msg = msg + "   --- Nº Referencia: "+refNumber+"\n";
	  }
	  msg = msg + "   --- Nº prestamos: "+borrowed+"\n---------------------------------";
	  System.out.println(msg);
   }
}
