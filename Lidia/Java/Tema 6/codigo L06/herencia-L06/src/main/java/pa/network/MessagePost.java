package pa.network;

import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * This class stores information about a post in a social network. 
 * The main part of the post consists of a (possibly multi-line)
 * text message. Other data, such as author and time, are also stored.
 * 
 * @author Michael Kölling and David J. Barnes
 * @version 0.1
 */
public class MessagePost extends Post
{
    private String message;   // an arbitrarily long, multi-line message

    public MessagePost(String author, String text)
    {
        super(author);
        message = text;
    }

    public String getText()
    {
        return message;
    }

    @Override
    public void display()
    {
    	System.out.println("Message autor: "+username);
    	System.out.println("Message content: "+message);
    	System.out.print("Message date: "+timeString(timestamp));
    	if(likes != 0) System.out.print(" - "+likes+" people like this.");
    	System.out.println();
    	if(comments.size() > 0) {
    		System.out.println("\t"+comments.size()+" comment(s).");
    		for(int i=0; i<comments.size(); i++) {
    			System.out.println("\t\t"+(i+1)+". "+comments.get(i));    			
    		}
    	}else {
    		System.out.println("\tNo comments.");
    	}
    	System.out.println();
    }
   
}
