package pa.network;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Post {
	
	protected String username;
	protected long timestamp;
	protected int likes;
	protected ArrayList<String> comments;
	
	public Post(String username) {
		this.username = username;
		this.comments = new ArrayList<String>();
	}
	
	public void like() {
		likes++;
	}
	
	public void unlike() {
		if (likes > 0) {
            likes--;
        }
	}
	
	public void addComment(String text) {
		comments.add(text);
	}
	
	public String getTimeStamp() {
		String date = new SimpleDateFormat("H:mm:ss").format(new Date(timestamp));
        return date;
	}
	
	// Sobreescritos por las clases hijas
	public void display() {}
	
	protected String timeString(long time) {
		long current = System.currentTimeMillis();
        long pastMillis = current - time;      // time passed in milliseconds
        long seconds = pastMillis/1000;
        long minutes = seconds/60;
        if(minutes > 0) {
            return minutes + " minutes ago";
        }
        else {
            return seconds + " seconds ago";
        }
	}
	
}
