package pa;

import pa.network.MessagePost;
import pa.network.NewsFeed;
import pa.network.PhotoPost;
import pa.network.Post;

public class Demo2 {

	public static void main(String[] args) {
		
		NewsFeed news = new NewsFeed();
		
		Post p1 = new MessagePost("Mario","Me voy a la uni");
		Post p2 = new PhotoPost("Juan","src/main/resources/imagenes/leopardo.jpg","Leopardo enfadado");
		
		p1.like();
		p1.like();
		p1.like();
		p1.unlike();
		
		p2.addComment("Que foto mas chula!");
		p2.addComment("Pues a mi no me gusta mucho");
		
		news.addPost(p1);
		news.addPost(p2);
		
		news.show();
		System.out.println();
		
		for(int i=1; i<=7; i++) {
			p1.addComment("commentario"+i);
		}
		for(int i=1; i<=18; i++) {
			p1.like();
		}
		for(int i=1; i<=25; i++) {
			p1.unlike();
		}
		
		for(int i=1; i<=10; i++) {
			p2.addComment("comentarioPhoto"+i);
		}
		for(int i=1; i<=35; i++) {
			p2.like();
		}
		for(int i=1; i<=4; i++) {
			p2.unlike();
		}
		
		news.show();
		

	}

}
