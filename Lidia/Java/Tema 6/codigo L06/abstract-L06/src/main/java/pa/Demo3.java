package pa;
import pa.network.Post;
import pa.network.MessagePost;
import pa.network.NewsFeed;
import pa.network.PhotoPost;

public class Demo3 {
	
	public static void main(String[] args) {
		
		NewsFeed n1 = new NewsFeed();
		
		Post p1 = new PhotoPost("Roberto","src/main/resources/imagenes/tigre.jpeg","Imagen de un tigre");
		Post p2 = new PhotoPost("Clara","src/main/resources/imagenes/leopardo.jpeg","Os gusta mi leopardo?");
		p2.addComment("Que chulo!");
		Post p3 = new PhotoPost("Aitor","src/main/resources/imagenes/guepardo.jpeg","Este es mi guepardo");
		for(int i=0; i<20; i++) {
			p3.like();
		}
		Post p4 = new MessagePost("Gloria","Me gusto la peli de anoche");
		p4.addComment("A mi tambien");
		p4.addComment("Pues a mi no");
		
		n1.addPost(p1);
		n1.addPost(p2);
		n1.addPost(p3);
		n1.addPost(p4);
		
		n1.show();
		
	}
}
