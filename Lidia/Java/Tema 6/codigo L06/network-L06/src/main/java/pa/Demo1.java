package pa;

import pa.network.NewsFeed;
import pa.network.PhotoPost;
import pa.network.MessagePost;

public class Demo1 {

	public static void main(String[] args) {
		
		NewsFeed news = new NewsFeed();
		
		MessagePost m1 = new MessagePost("Mario","Me voy a la uni");
		PhotoPost p1 = new PhotoPost("Juan","src/main/resources/imagenes/leopardo.jpg","Leopardo enfadado");
		
		m1.like();
		m1.like();
		m1.like();
		m1.unlike();
		
		p1.addComment("Que foto mas chula!");
		p1.addComment("Pues a mi no me gusta mucho");
		
		news.addMessagePost(m1);
		news.addPhotoPost(p1);
		
		news.show();
		System.out.println();
		
		for(int i=1; i<=7; i++) {
			m1.addComment("commentario"+i);
		}
		for(int i=1; i<=18; i++) {
			m1.like();
		}
		for(int i=1; i<=25; i++) {
			m1.unlike();
		}
		
		for(int i=1; i<=10; i++) {
			p1.addComment("comentarioPhoto"+i);
		}
		for(int i=1; i<=35; i++) {
			p1.like();
		}
		for(int i=1; i<=4; i++) {
			p1.unlike();
		}
		
		news.show();
		
		
	}

}