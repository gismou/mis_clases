var medicos = [ 
    { id: 1, nombre: "Monica", apellidos: "Saez Rosa", login: "msr141", password: "123", genero: "F"},
    { id: 2, nombre: "Pablo", apellidos: "Aragon Llabata", login: "pal28", password: "456", genero: "M"},
    { id: 3, nombre: "Javier", apellidos: "Gomez Guzman", login: "jgg181", password: "789", genero: "M"}
];

var pacientes = [
    { id: 1, nombre: "Violeta", apellidos: "Baeza Ivorra", fecha_nacimiento: "21/12/1999", genero: "M", medico: 3, codigo_acceso: "10", observaciones: ""},
    { id: 2, nombre: "Lidia", apellidos: "Terres Molina", fecha_nacimiento: "09/04/2000", genero: "M", medico: 2, codigo_acceso: "20", observaciones: ""},
    { id: 3, nombre: "Maria", apellidos: "Aragon Llabata", fecha_nacimiento: "04/06/1997", genero: "M", medico: 1, codigo_acceso: "30", observaciones: ""},
    { id: 4, nombre: "Beatriz", apellidos: "Lopez Pastor", fecha_nacimiento: "24/01/2001", genero: "M", medico: 2, codigo_acceso: "40", observaciones: ""}
];

var medicamento = [
    { id: 1, nombre: "Paracetamol", descripcion: "Analgésico y antipirético, inhibidor de la síntesis de prostaglandinas periférica y central por acción sobre la ciclooxigenasa.", num_dosis: "20", importe: "3", importe_subvencionado: "2"},
    { id: 2, nombre: "Ibuprofeno", descripcion: "Medicamento que se usa para tratar la fiebre, la hinchazón, el dolor y el enrojecimiento al impedir que el cuerpo elabore sustancias que causan inflamación.", num_dosis: "15", importe: "6", importe_subvencionado: "4"},
    { id: 3, nombre: "Omeprazol", descripcion: "Inhibidor de la bomba de protones utilizado para tratar diversas condiciones relacionadas con la producción excesiva de ácido en el estómago.", num_dosis: "30", importe: "8", importe_subvencionado: "6"},
    { id: 4, nombre: "Amoxicilina", descripcion: "Antibiótico de amplio espectro utilizado para tratar diversas infecciones bacterianas.", num_dosis: "10", importe: "5", importe_subvencionado: "3"},
    { id: 5, nombre: "Loratadina", descripcion: "Antihistamínico utilizado para aliviar los síntomas de la alergia, como la picazón, la secreción nasal y los ojos llorosos.", num_dosis: "25", importe: "4", importe_subvencionado: "3"},
    { id: 6, nombre: "Aspirina", descripcion: "Analgésico, antiinflamatorio y antipirético utilizado para aliviar el dolor, reducir la inflamación y bajar la fiebre.", num_dosis: "30", importe: "7", importe_subvencionado: "5"},
    { id: 7, nombre: "Cetirizina", descripcion: "Antihistamínico utilizado para tratar los síntomas de la rinitis alérgica y la urticaria.", num_dosis: "20", importe: "5", importe_subvencionado: "4"},
    { id: 8, nombre: "Diazepam", descripcion: "Benzodiazepina utilizada para tratar la ansiedad, los trastornos del sueño y los espasmos musculares.", num_dosis: "10", importe: "9", importe_subvencionado: "7"}
];


var medicacion = [
    { medicamento: 1, paciente: 1 , fecha_asignacion: "24/07/2022", dosis: "2 capsulas", tomas: "20", frecuencia: "0.25", observaciones:""},
    { medicamento: 1, paciente: 2 , fecha_asignacion: "03/01/2023", dosis: "4 capsulas", tomas: "5", frecuencia: "0.5", observaciones:""},
    { medicamento: 2, paciente: 3 , fecha_asignacion: "12/11/2022", dosis: "1 capsula", tomas: "10", frecuencia: "1", observaciones:""},
    { medicamento: 1, paciente: 3 , fecha_asignacion: "01/10/2022", dosis: "2 capsula", tomas: "2", frecuencia: "0.5", observaciones:""},
    { medicamento: 2, paciente: 4 , fecha_asignacion: "09/09/2022", dosis: "2 capsula", tomas: "2", frecuencia: "1", observaciones:""}

];

var tomas = [
    { medicamento: 1, paciente: 1 , fecha: "24/07/2022 08:30:00"},
    { medicamento: 1, paciente: 1 , fecha: "25/07/2022 08:45:00" },
    { medicamento: 1, paciente: 1 , fecha: "26/07/2022 10:00:00"},
    { medicamento: 1, paciente: 1 , fecha: "27/07/2022 12:15:00"},
    { medicamento: 1, paciente: 1 , fecha: "28/07/2022 08:00:00"},
    { medicamento: 1, paciente: 2 , fecha: "03/01/2023 09:30:00"},
    { medicamento: 1, paciente: 2 , fecha: "04/01/2023 12:00:00"},
    { medicamento: 2, paciente: 3 , fecha: "12/11/2022 13:12:00"},
    { medicamento: 2, paciente: 3 , fecha: "13/11/2022 15:45:00"},
    { medicamento: 2, paciente: 3 , fecha: "14/11/2022 17:55:00"},
    { medicamento: 2, paciente: 3 , fecha: "15/11/2022 08:00:00"},
    { medicamento: 2, paciente: 3 , fecha: "16/11/2022 08:00:00"}
];

module.exports.medicos = medicos;
module.exports.pacientes = pacientes;
module.exports.medicamento = medicamento;
module.exports.medicacion = medicacion;
module.exports.tomas = tomas;