var rpc = require("./rpc.js");
var datos = require('./datos.js');

var medicos = datos.medicos;
var pacientes = datos.pacientes;
var medicamento = datos.medicamento;
var medicacion_datos = datos.medicacion;
var tomas = datos.tomas;



//REALIZAR UN LOGIN PARA EL PACIENTE. "FUNCION 1"
function login(codigo_acceso) {

    console.log("Esto es lo que recibo: " , codigo_acceso);

    var encontrado = false;
    var paciente = [];

    for (var i = 0; i < pacientes.length; i++){
        if(pacientes[i].codigo_acceso == codigo_acceso){
            encontrado = true;
            paciente = pacientes[i];
            return paciente;
        }
    }
    if(!encontrado){
        return null;
    }
}

//OBTENER LISTADO DE MEDICAMENTOS. "FUNCION 2"
function listadoMedicamentos(){ 

    var medicamentos = []; 
    
    for(var i = 0; i < medicamento.length; i++){
        medicamentos.push(medicamento[i]); 
    }
    return medicamentos; 
}

//OBTIENE LOS DATOS DEL MEDICO INDICADO. "FUNCION 3"
function datosMedico(idMedico){ 

    console.log("Esto el idMedico que recibo del main: ", idMedico);
    
    var medico = [];

    for(var i = 0; i < medicos.length; i++){
        if(idMedico == medicos[i].id){ 
            medico.push({id: medicos[i].id, nombre: medicos[i].nombre, apellidos: medicos[i].apellidos,genero: medicos[i].genero});         
            return medico;
        }
        
    }
    //Si no encuentra ningun medico, no entra en el if
    //y devuelve null al final.
    return null;
}

// OBTIENE UN LISTADO DE LA MEDICACION DEL PACIENTE. "FUNCION 4"
function medicacion(idPaciente){ 

    console.log("¿Cual es el id del Paciente para la medicacion? : ", idPaciente);
    
    var listaMedicacion = []; 
    
    for(var i = 0; i < medicacion_datos.length; i++){
        if(idPaciente == medicacion_datos[i].paciente){ 
            listaMedicacion.push(medicacion_datos[i]); 
        }
    }
    return listaMedicacion; 
}

//FUNCION 5
function listadoTomas(idPaciente,idMedicamento){ 

    var listaTomas = [];

    for(var i=0; i<tomas.length;i++){
        if(tomas[i].medicamento == idMedicamento && tomas[i].paciente == idPaciente){
            listaTomas.push(tomas[i]);
        }
    }

    console.log("Hola?",listaTomas);
    
    return listaTomas; 
}

//FUNCION 6
function agregarToma(idPaciente,idMedicamento){
    
    var fechaAhora = new Date();
    var dia = fechaAhora.getDate();
    var mes = fechaAhora.getMonth() + 1; // Sumamos 1 ya que los meses en JavaScript se indexan desde 0 (enero es 0)
    var anio = fechaAhora.getFullYear();
    var horas = fechaAhora.getHours();
    var minutos = fechaAhora.getMinutes();
    var segundos = fechaAhora.getSeconds();

    var fechaActual = dia.toString().padStart(2, '0') + "/" + mes.toString().padStart(2, '0') + "/" + anio + " " + horas.toString().padStart(2, '0') + ":" + minutos.toString().padStart(2, '0') + ":" + segundos.toString().padStart(2, '0');


    var tomaNueva = {medicamento:idMedicamento,paciente:idPaciente,fecha:fechaActual};
    console.log("La toma nueva es: ", tomaNueva);

    tomas.push(tomaNueva);

    console.log("Todas las tomas son: ", tomas);

    return tomaNueva;

}

//FUNCION 7
function eliminarToma(pacienteBorrar,medicamentoBorrar,fechaBorrar) {

    console.log("IdPaciente: ", pacienteBorrar);
    console.log("idMedicamento: ", medicamentoBorrar);
    console.log("Fecha a borrar: ", fechaBorrar);

    for(var r = 0; r< tomas.length;r++){
        if(tomas[r].paciente == pacienteBorrar && tomas[r].medicamento == medicamentoBorrar && tomas[r].fecha == fechaBorrar){
            console.log("La toma que vamos a eliminar es: ",tomas[r]);
            tomas.splice(r,1);
            return true;
        }  
    }

    return false;
    
}

function eliminarMedicacion(idMedicamento) {
    
    console.log("El id del medicamento es: ", idMedicamento);

    for (var i = 0; i < medicacion_datos.length; i++) {
        if (medicacion_datos[i].medicamento == idMedicamento) {
            medicacion_datos.splice(i, 1);
            break; // Agregamos break para salir del bucle después de eliminar el elemento
        }
    }

    return medicacion_datos;
}

//rpc.delay = 5000; // retrasar todas las operaciones 5 segundos

var servidor = rpc.server(); // crear el servidor RPC
var app = servidor.createApp("gestion_pacientes"); // creamos la aplicación RPC

//Registrar los procedimientos
app.register(login);
app.register(listadoMedicamentos);
app.register(datosMedico);
app.register(medicacion);
app.register(listadoTomas);
app.register(agregarToma);
app.register(eliminarToma);
app.register(eliminarMedicacion);