var express = require("express");
var app = express();

let datos = require('./datos.js');
console.log(datos, 'the json obj');

app.use("/apiMedico", express.static("../medico"));
app.use("/apiPaciente", express.static("../paciente"));
app.use(express.json()); 


// ----------- BASE DE DATOS ---------------- //

var mysql = require("mysql");
var database ={
    host:"localhost",
    user:"root",
    password:"",
    database :"hospital",
};

var conexion = mysql.createConnection(database);
console.log("Conectando con la base de datos...");
conexion.connect((err) => {
    if(err){
        console.log("Se ha producido un error al conectar a la base de datos",err);
        process.exit();
    }else{
        console.log("Base de datos conectada correctamente!!!");
    }
});

// ------------------------------------------ //


var siguientepaciente = 5;
var codigo_acceso = 50;

var medicos = datos.medicos;
var pacientes = datos.pacientes;
var medicamento = datos.medicamento;
var medicacion = datos.medicacion;
var tomas = datos.tomas;


app.post("/api/medico/:idMedico/anyadir_masivo", function(req, res){

    var medicamento = req.body.medicamento;
    var idMedico = req.params.idMedico;

    var contador = 0;

    console.log("El medicamento que recibo en servidor es: ", medicamento);
    console.log("El id del medico es: ", idMedico);

    for(var i = 0; i<pacientes.length;i++){
        if(pacientes[i].medico ==  idMedico){
            console.log(pacientes[i].id);
            var nueva_medicacion = { 
                medicamento: medicamento,
                paciente: pacientes[i].id,
                fecha_asignacion: "22/09/2022",
                dosis: 1,
                tomas: 7,
                frecuencia: "1",
                observaciones: ""
            }

            medicacion.push(nueva_medicacion);
            contador++;
        }

    }

    res.status(200).json(contador);
      
});

//OBTIENE UN ARRAY CON MEDICAMENTOS. "FUNCION 1" --> FUNCIONANDO CON BD !!!
app.get("/api/medicamento", function (req, res) {

    /* var arrayMedicamentos = [];

    for(var i = 0; i<medicamento.length;i++){

        var datosMedicamento = { 
            id: medicamento[i].id,
            nombre: medicamento[i].nombre,
            descripcion: medicamento[i].descripcion,
            num_dosis: medicamento[i].num_dosis,
            importe: medicamento[i].importe,
            importe_subvencionado: medicamento[i].importe_subvencionado
        }
        arrayMedicamentos.push(datosMedicamento);
    } */

     // --- CODIGO PARA FORMA CON BD --- //
     let sql = "select * from medicamentos";
     conexion.query(sql,(err,rows) => {
         if(err){
             console.log("Error al realizar la select",err);
             res.status(500).json("Error al realizar la consulta");
         }else{
            console.log("Medicamentos:",rows);
            res.status(200).json(rows);
         }
     })
     // -------------------------------- //

    //res.status(200).json(arrayMedicamentos);
});

//COMPRUEBA CREDENCIALES DEL MEDICO (POST). "FUNCION 2" --> FUNCIONANDO CON BD !!!
app.post("/api/medico/login", function (req, res) {

    var usuario = req.body.usuario; //Capturamos el usuario del body que se recibe en el req
    var contra = req.body.contra; //capturamos la contraseña que se recibe por el body del req

    //var encontrado = false;
    /* var i = 0; */

   /*  while(i<medicos.length){
        if(medicos[i].login == usuario && medicos[i].password == contra && !encontrado){ //Recorremos el array de medicos y comprobamos si existe el usuario y la cointraseña (y si coinciden)
            encontrado = true; // Si lo encontramos, dejamos de buscar y ya no entra en el if
            res.status(201).json(medicos[i].id);
        }
        i++;
    } */

    // --- CODIGO PARA FORMA CON BD --- //
    let sql = "select id from medicos where login = '"+usuario+"' && password = '"+contra+"'";
    conexion.query(sql,(err,rows) => {
        if(err){
            console.log("Error al realizar la select",err);
            res.status(500).json("Error al realizar la consulta");
        }else{
            if(rows != null && rows != undefined && rows.length == 1){
                res.status(201).json(rows[0].id);
            }else{
                res.status(403).json("No se ha encontrado en la base de datos");
            }
        }
    })
    // -------------------------------- //

});

//OBTIENE LOS DATOS DEL PACIENTE (GET) [SIN EL CODIGO DE ACCESO] "FUNCION 3"
app.get("/api/paciente/:id_paciente", function (req, res) {
   
    var id_paciente = req.params.id_paciente;
    /* var arrayPaciente = [];

    for (var i = 0; i < pacientes.length; i++) {
        if (pacientes[i].id == id_paciente) {
            var datosPaciente = {
                nombre: pacientes[i].nombre,
                apellidos: pacientes[i].apellidos,
                fecha_nac: pacientes[i].fecha_nacimiento,
                genero: pacientes[i].genero,
                id_medico: pacientes[i].medico,
                observaciones: pacientes[i].observaciones
            }
            arrayPaciente.push(datosPaciente);
        }
    } */
   /*  res.status(200).json(arrayPaciente);
    return; */

    // --- CODIGO PARA FORMA CON BD --- //
    let sql = "select id,nombre,apellidos,fecha_nacimiento,genero,medico,observaciones from pacientes where id = '"+id_paciente+"'";
    conexion.query(sql, (err,rows)=>{
        if(err){
            console.log("Error al realizar la select",err);
            res.status(500).json("Error al realizar la consulta");
        }else{
            if(rows.length == 1){
                console.log("Paciente:",rows);
                res.status(201).json(rows);
            }
        }
    })

    // -------------------------------- //

});


//OBTIENE LOS DATOS DEL MEDICO (GET) [NO DEVOLVER LA CONTRASEÑA] "FUNCION 4" --> FUNCIONANDO CON BD !!!
app.get("/api/medico/:id_medico", function (req, res) {
    
    var id = req.params.id_medico;

    /* var arrayMedico=[];
    console.log(datos); */

    /* for(var i = 0; i<medicos.length;i++){
        if(medicos[i].id == id){
            var datosMedico = { 
                id : medicos[i].id,
                nombre : medicos[i].nombre,
                apellidos : medicos[i].apellidos,
                login : medicos[i].login
            }
            arrayMedico.push(datosMedico);
        }
    }

    res.status(200).json(arrayMedico);
    return; */

    // --- CODIGO PARA FORMA CON BD --- //

    let sql = "select id,nombre,apellidos,genero from medicos where id = '"+id+"'";
    conexion.query(sql, (err,rows)=>{
        if(err){
            console.log("Error al realizar la select",err);
            res.status(500).json("Error al realizar la consulta");
        }else{
            if(rows.length == 1){
                console.log("Medico:",rows);
                res.status(201).json(rows);
            }
        }
    })

    // -------------------------------- //
});


//OBTIENE UN ARRAY CON LOS DATOS DE SUS PACIENTES (GET) "FUNCION 5" --> FUNCIONANDO CON BD !!!
app.get("/api/medico/:id/paciente", function (req, res) {
    var id_medico = req.params.id;
    console.log("id_medico",id_medico);
    /* var arrayPacientes = [];

    for (var i = 0; i < pacientes.length; i++) {
        if (pacientes[i].medico == id_medico) {
            var datosPaciente = {
                id: pacientes[i].id,
                nombre: pacientes[i].nombre,
                apellidos: pacientes[i].apellidos,
                fecha_nacimiento: pacientes[i].fecha_nacimiento,
                genero: pacientes[i].genero,
                medico: pacientes[i].medico,
                observaciones: pacientes[i].observaciones
            };
            arrayPacientes.push(datosPaciente);
        }
    }
    res.status(200).json(arrayPacientes); */

    // --- CODIGO PARA FORMA CON BD --- //
    let sql = "select id,nombre,apellidos,fecha_nacimiento,genero,observaciones from pacientes where medico = '"+id_medico+"'";
    console.log(sql);
    conexion.query(sql, (err,rows)=>{
        if(err){
            console.log("Error al realizar la select",err);
            res.status(500).json("Error al realizar la consulta");
        }else{
            if(rows.length > 0){
                console.log("Paciente:",rows);
                res.status(201).json(rows);
            }
        }
    })

    // -------------------------------- //
});

//CREA UN NUEVO PACIENTE (POST) "FUNCION 6" --> FUNCIONANDO CON BD !!!
app.post("/api/medico/:id/pacientes", function (req, res) {

    var id_medico = req.params.id;

    var paciente_creado = {
        //id: siguientepaciente,
        nombre: req.body.nombre,
        apellidos: req.body.apellidos,
        fecha_nacimiento: req.body.fecha_nacimiento,
        genero: req.body.genero,
        codigo_acceso : codigo_acceso,
        observaciones : req.body.observaciones,
        medico: id_medico
    };

    //pacientes.push(paciente_creado);
    //siguientepaciente++;
    //codigo_acceso = codigo_acceso + 10;
    
    // ------------------ CÓDIGO BD ----------------------------- //

    // -- Obtener el mayor código de acceso de los pacientes
    let sql = "select max(codigo_acceso) as maximo from pacientes";
    conexion.query(sql,(err1,rows) => {
        if(err1){
            console.log("Error al realizar la select",err1);
            res.status(500).json("Error al realizar la consulta");
        }else{
            if(rows.length = 1){
                console.log("mayor codigo_acceso de paciente = "+rows[0].maximo);
                let codigo_acceso = rows[0].maximo + 10;

                // -- Insertar nuevo paciente en la BD
                let sql2 = "insert into pacientes(nombre,apellidos,fecha_nacimiento,genero,medico,codigo_acceso,observaciones)values('"+paciente_creado.nombre+"','"+paciente_creado.apellidos+"','"+paciente_creado.fecha_nacimiento+"','"+paciente_creado.genero+"','"+paciente_creado.medico+"',"+codigo_acceso+",'"+paciente_creado.observaciones+"')";
                conexion.query(sql2,(err2,nuevoPaciente) => {
                    if(err2){
                        console.log("Error al realizar la select",err2);
                        res.status(500).json("Error al realizar la consulta");
                    }else{
                        console.log("Paciente insertado:",nuevoPaciente);
                        console.log("ID_Paciente_insertado:",nuevoPaciente.insertId);
                        res.status(201).json(nuevoPaciente.insertId);
                    }
                });
            }
        }
    })

    // --------------------------------------------------------- //

    //res.status(201).json(paciente_creado);
});


//MODIFICAR PACIENTE (PUT) "FUNCION 7" --> FUNCIONANDO CON BD !!!
app.put("/api/paciente/:id", function (req, res) {
   
    var id_paciente = req.params.id;

    var paciente = {
        id: id_paciente, 
        nombre: req.body.nombre, 
        apellidos: req.body.apellidos,
        fecha_nacimiento: req.body.fecha_nacimiento,
        genero: req.body.genero,
        medico: req.body.medico,   
        observaciones: req.body.observaciones
    }

    /* for (var i = 0; i < pacientes.length; i++) {
        if (pacientes[i].id == id_paciente) {

          console.log("Vamos a modificar a", pacientes[i].nombre);
      
          if (paciente.nombre !== "") {
            pacientes[i].nombre = paciente.nombre;}
          if (paciente.apellidos !== "") {
            pacientes[i].apellidos = paciente.apellidos;
          }
          if (paciente.fecha_nacimiento !== "") {
            pacientes[i].fecha_nacimiento = paciente.fecha_nacimiento;
          }     
          if (paciente.genero !== "") {
            pacientes[i].genero = paciente.genero;
          }      
          if (paciente.medico !== "") {
            pacientes[i].medico = paciente.medico;
          }      
          if (paciente.observaciones !== "") {
            pacientes[i].observaciones = paciente.observaciones;
          }
        }
      }
    console.log(paciente);
    res.status(201).json("Hecho"); */

    
    // ------------------ CÓDIGO BD ----------------------------- //

    let sql = "UPDATE pacientes SET nombre = '"+paciente.nombre+"', apellidos = '"+paciente.apellidos+"', fecha_nacimiento = '"+paciente.fecha_nacimiento+"', genero = '"+paciente.genero+"', medico = '"+paciente.medico+"', observaciones = '"+paciente.observaciones+"' WHERE id = '"+paciente.id+"'";
    console.log(sql);
    conexion.query(sql,(err,rows) => {
        if (err) {
            console.log("Error al realizar el insert", err);
            res.status(500).json("Error al realizar la insercion");
        }else{
            console.log("Paciente actualizado:", rows);
            res.status(201).json("Paciente actualizado");
        }
    })  

    // --------------------------------------------------------- //

});


app.get("/api/pacientes", function (req, res) {
    res.status(200).json(pacientes);
});

//OBTENER MEDICACION DE UN PACIENTE CONCRETO "FUNCION 8" --> FUNCIONANDO CON BD !!!
app.get("/api/paciente/:id/medicacion", function (req, res) {

    var id_paciente = req.params.id;
    console.log("El id del paciente que estas buscando es: " + id_paciente);

   /*  var medicacion_paciente=[];

    for (var i = 0; i <medicacion.length;i++){
        if(medicacion[i].paciente == id_paciente){
            medicacion_paciente.push(medicacion[i]);
        }
        
    }
    res.status(200).json(medicacion_paciente); */

    // ------------------ CÓDIGO BD ----------------------------- //

    let sql = "SELECT medicamento, fecha_asignacion, dosis, tomas, frecuencia, observaciones FROM medicacion WHERE paciente = '"+id_paciente+"'";
    conexion.query(sql, (err,rows) => {
        if(err){
            console.log("Error al realizar la select",err);
            res.status(500).json("Error al realizar la consulta");
        }else{
            if(rows.length > 0){
                res.status(201).json(rows);
            }else{
                console.log("No se ha encontrado resultado");
                res.status(404).json(0);
            }
        }
    })

    // --------------------------------------------------------- //
});

//ASIGNA UNA NUEVA MEDICACION A ESE PACIENTE "FUNCION 9" --> FUNCIONANDO CON BD !!!
app.post("/api/paciente/:id/medicacion", function (req, res) {

    var id_paciente = req.params.id;
    var fecha = new Date();

    var dia = fecha.getDate();
    var mes = fecha.getMonth() + 1; // Se suma 1 porque los meses en JavaScript comienzan desde 0 (enero es el mes 0)
    var anio = fecha.getFullYear();

    // Formatear el día y el mes con dos dígitos si es necesario
    if (dia < 10) {
    dia = '0' + dia;
    }
    if (mes < 10) {
    mes = '0' + mes;
    }

    var fechaActual = dia + '/' + mes + '/' + anio;

    var nueva_medicacion = {
        medicamento: req.body.medicamento,
        paciente: id_paciente,
        fecha_asignacion: fechaActual,
        dosis: req.body.dosis,
        tomas: req.body.tomas,
        frecuencia : req.body.frecuencia,
        observaciones : req.body.observaciones
    };

    /* medicacion.push(nueva_medicacion);
    res.status(201).json(nueva_medicacion); */

    /* ------------------- HECHO PARA BD ------------------------- */

    let sql = "INSERT INTO medicacion(medicamento,paciente,fecha_asignacion,dosis,tomas,frecuencia,observaciones) VALUES('"+nueva_medicacion.medicamento+"','"+nueva_medicacion.paciente+"','"+nueva_medicacion.fecha_asignacion+"','"+nueva_medicacion.dosis+"','"+nueva_medicacion.tomas+"','"+nueva_medicacion.frecuencia+"','"+nueva_medicacion.observaciones+"')";
    conexion.query(sql,(err,rows) => {
        if(err){
            console.log("Error al insertar en la BD",err);
            res.status(500).json(err);
        }else{
            console.log("Medicación insertada:",rows);
            res.status(201).json(rows.insertId);
        }
    })

    /*------------------------------------------------------------ */

});

//OBTENER LAS TOMAS DE UN PACIENTE PARA UN MEDICAMENTO "FUNCION 10"
app.get("/api/paciente/:id/medicacion/:idm", function (req, res) {

    var id_medicamento = req.params.idm;
    console.log("Este es el id del medicamento:", id_medicamento);
    var id_paciente = req.params.id;
    console.log("Este es el id del paciente: ", id_paciente);

    /* var tomas_medicamento=[];

    for(var i = 0; i<tomas.length;i++){
        if(id_paciente == tomas[i].paciente && id_medicamento == tomas[i].medicamento){
            tomas_medicamento.push(tomas[i]);
        }
    } */

    //res.status(201).json(tomas_medicamento);

    /* ------------------- HECHO PARA BD ------------------------- */

    let sql = "SELECT fecha FROM tomas WHERE medicamento = '"+id_medicamento+"' and paciente = '"+id_paciente+"'";
    conexion.query(sql, (err,rows) => {
        if(err){
            console.log("Error al realizar la select",err);
            res.status(500).json("Error al realizar la consulta");
        }else{
            if(rows.length > 0){
                res.status(201).json(rows);
            }else{
                console.log("No se ha encontrado resultado");
                res.status(404).json(0);
            }
        }
    })

    /*------------------------------------------------------------ */
    
});

//OBTENER TODOS DATOS DE LAS TOMAS DE UN PACIENTE
app.post("/api/paciente/:id", function(req,res){
    var idPaciente = req.params.id;
    let sql = "SELECT t.fecha, p.nombre as nombrePaciente, m.nombre as nombreMedicamento FROM tomas t, pacientes p, medicamentos m WHERE t.paciente = '"+idPaciente+"' AND t.paciente = p.id AND t.medicamento = m.id";
    console.log("Entro al get del servidor para obtener las tomas")
    conexion.query(sql, (err,rows) => {
        console.log("He obtenido de la consulta lo siguiente: ",rows);
        if(err){
            console.log("Error al realizar la select: ",err);
            res.status(500).json("Error al realizar la consulta");
        }else{
            if(rows.length > 0){
                res.status(201).json(rows);
            }else{
                console.log("No se ha encontrado resultado");
                res.status(404).json(0);
            }
        }
    })
})

app.get("/api/medicamento", function(req,res){//Para obtener el array de medicamentos
    res.status(200).json(medicamento);
})

//FUNCION C4 EXTRA. ELIMINAR PACIENTE
app.delete("/api/paciente/:id", function(req,res){
    var id_paciente=req.params.id;
   /*  var encontrado=false;
    for (var x=0; x<pacientes.length; x++){
        if(pacientes[x].id==id_paciente){
            pacientes.splice(x,1);
            encontrado=true;
        }
        
    } */

    // Primero borramos las medicaciones con el id del paciente:
    let sql = "delete from medicacion where paciente = '"+id_paciente+"'";
    conexion.query(sql,(err,rows) => {
        if (err) {
            console.log("Error al borrar las medicaciones del paciente");
            res.status(500).json("Error al borrar las medicaciones del paciente");
        }else{
            console.log("Medicaciones borradas:",rows);
            let sql2 = "delete from pacientes where id = '"+id_paciente+"'";
            conexion.query(sql2,(err,rows)=>{
                if(err){
                    console.log("Error al borrar el paciente", err);
                    res.status(500).json("Error al borrar el paciente");
                }else{
                    console.log("Paciente borrado:",rows);
                    res.status(201).json("Paciente y sus medicaciones borradas");
                }
            })
        }
    });

    //Tambien borramos las medicaciones con el id del paciente:
   /*  for (var y=0; y<medicacion.length; y++){
        if(medicacion[y].paciente==id_paciente){
            medicacion.splice(y,1);
        }
    }
    if (!encontrado){
        res.status(404).json(encontrado);
    }
    else{        
        res.status(200).json(encontrado);
    } */

})




app.listen(3000, function(){
    console.log("Servidor escuchando en el puerto 3000")
});