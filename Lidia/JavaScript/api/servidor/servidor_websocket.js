// Crear un servidor HTTP
var http = require("http");
const { stringify } = require("querystring");
var httpServer = http.createServer();

// Crear servidor WS
var WebSocketServer = require("websocket").server; // instalar previamente: npm install websocket
var wsServer = new WebSocketServer({
	httpServer: httpServer
});

// Iniciar el servidor HTTP en un puerto
var puerto = 4444;
httpServer.listen(puerto, function () {
	console.log("Servidor de WebSocket iniciado en puerto:", puerto);
});

var conexiones = []; // Todas las conexiones (clientes) de mi servidor
wsServer.on("request", function (request) { // este callback se ejecuta cuando llega una nueva conexión de un cliente
	var connection = request.accept("pacientes", request.origin); // aceptar conexión	
	var cliente = { connection: connection }; 
	conexiones.push(cliente)
	console.log("Cliente conectado.");
	connection.on("message", function (message) { // mensaje recibido del cliente
        if (message.type === "utf8") {
			var msg = JSON.parse(message.utf8Data);
            switch (msg.operacion){
                case 'primerMensaje':
                    cliente.persona=msg.persona;
                    cliente.id= msg.id;
                    cliente.nombre= msg.nombre;
                    break;
                case 'masMedicacion':
                    for (var i = 0; i < conexiones.length; i++) { 
						if (conexiones[i].persona=='medico' && conexiones[i].id==msg.idReceptor) {							
							conexiones[i].connection.sendUTF(JSON.stringify({tipo: 'masMedicacion', nombreMedicacion: msg.nombreMedicacion, nombre: cliente.nombre, fecha: msg.fecha})); 
						}
					}
                    break;
                case 'reaccionAdversa':
                    for (var i = 0; i < conexiones.length; i++) { 
						if (conexiones[i].persona=='medico' && conexiones[i].id==msg.idReceptor) {
							conexiones[i].connection.sendUTF(JSON.stringify({tipo: 'reaccion', nombreMedicacion: msg.nombreMedicacion, nombre: cliente.nombre, fecha: msg.fecha})); 
						}
					}

                    break;
                case 'pedirCita':
                    for (var i = 0; i < conexiones.length; i++) { 
						if (conexiones[i].persona=='medico' && conexiones[i].id==msg.idReceptor) {
							conexiones[i].connection.sendUTF(JSON.stringify({tipo: 'cita', nombre: cliente.nombre, fecha: msg.fecha, fechaCita: msg.fechaCita, horaCita: msg.horaCita})); 
						} 
					}
                    break;
				case 'mensajeParaPaciente':
					for (var i = 0; i < conexiones.length; i++) { 
						if (conexiones[i].persona=='paciente' && conexiones[i].id==msg.idReceptor) {							
							conexiones[i].connection.sendUTF(JSON.stringify({mensaje: msg.mensaje, fecha: msg.fecha})); 							
						} 
					}
					break;
            }
        }
		
	});


	connection.on("close", function (reasonCode, description) { // conexión cerrada
		conexiones.splice(conexiones.indexOf(connection), 1);
		console.log("Cliente desconectado. Ahora hay", conexiones.length);
	});
});