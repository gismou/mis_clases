// rest.get(url, callback)
// rest.post(url, body, callback)
// rest.put(url, body, callback)
// rest.delete(url, callback)
// function callback(estado, respuesta) {...}
var conexion;

var mensajes=[];

var seccionActual = "login";
var id_medico_global = 0;
var id_paciente_global = 0;
var id_medicamento_global = 0;
var listadoMedicamentos=[]; //Variable que guarda el listado de medicamentos que tiene el paciente.

function cambiarSeccion(seccion){
    document.getElementById(seccionActual).classList.remove("activa");
    document.getElementById(seccion).classList.add("activa");
    seccionActual = seccion;
}

function comprobarCampoVacio(seccion){
    var campos = document.getElementById(seccion).querySelectorAll("input");
    var selectGenero = document.getElementById("genero");
    var vacio=false;
    
    if (selectGenero.value == 'H' || selectGenero.value == 'M'){
        for (var x=0; x<campos.length-1; x++){
            if (campos[x].value==""){
                vacio=true;
            }
        } 
    }else{
        vacio = true;
    }

    return vacio;
}


//FUNCION 1. ARRAY MEDICAMENTOS (GET: API/MEDICAMENTO) --> FUNCIONANDO CON BD !!!

function verMedicamentos() { //Ver lista de los medicamentos

    rest.get("/api/medicamento", function (estado, respuesta) {

        if (estado != 200) {
            alert("Error cargando la lista de medicamentos");
            
        }
        else{
            /*
        var lista = document.getElementById("listaMedicamentos");

        lista.innerHTML = "";

        for (var i = 0; i < respuesta.length; i++) {
            lista.innerHTML +=  "<ul>" + //Cambiar estilo de estas etiquetas para maypr diferenciación.
                                "<li>" + "ID del medicamento: " + respuesta[i].id + "</li>" +
                                "</ul>"+

                                  "<ul>" +
                                  "<li>" + " Nombre del medicamento: " + respuesta[i].nombre + "</li>"
                                + "<li>" + " Descripción del medicamento: " + respuesta[i].descripcion + "</li>"
                                + "<li>" + " Numero de dosis por caja: " + respuesta[i].num_dosis + "</li>"
                                + "<li>" + " Importe del medicamento: " + respuesta[i].importe + "€" + "</li>"
                                + "<li>" + " Importe subvencionado por el estado :  " + respuesta[i].importe_subvencionado + "€" + "</li>"
                                + "</ul>" 

                                + "</br>";
        }
*/

        }

    });

}

/* ------- FUNCIÓN CREADA PARA MEJORAR EL PROGRAMA CON BD ---------------------- */

function rellenarSelectMedicamentos() {
    
    rest.get("/api/medicamento", function(estado, respuesta){
        if(estado != 200){
            alert("Error cargando el select de medicamentos");
        }else{
            const select = document.getElementById("selectMedicamento");
            for (let i = 0; i < respuesta.length; i++) {
                select.innerHTML += '<option value="'+respuesta[i].id+'">'+respuesta[i].nombre+'</option>';
            }
        }
    })

}

/* ----------------------------------------------------------------------------- */

//FUNCION 2.LOGIN PARA MEDICO (POST: API/MEDICO/LOGIN) --> FUNCIONANDO CON BD !!! 
function login() {

    var medico = {
        usuario: document.getElementById("user").value,
        contra: document.getElementById("password").value
    };

    rest.post("/api/medico/login", medico, function (estado, respuesta) {
        //En la "respuesta" recibo el id del medico que hace el login.
        if (estado == 201) {
            cambiarSeccion("menu_principal");
            id_medico_global = respuesta;       
            datosMedico(respuesta);
            listaPacientes(respuesta);
            verMedicamentos();      
              conexion = new WebSocket('ws://localhost:4444', "pacientes");//Lo conectamos al servidor websocket
            // Connection opened
            conexion.addEventListener('open', function (event) {
                console.log("Cliente conectado!!!");
                conexion.send(JSON.stringify({ operacion: "primerMensaje", persona: 'medico', id: id_medico_global, nombre:  nombreCompleto}));
          
            });
            conexion.addEventListener('message', function (event) {
                var mensaje=JSON.parse(event.data);             
                switch(mensaje.tipo){
                    case "masMedicacion":
                    mensajes.push({nombrePaciente: mensaje.nombre, fecha: mensaje.fecha, mensaje: 'No tengo más '+mensaje.nombreMedicacion+' y necesito una nueva receta'});
                        break;
                                    
                    case "reaccion":
                        mensajes.push({nombrePaciente: mensaje.nombre, fecha: mensaje.fecha, mensaje: 'Me sienta mal el '+mensaje.nombreMedicacion});
                        break;		
                    case "cita":
                        mensajes.push({nombrePaciente: mensaje.nombre, fecha: mensaje.fecha, mensaje: 'Quiero una cita para el dia '+mensaje.fechaCita+" a las "+mensaje.horaCita});
                        break;	  
                           
                }
                verMensajes();
                
            });
            conexion.addEventListener("close", function () {
                console.log("Desconectado del servidor!!!");
            });

            conexion.addEventListener("error", function () {
                console.log("Error con la conexión!!!");
            });
      

        } else {
            alert("La autenticación ha fallado");
        }
    });
}

var datosPaciente_global; //Variable global de los datos del paciente

//FUNCION 3.OBTENER DATOS DEL PACIENTE (GET: API/PACIENTE/:ID)  --> FUNCIONANDO CON BD !!! 
function datosPaciente(id_paciente){
    pagina_datos_paciente();//Para cambiar de sección.
    obtenerMedicacion(id_paciente);

    id_medicamento_global =  document.getElementById("id_medicamento");
    
    rest.get("/api/paciente/" + id_paciente, function (estado, arrayPaciente) {        
        lista = document.getElementById("datosPaciente");
        lista.innerHTML = "";
        
        if (estado == 201) {
                console.log(arrayPaciente);
                for(var i = 0; i<arrayPaciente.length;i++){
                    lista.innerHTML += "<li>" + "Nombre : " +  arrayPaciente[i].nombre +"</li>"
                                        + "<li >" + "Apellidos : " +  arrayPaciente[i].apellidos + "</li>"
                                        + "<li>" + "Fecha de Nacimiento : " +  arrayPaciente[i].fecha_nac + "</li>"
                                        + "<li>" + "Genero : " +  arrayPaciente[i].genero + "</li>"
                                        + "<li>" + "Id de su medico : " +  id_medico_global + "</li>"
                                        + "<li>" + "Observaciones : " +  arrayPaciente[i].observaciones + "</li>"
                                        + "<button onclick='pagina_modificar_paciente("+id_paciente+")' class='centrar modificar'>Modificar los datos del paciente</button>"
                                        + "</br>";
                                        datosPaciente_global={nombre: arrayPaciente[i].nombre, apellidos: arrayPaciente[i].apellidos, fecha_nacimiento: arrayPaciente[i].fecha_nac
                                        ,genero: arrayPaciente[i].genero, observaciones: arrayPaciente[i].observaciones};
                                        
                }
            
        id_paciente_global = id_paciente;
        }
        else{
            alert("Algo falla");
        }

    });

}

//FUNCION 4.OBTENER DATOS DEL MEDICO (GET: API/MEDICO/:ID) --> FUNCIONANDO CON BD !!!
function datosMedico(id_medico) { //Ver lista de los medicamentos

    rest.get("/api/medico/" + id_medico, function (estado, arrayMedico) {

        lista = document.getElementById("datosMedico");
        lista.innerHTML = "";

        bienvenida = document.getElementById("datosMedico");
        bienvenida.innerHTML = "";

        /* console.log("Esto se obtiene se los datos del medico",arrayMedico);
        
        if (estado == 200) {
            for(var i = 0; i<arrayMedico.length;i++){
                lista.innerHTML += "<h2>" + "BIENVENIDO/A A SU PORTAL DOCTOR/A : " +  arrayMedico[i].nombre + " " +  arrayMedico[i].apellidos + "</h2>";
                nombreCompleto=arrayMedico[i].nombre + " " +  arrayMedico[i].apellidos;
            }

        }else{
            alert("Algo falla");
        } */

        // --- CODIGO PARA FORMA CON BD --- //
        if (estado == 201) {
            lista.innerHTML += "<h2>" + "BIENVENIDO/A A SU PORTAL DOCTOR/A : " +  arrayMedico[0].nombre + " " +  arrayMedico[0].apellidos + "</h2>";
            nombreCompleto=arrayMedico[0].nombre + " " +  arrayMedico[0].apellidos;
        }else{
            alert("Algo ha salido mal");
        }

        // -------------------------------- //
    });

}

var nombreCompleto;

//FUNCION 5.OBTENER LOS DATOS DEL PACIENTE (GET: API/MEDICO/:ID/PACIENTES) --> FUNCIONANDO CON BD !!!
function listaPacientes() { //Ver lista de los pacientes de un médico
    rest.get("/api/medico/" + id_medico_global + "/paciente", function (estado, pacientes) {
        /* if (estado != 200) {
            alert("Error cargando la lista de pacientes");
            return;
        } */

        if (estado != 201) {
            alert("Error cargando la lista de pacientes!");
            return;
        }   

        lista = document.getElementById("listaPacientes");
        lista.innerHTML="";
        //Ponemos los encabezados de la tabla:
        var encabezados=document.createElement("tr");
        encabezados.innerHTML = "<tr><td> Nombre </td><td> Apellidos </td> <td>Fecha nacimiento</td><td>Genero</td><td>Observaciones</td><td></td><td></td></tr>";
        lista.appendChild(encabezados);
        if (pacientes.length==0){
            lista.innerHTML+="<tr><td colspan=7>No tiene pacientes </td></tr>"
            
        }
        else{
            var selectPaciente= document.getElementById("selectPaciente"); 
            selectPaciente.innerHTML="<option value=''>Seleccione paciente</option>";
            for(var t = 0; t<pacientes.length;t++){
                var fila=document.createElement("tr");

                fila.innerHTML= "<td>"+pacientes[t].nombre+ "</td>"
                                + " <td>"+ pacientes[t].apellidos+"</td>"
                                + "<td>"+pacientes[t].fecha_nacimiento+"</td>" 
                                + "<td>"+pacientes[t].genero+"</td>"
                                + "<td>"+pacientes[t].observaciones+"</td>"
                                + "<td> <button id="+pacientes[t].id+" onclick='datosPaciente(this.id)'> Expediente </button></td>"
                                + "<td> <button id="+pacientes[t].id+" onclick='eliminarPaciente(this.id)'> Eliminar paciente </button> </td>";
                lista.appendChild(fila);
                                        
                var opcion = document.createElement("option");
                opcion.text = pacientes[t].nombre+ " "+ pacientes[t].apellidos;
                opcion.value = pacientes[t].id;
                opcion.id= "option"+pacientes[t].id;
                selectPaciente.appendChild(opcion);
                                
            } 

        }

        menu_principal(); 
        verMensajes();
    });

}


//FUNCION 6. CREA UN NUEVO PACIENTE (POST:API/MEDICO/:ID/PACIENTES) --> FUNCIONANDO CON BD !!!
function nuevo_paciente() {
    if (comprobarCampoVacio("nuevo_paciente")){
        alert("Debe completar todos los campos");
    }
    else{
        var nuevo_paciente = {
            nombre: document.getElementById("nombre").value,
            apellidos: document.getElementById("apellidos").value,
            fecha_nacimiento : document.getElementById("fecha_nacimiento").value,
            genero : document.getElementById("genero").value,
            observaciones: document.getElementById("observaciones").value
        };
    
        //Visualizamos info de los campos del html
        /* console.log("La fecha de nacimiento del nuevo paciente es: " + nuevo_paciente.nacimiento); */
        rest.post("/api/medico/" + id_medico_global + "/pacientes", nuevo_paciente, function (estado, respuesta) {
            console.log(respuesta);
            if (estado == 201) {  
                listaPacientes();
                limpiar();
            } else {
                alert("Error creando el nuevo paciente");
            }
        });

    }


    
}


//FUNCION 7. MODIFICA LOS DATOS DE UN PACIENTE (PUT:API/PACIENTE/:ID)  --> FUNCIONANDO CON BD !!!
function modificarPaciente(id_paciente_global) {

    console.log("El id_paciente es: ", id_paciente_global);

    var paciente = {
        medico : id_medico_global,
        nombre: document.getElementById("nombre_modificado").value,
        apellidos: document.getElementById("apellidos_modificado").value,
        fecha_nacimiento: document.getElementById("fecha_nacimiento_modificado").value,
        genero: document.getElementById("genero_modificado").value,
        observaciones: document.getElementById("observaciones_modificado").value
    };

    rest.put("/api/paciente/" + id_paciente_global, paciente, function (estado, pac) {       
        if (estado == 201) {
            listaPacientes();
            limpiar_modificado();
        } else {
            alert("Error introduciendo los datos");
        }
    });
}

//FUNCION 8. OBTENER MEDICACION PACIENTE (POST:API/PACIENTE/:ID/MEDICACION)
function obtenerMedicacion(id_paciente) { //Ver lista de los medicamentos
    document.getElementById("ver_medicacion_paciente").classList.add("activa");

    rest.get("/api/paciente/" + id_paciente + "/medicacion", function (estado, respuesta) {
        var divMedicamentos = document.getElementById("ver_medicacion_paciente");
        divMedicamentos.innerHTML = "";

        if (estado != 201) {
            if (respuesta == 0){
                //Si no tiene medicamentos
                divMedicamentos.innerHTML="<p> El paciente no tiene medicamentos<p><br><button onclick='menu_principal()'>Volver al menú principal</button><button onclick='pagina_anyadir_medicacion_paciente()'>Añadir medicación</button>";
            }else{
                alert("Error cargando la lista de medicamentos");
            }
            return;
        }else{
            // Si tiene medicamentos
            rest.get("/api/medicamento", function(response, medicamentos){

                if(response!=200){
                    alert("Ha habido un error")
                }
                else{
                    var tabla=document.createElement("table");
                    tabla.innerHTML="<tr><td>Medicación</td><td>Fecha de asignación</td><td>Dosis</td><td>Tomas</td><td>Frecuencia</td><td>Observaciones</td><td></td></tr>"
                    divMedicamentos.innerHTML="<h2>Lista de medicamentos</h2>";
                    divMedicamentos.appendChild(tabla);
                    var agregarMedicacion= document.createElement("button");
                    var volverMenuPrincipal=document.createElement("button");
                    volverMenuPrincipal.setAttribute("onclick","menu_principal()");
                    volverMenuPrincipal.innerHTML="Volver al menú principal";
                    agregarMedicacion.setAttribute("onclick","pagina_anyadir_medicacion_paciente()");
                    agregarMedicacion.setAttribute("class","centrar");
                    agregarMedicacion.setAttribute("class", "espacio")
                    agregarMedicacion.innerHTML="Agregar medicación";
                    divMedicamentos.appendChild(agregarMedicacion);
                    divMedicamentos.appendChild(volverMenuPrincipal);
                    
                    for (var x=0; x<respuesta.length; x++){
                        var fila = document.createElement("tr");                        
                        for(var y=0; y<medicamentos.length; y++){
                            if(medicamentos[y].id==respuesta[x].medicamento){
                                var idToma=medicamentos[y].id;
                                medicamentos_usados_global.push(medicamentos[y].nombre);
                                fila.innerHTML="<td>"+medicamentos[y].nombre+"</td>";
                            }                  

                        }
                        fila.innerHTML += "<td>" + respuesta[x].fecha_asignacion + "</td>" +
                                            "<td>" + respuesta[x].dosis + "</td>" +
                                            "<td>" + respuesta[x].tomas + "</td>" +
                                            "<td>" + respuesta[x].frecuencia + "</td>" +
                                            "<td>" + respuesta[x].observaciones + "</td>" +
                                            "<td><button id='" + idToma + "' onclick='pagina_tomas(); obtener_tomas(this.id)'>Ver tomas de este medicamento</button></td>";

                        tabla.appendChild(fila);
                    } 
                }
                
            })     


            

        }

    });

}

//FUNCION 9. ASIGNAR MEDICACION (POST:API/PACIENTE/:ID/MEDICACION)
function asignar_medicacion() {

    console.log("El id global del paciente es: " +  id_paciente_global);

    var nueva_medicacion = {
        medicamento: document.getElementById("selectMedicamento").value,
        dosis : document.getElementById("dosis").value,
        tomas : document.getElementById("tomas").value,
        frecuencia : document.getElementById("frecuencia").value,
        observaciones: document.getElementById("observaciones_medicacion").value
    };

    //Visualizamos info de los campos del html
    console.log("La frecuencia de la nueva med es: ",nueva_medicacion.frecuencia);
        
    rest.post("/api/paciente/" + id_paciente_global + "/medicacion", nueva_medicacion, function (estado, respuesta) {
        if (estado == 201) {
            console.log("ESTO ES LO QUE RECIBO DEL SERVIDOR DE LA MEDICACION: ",respuesta);
            datosPaciente(id_paciente_global)
            pagina_datos_paciente();
        } else {
            alert("Error añadiendo la medicacion");
        }
    });
    
}


//FUNCION 10. OBTENER TOMAS (GET:API/PACIENTE/:ID/MEDICACION/:IDM)
function obtener_tomas(id_medicamento) {
    console.log(id_medicamento);
    rest.get("/api/paciente/" + id_paciente_global + "/medicacion/" + id_medicamento, function (estado, respuesta) {
        
        console.log("Las tomas son: ", respuesta);

        if (estado != 201) {
            alert("Error viendo las tomas");
        } 
        
        lista = document.getElementById("listaTomas");
        lista.innerHTML = "";
        if (respuesta.length==0){
            lista.innerHTML+="<p> No hay tomas para esta medicación</p>";
        }

        for(var t = 0; t<respuesta.length;t++){
            lista.innerHTML += "<li>" + "Fecha de las tomas : " +  respuesta[t].fecha + "</li>";                
        } 
    
        lista.innerHTML += "<button onclick='pagina_datos_paciente()'>Volver</button>";
           
    });
    
}

//FUNCION 11. OBTENER MEDICAMENTOS (10 PRIMEROS RESULTADOS) DE (https://cima.aemps.es/cima/rest/medicamentos?nombre=:medicamento)
function buscarMedicamentos(){
    // Declarar y obtener variables con los nodos DOM que necesitamos de la página
    //let input = document.getElementById("input_buscar_medicamentos");
    let div = document.getElementById("div_buscar_medicamentos");

    // Obtener nombre del medicamento a buscar de la id del input
    let nombreMedicamento = document.getElementById("input_buscar_medicamentos").value;

    // Comprobar que el input tiene valor
    if(nombreMedicamento.length == 0 || nombreMedicamento == "" || nombreMedicamento == null) alert("Introduce un nombre de medicamento");
    else{
        // Realizamos un fetch a la api para obtener los resultados
        fetch('https://cima.aemps.es/cima/rest/medicamentos?nombre='+nombreMedicamento)
        .then(response => response.json())    // a fetch le llega una respuesta en string que tiene que ser parseada a JSON
        .then(data => {
          // Comprobamos que hayamos obtenido algún resultado
          if(data.totalFilas > 0){
            // Si obtenemos resultados los recorremos y montamos una tabla con hasta 10 de los primeros resultados
            let codigoTabla = "<table><tr><th>Nº REGISTRO</th><th>NOMBRE</th></tr>"
            console.log(data);
            
            for (var i in data.resultados){
              if(i < 10 && data.resultados[i].nombre != null){
                console.log(data.resultados[i].nombre);
                codigoTabla += "<tr><td>"+data.resultados[i].nregistro+"</td><td>"+data.resultados[i].nombre+"</td></tr>";
              }
            }
            codigoTabla += "</table>";
            div.innerHTML = codigoTabla;
          }else{
            // Si no obtenemos resultados lo indicamos con un mensaje por pantalla
            div.innerHTML = "<p class='centrar'>No se han encontrado resultados</p>";
          }
          
        })
        .catch(error => console.error(error));
    }
} 

//FUNCION 12. MANDAR TODAS LAS TOMAS DE LA BD AL SERVIDOR DE PROFESOR (https://undefined.ua.es/telemedicina/api/datos)
function mandarTomas(){
    console.log("Entro a MandarTomas");
    // Obtener todas las tomas y datos del paciente
    rest.post("/api/paciente/" + id_paciente_global,function(estado,respuesta){
        if(estado != 201){
            alert("Error al obtener las tomas");
        }

        console.log("Obtengo de respuesta: ",respuesta);

        // Montar Objeto json para mandar con todas las tomas
        var todasTomas = [{
            "id":0,
            "id_area":1,
            "fecha": "",
            "datos": []
        }];
        for(var i = 0; i<respuesta.length; i++){
            console.log("respuesta iterada: ",respuesta[i]);
            todasTomas[0].datos.push({
                "paciente": respuesta[i].nombrePaciente,
                "medicamento": respuesta[i].nombreMedicamento,
                "fecha": respuesta[i].fecha
            })
        }

        console.log(todasTomas);

    })


    // Enviar con post
   /*  rest.post("https://undefined.ua.es/telemedicina/api/datos",todasTomas, function (estado, respuesta) {
        if (estado == 201) {
            console.log("ESTO ES LO QUE RECIBO DEL SERVIDOR DE LA MEDICACION: ",respuesta);
            datosPaciente(id_paciente_global)
            pagina_datos_paciente();
        } else {
            alert("Error añadiendo la medicacion");
        }
    }); */
}

//FUNCION ADICIONAL C4. ELIMINAR PACIENTE
function eliminarPaciente(idPaciente){
    console.log(idPaciente)

    rest.delete("/api/paciente/"+idPaciente,function(estado, ok){
        if(estado!=201){
            alert("Ha ocurrido un error "+ok);         
        }
        else{
            datosMedico(id_medico_global);
            listaPacientes(id_medico_global);
        }

    })
}

//CAMBIAR EL NOMBRE DE TODAS LAS FUNCIONES
function menu_principal() {
    medicamentos_usados_global=[]
    cambiarSeccion("menu_principal");
}

function menu_login() {
    mensajes=[];
    conexion.close();
    limpiar_login();
    cambiarSeccion("login");
}


function pagina_nuevo_paciente(){
    cambiarSeccion("nuevo_paciente");
}

function pagina_modificar_paciente(){
    document.getElementById("nombre_modificado").value=datosPaciente_global.nombre;
    document.getElementById("apellidos_modificado").value=datosPaciente_global.apellidos;
    document.getElementById("fecha_nacimiento_modificado").value=datosPaciente_global.fecha_nacimiento;
    document.getElementById("genero_modificado").value=datosPaciente_global.genero;
    document.getElementById("observaciones_modificado").value=datosPaciente_global.observaciones;
    cambiarSeccion("modificar_paciente");
}

function pagina_datos_paciente(){
    cambiarSeccion("ver_datos_paciente");
}

var medicamentos_usados_global=[]; //Variable que guarda el nombre de los medicamentos que ya están usados.

function pagina_medicacion_paciente(){

    cambiarSeccion("ver_medicacion_paciente");
}

function pagina_anyadir_medicacion_paciente() {
    rest.get("/api/medicamento", function (estado, medicamentos) {
        if (estado != 200) {
            alert("Error cargando la lista de medicamentos");
        } else {
            /* var select = document.getElementById("selectMedicamento");
            select.innerHTML=" <option value=''>Elija medicamento</option>";
            for (var i = 0; i < medicamentos.length; i++) {
                var medicamento_usado=false;
                for (var z=0; z<medicamentos_usados_global.length;z++){
                    if (medicamentos_usados_global[z]==medicamentos[i].nombre){
                        medicamento_usado=true;
                    }
                }
                if (medicamento_usado==false){                    
                    var opcion = document.createElement("option");
                    opcion.text = medicamentos[i].nombre;
                    opcion.value = medicamentos[i].id;
                    select.appendChild(opcion);
                }
            } */
            rellenarSelectMedicamentos();
            cambiarSeccion("anyadir_medicacion_paciente");
        }
    });
}


function pagina_tomas(){
    cambiarSeccion("ver_tomas_medicacion");
}

function pagina_BuscarMedicamentos(){
    cambiarSeccion("buscar_medicamentos")
}

function limpiar(){
    var limpiar = {
        nombre: document.getElementById("nombre").value = "",
        apellidos: document.getElementById("apellidos").value = "",
        fecha_nacimiento: document.getElementById("fecha_nacimiento").value= "",
        genero: document.getElementById("genero").value= "",
        observaciones: document.getElementById("observaciones").value= ""
    };
}

function limpiar_modificado(){
    var limpiar = {
        nombre: document.getElementById("nombre_modificado").value = "",
        apellidos: document.getElementById("apellidos_modificado").value = "",
        fecha_nacimiento: document.getElementById("fecha_nacimiento_modificado").value= "",
        genero: document.getElementById("genero_modificado").value= "",
        observaciones: document.getElementById("observaciones_modificado").value= ""
    };
}

function limpiar_login(){
    var limpiar_login = {
        usuario: document.getElementById("user").value = "",
        contra: document.getElementById("password").value = ""
    };
}


function verMensajes(){
    var tabla=document.getElementById("listaMensajes");
    tabla.innerHTML="<tr><th>Emisor</th><th>Receptor</th><th>Fecha mensaje</th><th>Mensaje</th></tr>";
    if (mensajes.length==0){
        tabla.innerHTML+="<tr><td colspan='5'>No tiene ningún mensaje</td></tr>"
    }
    else{
        for (var x=0; x<mensajes.length; x++){
            var fila= document.createElement("tr");
            if (mensajes[x].emisor=="medico"){
                fila.innerHTML="<td>Yo</td><td>"+mensajes[x].nombrePaciente+"</td>";
            }
            else{
                fila.innerHTML="<td>"+mensajes[x].nombrePaciente+"</td><td>Yo</td>";
            }
            fila.innerHTML+="<td>"+mensajes[x].fecha+"</td>"
                            +"<td>"+mensajes[x].mensaje+"</td>";
                            
            tabla.appendChild(fila);
        }
    }
}


function enviarMensaje(){
    var idPaciente= document.getElementById("selectPaciente").value;
    if (idPaciente!=""){
        var fechaAhora = new Date();
        var dia = fechaAhora.getDate();
        var mes = fechaAhora.getMonth() + 1; // Sumamos 1 ya que los meses en JavaScript se indexan desde 0 (enero es 0)
        var anio = fechaAhora.getFullYear();
        var horas = fechaAhora.getHours();
        var minutos = fechaAhora.getMinutes();
        var segundos = fechaAhora.getSeconds();
        var fechaActual = dia.toString().padStart(2, '0') + "/" + mes.toString().padStart(2, '0') + "/" + anio + " " + horas.toString().padStart(2, '0') + ":" + minutos.toString().padStart(2, '0') + ":" + segundos.toString().padStart(2, '0');
    
        var nombrePaciente= document.getElementById("option"+idPaciente).text;
        var mensaje= document.getElementById("mensaje").value;
        conexion.send(JSON.stringify({ operacion: "mensajeParaPaciente", idReceptor: idPaciente,  mensaje: mensaje, fecha: fechaActual})); 
        mensajes.push({emisor: 'medico', nombrePaciente: nombrePaciente, fecha: fechaActual, mensaje: mensaje});            
        verMensajes();


    }
    
}