var app = rpc("localhost", "gestion_pacientes"); // obtener una referencia a la app RPC
var conexion;
//Obtener referencias a los procedimientos remotos registrados por el servidor
var login = app.procedure("login");
var listadoMedicamentos = app.procedure("listadoMedicamentos");
var datosMedico = app.procedure("datosMedico");
var medicacion = app.procedure("medicacion");
var listadoTomas = app.procedure("listadoTomas");
var agregarToma = app.procedure("agregarToma");
var eliminarToma = app.procedure("eliminarToma");
//var crearTomasDiarias = app.procedure("crearTomasDiarias");
var eliminarMedicacion = app.procedure("eliminarMedicacion");

var mensajes=[]; 
var seccionActual = "login";
var idPaciente; 
var idMedico;

var fechaGlobal = " ";
var frecuenciaTomas;

function cambiarSeccion(seccion){
    document.getElementById(seccionActual).classList.remove("activa");
    document.getElementById(seccion).classList.add("activa");
    seccionActual = seccion;
}
function calcularMinutosHastaFecha(fechaObjetivo) {
    var fechaActual = new Date();
    // Convertimos a formato ISO 8601 "YYYY-MM-DDTHH:mm:ss"
    var partes = fechaObjetivo.split(" ");
    var fechaPartes = partes[0].split("/");
    var horaPartes = partes[1].split(":");
    var fechaISO8601 = fechaPartes[2] + "-" + fechaPartes[1] + "-" + fechaPartes[0] + "T" + horaPartes[0] + ":" + horaPartes[1] + ":" + horaPartes[2];
    var objetivo = new Date(fechaISO8601);
  
    // Restamos la fecha actual a la fecha objetivo
    var diferencia = fechaActual - objetivo;  
    // Convertimos la diferencia de milisegundos a minutos
    var minutos = Math.floor(diferencia / 60000);
  
    return minutos;
  }
  
var nombreTotal;
var nombreMedico;
//FUNCION 1. "LOGIN"
function entrar(){

    var codigo_acceso = document.getElementById("codigo_acceso").value;

    login(codigo_acceso, function(paciente) {

        if(paciente == null){
            alert("Error, la autenticación no es correcta. Vuelva a intentarlo");
            limpiar();
        }
        else{

            cambiarSeccion("listado");

            var bienvenida = document.getElementById("bienvenida");
            bienvenida.innerHTML = "¡Bienvenido/a " + paciente.nombre + " " + paciente.apellidos +"!";
            nombreTotal=paciente.nombre + " " + paciente.apellidos;

            var observaciones_medico = document.getElementById("observaciones_medico");
            if (paciente.observaciones==""){
                observaciones_medico.innerHTML="Todavía no tiene observaciones de su médico"
            }
            else{
                observaciones_medico.innerHTML = "Las observaciones para ti son las siguientes: " + paciente.observaciones + "";
            }
            
            conexion= new WebSocket('ws://localhost:4444', "pacientes");
            // Connection opened
            conexion.addEventListener('open', function (event) {
                console.log("Cliente conectado!!!");
                conexion.send(JSON.stringify({operacion: "primerMensaje", persona: 'paciente', id: idPaciente, nombre:  paciente.nombre+' '+paciente.apellidos}));

            });
            // Para escuchar los mensajes
            conexion.addEventListener('message', function (event) {
                console.log("hay un mensaje")
                mensaje=JSON.parse(event.data);
                mensajes.push({emisor: 'medico', fecha: mensaje.fecha, mensaje: mensaje.mensaje});        
                verMensajes();            

            });
            conexion.addEventListener("close", function () {
                console.log("Desconectado del servidor!!!");
            });
            conexion.addEventListener("error", function () {
                console.log("Error con la conexión!!!");
            });

           
            idPaciente = paciente.id;
            idMedico = paciente.medico;

            verDatosMedico();
            verMedicacion();
        }

    });

}



//FUNCION 3. DATOS MEDICO
function verDatosMedico(){

    datosMedico(idMedico, function(medico) {

        console.log("El id del medico en el main es: ", idMedico);

        var bienvenida_medico = document.getElementById("bienvenida_medico");
        bienvenida_medico.innerHTML = "";

        if(datosMedico == null){
            alert("Vaya, algo ha fallado con este medico");
        }
        else{
            console.log(medico);

            var bienvenida_medico = document.getElementById("bienvenida_medico");

            for(var i=0; i<medico.length;i++){
                bienvenida_medico.innerHTML = "Su médico es " + medico[i].nombre + " "+ medico[i].apellidos;
                nombreMedico=medico[i].nombre + " "+ medico[i].apellidos;
            }
            
        }
    });
}
var idFila=0;
//FUNCION 4. MEDICACION
function verMedicacion(){

    var medicacion_paciente = document.getElementById("medicacion_paciente");
    medicacion_paciente.innerHTML = "";

    var mensaje_medicacion = document.getElementById("mensaje_medicacion");
    mensaje_medicacion.innerHTML = "";

    medicacion(idPaciente, function(listaMedicacion) {
        mensaje_medicacion.innerHTML += "<h2>La lista de la medicacion es: </h2>"
        var tabla= document.createElement("table");
        tabla.innerHTML="<tr><th>Medicamento</th><th>Fecha asignación</th><th>Dosis</th><th>Tomas</td><th>Frecuencia</th><th>Observaciones</th><th></th>"
                        + "<th></th>";
        medicacion_paciente.appendChild(tabla);
        listadoMedicamentos(function(medicamentos){
            for (var i = 0; i < listaMedicacion.length; i++) {
                for (var j=0; j<medicamentos.length; j++){ 
                    if (medicamentos[j].id==listaMedicacion[i].medicamento){
                        var fila= document.createElement("tr");
                        fila.setAttribute("id","fila"+idFila);
                        idFila++;
                        fila.innerHTML= "<td>"+medicamentos[j].nombre+"</td>"
                                        + "<td>"+listaMedicacion[i].fecha_asignacion+"</td>"
                                        + "<td>"+ listaMedicacion[i].dosis +"</td>"
                                        + "<td>"+ listaMedicacion[i].tomas +"</td>"
                                        + "<td>"+ listaMedicacion[i].frecuencia +"</td>"
                                        + "<td>"+ listaMedicacion[i].observaciones +"</td>"
                                        + "<td><button onclick='verTomas(),verListadoTomas("+idPaciente+","+listaMedicacion[i].medicamento+")'>Ver tomas</button></td>"
                                        + "<td><button onclick=\"verOpciones('"+medicamentos[j].nombre+"','"+listaMedicacion[i].medicamento+"')\">Más opciones</button></td>";




                        establecerColor(fila.id,listaMedicacion[i].frecuencia, listaMedicacion[i].medicamento);
                        tabla.appendChild(fila);
                        verMensajes();
                                       
                    }
                }


                frecuenciaTomas = listaMedicacion[i].frecuencia;

            }   
        });     
    });
}
function establecerColor(id_fila, frecuencia, idMed){
    listadoTomas(idPaciente,idMed, function(listaTomas) {
        if (listaTomas.length==0){            
            var fila=document.getElementById(id_fila);
            fila.setAttribute("class","rojo");
        }
        else{
            var ultimaToma=listaTomas[listaTomas.length-1].fecha;
            var minutosPasados=calcularMinutosHastaFecha(ultimaToma);
            if (minutosPasados>=frecuencia*1440){
                var fila=document.getElementById(id_fila);
                fila.setAttribute("class","rojo");
            }
            else{
                var fila=document.getElementById(id_fila);
                fila.setAttribute("class","verde");
            }

        }
        

    })
}

//FUNCION 5. LISTADO TOMAS
function verListadoTomas(idPaciente,idMedicamento){

    listadoTomas(idPaciente,idMedicamento, function(listaTomas) {

        var listado_tomas_html = document.getElementById("listado_tomas_html");
        listado_tomas_html.innerHTML = "";

        var listaTomasTotal = [];
        if (listaTomas.length==0){
            listado_tomas_html.innerHTML = "<p>No tiene tomas</p>";
        }
        
        for(var i = 0; i<listaTomas.length;i++){
            console.log("La lista de las tomas es: ", listaTomas[i]);
            listado_tomas_html.innerHTML += "<ol>" + "Fecha de la toma: " + listaTomas[i].fecha 
                                        + "<button onclick='borrarToma("+listaTomas[i].medicamento+",\""+listaTomas[i].fecha+"\")' class='borrarToma'>Borrar toma</button></ol>";
            listaTomasTotal.push({medicamento:listaTomas[i].medicamento,paciente:listaTomas[i].paciente,fecha:listaTomas[i].fecha});
        }
        listado_tomas_html.innerHTML+="<button onclick='anyadirToma("+idMedicamento+")'class='anyadir'>Añadir toma </button>";

         
    });

}


function anyadirToma(idMedicamento){

    agregarToma(idPaciente,idMedicamento, function(tomas) {
        console.log("Toma nueva añadida", tomas);
        verListadoTomas(idPaciente,idMedicamento);
    });
    

}


function borrarToma(idMedicamento,fechaBorrar){

    console.log("Fecha a borrar: ", fechaBorrar);

    eliminarToma(idPaciente,idMedicamento,fechaBorrar, function(eliminado) {

        if(eliminado){
            console.log("Fecha eliminada");
            //Llamamos a la funcion para que refresque!
            verListadoTomas(idPaciente,idMedicamento);  
        } 
        else{
            console.log("Fecha no eliminada");
        }

    });

}

function salir(){
    mensajes=[];
    conexion.close();
    limpiar();
    cambiarSeccion("login");
}

function ver_pagina_principal(){
    verDatosMedico();
    verMedicacion();
    cambiarSeccion("listado");
}

function verTomas(){
    cambiarSeccion("listado_tomas");
}


function verOpciones(nombreMedicamento) {
    document.getElementById("necesito_mas").innerHTML = "Pedir más " + nombreMedicamento + " a mi médico";
    document.getElementById("necesito_mas").setAttribute("onclick", "pedirMedicacion('" + nombreMedicamento + "')");
    document.getElementById("reaccion_adversa").innerHTML = "Comentar reacción adversa del " + nombreMedicamento + " a mi médico";
    document.getElementById("reaccion_adversa").setAttribute("onclick", "comentarReaccionAdversa('" + nombreMedicamento + "')");
    cambiarSeccion("mas_opciones");
}


function limpiar(){
    var limpiar = {
        nombre: document.getElementById("codigo_acceso").value = "",
    };
}
function pedirMedicacion(nombreMedicacion){
    console.log("medicacion pedida")
    var fechaAhora = new Date();
    var dia = fechaAhora.getDate();
    var mes = fechaAhora.getMonth() + 1; // Sumamos 1 ya que los meses en JavaScript se indexan desde 0 (enero es 0)
    var anio = fechaAhora.getFullYear();
    var horas = fechaAhora.getHours();
    var minutos = fechaAhora.getMinutes();
    var segundos = fechaAhora.getSeconds();
    var fechaActual = dia.toString().padStart(2, '0') + "/" + mes.toString().padStart(2, '0') + "/" + anio + " " + horas.toString().padStart(2, '0') + ":" + minutos.toString().padStart(2, '0') + ":" + segundos.toString().padStart(2, '0');

    conexion.send(JSON.stringify({ operacion: "masMedicacion", idReceptor: idMedico, nombreMedicacion: nombreMedicacion, fecha: fechaActual}));
    mensajes.push({fecha: fechaActual, mensaje: 'No tengo más '+nombreMedicacion+' y necesito una nueva receta'});
    verMensajes();
}   

function comentarReaccionAdversa(nombreMedicacion)
{
    console.log("reaccion comentada")
    var fechaAhora = new Date();
    var dia = fechaAhora.getDate();
    var mes = fechaAhora.getMonth() + 1; // Sumamos 1 ya que los meses en JavaScript se indexan desde 0 (enero es 0)
    var anio = fechaAhora.getFullYear();
    var horas = fechaAhora.getHours();
    var minutos = fechaAhora.getMinutes();
    var segundos = fechaAhora.getSeconds();
    var fechaActual = dia.toString().padStart(2, '0') + "/" + mes.toString().padStart(2, '0') + "/" + anio + " " + horas.toString().padStart(2, '0') + ":" + minutos.toString().padStart(2, '0') + ":" + segundos.toString().padStart(2, '0');

    conexion.send(JSON.stringify({ operacion: "reaccionAdversa", idReceptor: idMedico, nombreMedicacion: nombreMedicacion, fecha: fechaActual}));
    mensajes.push({fecha: fechaActual, mensaje: 'Me sienta mal el '+nombreMedicacion});
 
    verMensajes();

}

function pedirCita(){
    var fecha=document.getElementById("fechaCita").value;
    var hora = document.getElementById("horaCita").value;
    if (fecha!="" && hora!=""){
        console.log("cita pedida")
        var fechaAhora = new Date();
        var dia = fechaAhora.getDate();
        var mes = fechaAhora.getMonth() + 1; // Sumamos 1 ya que los meses en JavaScript se indexan desde 0 (enero es 0)
        var anio = fechaAhora.getFullYear();
        var horas = fechaAhora.getHours();
        var minutos = fechaAhora.getMinutes();
        var segundos = fechaAhora.getSeconds();
        var fechaActual = dia.toString().padStart(2, '0') + "/" + mes.toString().padStart(2, '0') + "/" + anio + " " + horas.toString().padStart(2, '0') + ":" + minutos.toString().padStart(2, '0') + ":" + segundos.toString().padStart(2, '0');

        conexion.send(JSON.stringify({ operacion: "pedirCita", idReceptor: idMedico, fecha: fechaActual, fechaCita: fecha, horaCita: hora}));
        mensajes.push({fecha: fechaActual, mensaje: 'Quiero una cita para el dia '+fecha+" a las "+hora});                        
        verMensajes();

    }
    else{
        alert("Debe rellenar la fecha y la hora")
    }

    

}
function verMensajes(){
    var tabla=document.getElementById("listaMensajes");
    tabla.innerHTML="<tr><th>Emisor</th><th>Receptor</th><th>Fecha mensaje</th><th>Mensaje</th></tr>";
    if (mensajes.length==0){
        tabla.innerHTML+="<tr><td colspan='4'>No tiene ningún mensaje</td></tr>"
    }
    else{
        for (var x=0; x<mensajes.length; x++){
            var fila= document.createElement("tr");
            if (mensajes[x].emisor=="medico"){
                fila.innerHTML="<td>"+nombreMedico+"</td><td>Yo</td>";

            }
            else{
                fila.innerHTML="<td>Yo</td><td>"+nombreMedico+"</td>";
            }
            fila.innerHTML+="<td>"+mensajes[x].fecha+"</td>"
                            +"<td>"+mensajes[x].mensaje+"</td>";
                            
            tabla.appendChild(fila);

        }
    }
}
