const modulos = [
    {
        nombre: 'Sistemas informáticos',
        curso: 1,
        alumnos: [
        'Don Pepito', 'Perico', 'Don José'
        ]
    },
    {
        nombre: 'Desarrollo Web en entorno cliente',
        curso: 2,
        asignatura: '',
        alumnos: [
        'Juan', 'Perico', 'Andrés', 'Don Pepito'
        ]
    },
    {
        nombre: 'Entornos',
        curso: 1,
        asignatura: '',
        alumnos: [
        'Juan'
        ]
    },
]

let setCurso1 = new Set();
let setCurso2 = new Set();
let enDosCursos = new Array();

// Obtenemos arrays con los nombres diferentes de cada alumno de cada curso
for(modulo of modulos){
    let curso = modulo.curso;
    for(alumno of modulo.alumnos){
        if(curso === 1) setCurso1.add(alumno);
        if(curso === 2) setCurso2.add(alumno);
    }
}

// Recorremos un set de cursos para ver si se repite el nombre en la otra
for(let val of setCurso1){
    if(setCurso2.has(val)) enDosCursos.push(val);
}

console.log("Alumnos en dos cursos: ");
for(let nom of enDosCursos){
    console.log("- "+nom);
}