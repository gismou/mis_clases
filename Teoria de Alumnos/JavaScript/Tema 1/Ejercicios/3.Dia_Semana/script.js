/* El 1 de Enero de 2021 fue viernes */
const dia = 15;
const mes = 1;
const diaSemana = ["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"];
const diasMeses = [31,28,31,30,31,30,31,31,30,31,30,31];
let diasTranscurridos = 0;
let diaSemanaElegido;

// Recorre meses para obtener el total de dias transcurridos desde el 1 de Enero
for(let i = 1; i<=mes; i++){
    if(i == mes){
        diasTranscurridos += dia;
    }else{
        diasTranscurridos += diasMeses[i-1];
    }
}

// Recorremos un bucle que se itere tantas veces como dias transcurridos, y obtenemos su posición en el array según el contador
// declarado, no el del for, al llegar este contador a 7, lo iniciamos a 0 de nuevo. Obtenemos así en cada iteración que día de la
// array de dias de semana obtenemos, para al final tener el que queremos.
cont = 4; // es cuatro por que es la posición del array de días de semana que tiene la posición del viernes, que es domingo.
for(i=0; i<diasTranscurridos; i++){
    if(cont == 7) cont = 0;
    diaSemanaElegido = diaSemana[cont];
    cont++;
}

console.log("Han transcurrido "+diasTranscurridos+" dias y el día de la semana es "+diaSemanaElegido);
