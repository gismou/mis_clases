const matriz = [];
for(i=0; i<20; i++){
    matriz.push(Math.floor(Math.random()*10));
}

// los números obtenidos los introduzco en un Set
const set = new Set(matriz);
let arrSinRepes = Array.from(set);
let moda = 0;
// recorro la matriz y luego el set, para ir contando resultados y ver cual es el mayor
let mayor = 0;
for(let i = 0; i<arrSinRepes.length; i++){
    let cont = 0;
    for(let j = 0; j < matriz.length; j++){
        if(matriz[j] === arrSinRepes[i]) cont++;
    }
    if(cont > mayor){
        mayor = cont;
        moda = arrSinRepes[i];
    }
}

console.log(matriz);
console.log("La moda es "+moda);