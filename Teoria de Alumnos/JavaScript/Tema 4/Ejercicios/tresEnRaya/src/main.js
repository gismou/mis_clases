import './style.css'
import { TresnEnRaya } from './TresEnRaya.mjs'

let ter = new TresnEnRaya();

const divTablero = document.getElementById('div_tablero');
const btnJugar = document.getElementById("btn_jugar");
const spanTurno = document.getElementById("span_turno");
const fila = document.getElementById("input_fila");
const columna = document.getElementById("input_columna");

// Iniciar tablero vacio
divTablero.innerHTML = ter.pintarTablero();
ter.jugada(2,3);
btnJugar.addEventListener("click",()=>{
  // Realiza jugada
  ter._fila = parseInt(fila.value);
  ter._columna = parseInt(columna.value);
  ter.jugada(); 
  // Pintar tablero
  divTablero.innerHTML = ter.pintarTablero();
  // Comprobar gana
  //if(ter.comprobarGana()) alert("Gana "+spanTurno.innerText);
  // Cambiar el turno
  spanTurno.innerHTML = ter._turno;
})