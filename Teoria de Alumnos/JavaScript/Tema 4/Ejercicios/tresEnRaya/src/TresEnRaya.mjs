class TresnEnRaya{

    static tablero;
    static turno;
    fila;
    columna;

    constructor(){
        this.tablero = [];
        // Definir el tamaño de la matriz
        for(let i=0; i<3; i++){
            this.tablero[i] = new Array(3);
        }
        // Llenarla de espacios en blanco
        for(let i=0; i<3;i++){
            for(let j=0; j<3;j++){
                this.tablero[i][j] = "-";
            }
        }
        this.turno = 'X';
        this.fila = 0;
        this.columna = 0;
    }

    get _fila(){
        return this.fila;
    }

    get _columna(){
        return this.columna;
    }

    set _fila(fila){
        this.fila = fila;
    }

    set _columna(columna){
        this.columna = columna;
    }

    get _turno(){
        return this.turno;
    }

    set _turno(turno){
        this.turno = turno;
    }

    cambiarTurno(){
        if(this.turno == 'X') this.turno = 'O';
        else this.turno = 'X';
    }

    pintarTablero(){
        let pintado = "";
        for(let i=0; i<3;i++){
            for(let j=0; j<3;j++){
                pintado += "[ "+this.tablero[i][j]+" ]";
            }
            pintado += "<br>";
        }
        console.log(pintado);
        return pintado;
    }

    /* jugadaVacia(){
        if(this.fila == undefined || this.columna == undefined){
            return true;
        }
        return false;
    } */

    jugadaValida(){
        if(this.fila < 1 || this.fila > 4){
            console.log("La fila sale de los límites (1 - 4");
            return false;
        }
        if(this.columna < 1 || this.columna > 4){
            console.log("La columna sale de los límites (1 - 4)");
            return false;
        }
        return true;
    }

    espacioLibre(){
        if(this.tablero[this.fila-1][this.columna-1] != '-'){
            alert("El espacio está ocupado");
            return false;
        }
        return true;
    }

 /*    comprobarGana(){
        let cont = 0;
        for(let i=0; i<3; i++){
            for(let j=0; j<3; j++){
                // Arriba
                if(this.casillaIgualTurno(i,j+1)) cont++;
                // Abajo
                if(this.casillaIgualTurno(i,j-1)) cont++;

                if(cont == 3) return true;
            }
        }
        return false;
    }
 */
 /*    casillaIgualTurno(i,j){
        // Invierte signo
        let signo = 'X';
        if(this.turno == 'X') signo = '0';
        if (this.tablero[i][j] == signo && this.tablero[i][j] != undefined) return true;
        return false;
    } */
    
    jugada(){
       /* this.jugadaVacia(); */
       if(this.jugadaValida() && this.espacioLibre()){
            console.log("fila: "+this.fila);
            console.log("columna: "+this.columna);
            this.tablero[this.fila-1][this.columna-1] = this.turno;
            this.cambiarTurno();
       }
    }

}

export{
    TresnEnRaya
}