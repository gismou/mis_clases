import { defineConfig } from "vite";
import eslint from "@rollup/plugin-eslint";

export default defineConfig({
    root: './src',
    server: {
        port: 8080
    },
    build: {
        outDir: '../dist'
    },
    plugins: [
        eslint({
            exclude: [/.css/,/.svg/]
        })
    ]
});