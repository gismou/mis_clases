import './style.css'
import { Gatos,estadoGato,pasoDelTiempo } from './Gatos.mjs'

let gato = new Gatos();

console.log("¡El gato acaba de nacer!");
estadoGato(gato);

pasoDelTiempo(gato,30);
estadoGato(gato);

console.log("El gato come");
gato.comer();
estadoGato(gato);

console.log("El gato duerme");
gato.dormir();
estadoGato(gato);

console.log("El gato juega");
gato.jugar();
estadoGato(gato);

pasoDelTiempo(gato,100);
estadoGato(gato);