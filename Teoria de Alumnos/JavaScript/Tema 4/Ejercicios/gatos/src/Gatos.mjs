class Gatos {

    #hambre;
    #cansancio;
    #felicidad;
    #tiempoDeVida;

    constructor(){
        this.#hambre = 0;
        this.#cansancio = 0;
        this.#felicidad = 0;
        this.#tiempoDeVida = 0;
    }

    get hambre(){
        return this.#hambre;
    }

    get cansancio(){
        return this.#cansancio;
    }

    get felicidad(){
        return this.#felicidad;
    }

    get tiempoDeVida(){
        return this.#tiempoDeVida;
    }

    set hambre(hambre){
        this.#hambre = hambre;
    }

    set cansancio(cansancio){
        this.#cansancio = cansancio;
    }

    set felicidad(felicidad){
        this.#felicidad = felicidad;
    }

    set tiempoDeVida(tiempoDeVida){
        this.#tiempoDeVida = tiempoDeVida;
    }

    comer(){
        this.#hambre -= Math.random() * (1 - 0) + 0;
    }

    dormir(){
        this.#cansancio -= Math.random() * (1 - 0) + 0;
    }

    jugar(){
        this.#felicidad += Math.random() * (1 - 0) + 0;
    }

}

function estadoGato(gato){
    console.log("--------- ESTADO DEL GATO ---------");
    console.log("- Felicidad: "+gato.felicidad.toFixed(2));
    console.log("- Hambre: "+gato.hambre.toFixed(2));
    console.log("- Cansancio: "+gato.cansancio.toFixed(2));
    console.log("- Milisegundos de vida: "+gato.tiempoDeVida);
    console.log("-----------------------------------");
}

function pasoDelTiempo(gato,milisegundos){
    gato.tiempoDeVida += milisegundos;
    console.log("Han pasado "+milisegundos+" milisegundos de vida para el gato");
    for(let i=0; i<milisegundos; i++){
        if(i % 10 == 0){
            gato.hambre += Math.random() * (1 - 0) + 0;
            gato.cansancio += Math.random() * (1 - 0) + 0;
            gato.felicidad -= Math.random() * (1 - 0) + 0;
        }
    }
}

export{
    Gatos,
    estadoGato,
    pasoDelTiempo
}