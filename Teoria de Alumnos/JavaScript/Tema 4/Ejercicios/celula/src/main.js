import './style.css'

// Objeto Celula -------------------------------------------
let celula =  {
    valor : 1,
    nombre : "celulosa",
    esMaligna : false,
    mitosis : function() {
        let nuevaCelula = {};
        /* Recorremos todas las claves del objeto y borramos
        todo menos la función mitosis, además de ir llenando la
        nueva copia de celula que vamos a devolver. */
        for(let key in this){
            nuevaCelula[key] = this[key];
            if(key != 'mitosis') delete this[key];
        }
        return nuevaCelula;
    }
}

// Test ----------------------------------------------------
console.log("Soy la celula original: ");
console.log(celula);

console.log("Soy la celula original tras hacer la mitosis: ");
let hija1 = celula.mitosis();
console.log(celula);

console.log("Soy la hija de la celula original: ");
console.log(hija1);

console.log("a la celula hija, le añadimos una propiedad cansancio de valor 10");
hija1['cansancio'] = 10;

console.log("la celula hija hace la mitosis y queda así: ");
let hija2 = hija1.mitosis();
console.log(hija1);

console.log("La hija de la primera hija ha quedado así: ");
console.log(hija2);