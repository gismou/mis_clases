let puntuacionTenis = [0,15,30,40,'ventaja'];
let posPuntosJ1 = 0;
let posPuntosJ2 = 0;

function iniciarJuego(){
    posPuntosJ1 = 0;
    posPuntosJ2 = 0;
}

function esDeuce(){
    if(posPuntosJ1 >= 3 && posPuntosJ2 >= 3){
        return true;
    }

    return false;
}

function finDeuce(){
    if(posPuntosJ1 == 5 || posPuntosJ2 == 5) return true;
    else return false;
}

function calcularPartidoFinalizado(){
    console.log("posPuntosJ1: "+posPuntosJ1);
    console.log("posPuntosJ2: "+posPuntosJ2);
    console.log("esDeuce: "+esDeuce());
    if(!esDeuce()){
        console.log("Entro en NO es deuce")
        if(posPuntosJ1 == 4 || posPuntosJ2 == 4) return true;
        else return false;
    }else{
        console.log("Entro en S es deuce")
        return finDeuce();  
    }
}

function puntoJugador(jugador){
    if(calcularPartidoFinalizado()){
        alert("El partido ya ha finalizado");
        return 0;
    }else{
        if(jugador == "puntuaJ1"){
            posPuntosJ1++;
            esDeuce();
            calcularPartidoFinalizado();
            return puntuacionTenis[posPuntosJ1];
        }else{
            posPuntosJ2++; 
            esDeuce();
            calcularPartidoFinalizado();
            return puntuacionTenis[posPuntosJ2];
        } 
    }
}

function resultado(){
    let res = puntuacionTenis[posPuntosJ1]+" - "+puntuacionTenis[posPuntosJ2];
    if(posPuntosJ1 == 3 && posPuntosJ2 == 3) res = "Deuce";
    if(calcularPartidoFinalizado()){
        if(posPuntosJ1 > posPuntosJ2) res = "Ganador Jugador 1";
        else res = "Ganador Jugador 2";
    }
    return res;
}

export{
    iniciarJuego,
    puntoJugador,
    resultado,
}