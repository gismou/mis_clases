import { iniciarJuego, puntoJugador, resultado } from "./tenis.mjs";

const puntosJ1 = document.getElementById('puntosJ1');
const puntosJ2 = document.getElementById('puntosJ2');
const botonJ1 = document.getElementById('puntuaJ1');
const botonJ2 = document.getElementById('puntuaJ2');
const pResultado = document.getElementById('resultado');

iniciarJuego();

botonJ1.addEventListener("click",()=>{
  let puntos = puntoJugador(botonJ1.id);
  puntosJ1.innerHTML = puntos;
  pResultado.innerHTML = resultado();
});
botonJ2.addEventListener("click",()=>{
  let puntos = puntoJugador(botonJ2.id);
  puntosJ2.innerHTML = puntos;
  pResultado.innerHTML = resultado();
});