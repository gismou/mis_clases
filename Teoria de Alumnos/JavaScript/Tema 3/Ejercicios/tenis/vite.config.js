import { defineConfig } from "vite";
import eslint from "@rollup/plugin-eslint";

export default defineConfig({
    root: './src',
    plugins: [
        eslint({
            exclude: [/.css/,/.svg/]
        })
    ]
});