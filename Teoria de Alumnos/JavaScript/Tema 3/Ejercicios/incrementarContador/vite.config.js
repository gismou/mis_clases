import { defineConfig } from "vite";
import { qrcode } from "vite-plugin-qrcode";
import eslint from "@rollup/plugin-eslint";

export default defineConfig({
    root: './src',
    server: {
        port: 8080
    },
    build: {
        outDir: '../dist'
    },
    plugins: [
        qrcode(),
        eslint({
            exclude: [/.css/,/.svg/]
        })
    ]
});