import {incrementar,reset} from './funciones.mjs';

const P = document.getElementById('cont');
const BI = document.getElementById('incrementar');
const BR = document.getElementById('reset');

BI.addEventListener('click',()=>{
    P.innerHTML = incrementar();
});

BR.addEventListener('click',()=>{
    P.innerHTML = reset();
})