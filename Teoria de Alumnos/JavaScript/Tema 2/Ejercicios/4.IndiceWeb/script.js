allH1 = document.getElementsByTagName("h1");
allH2 = document.getElementsByTagName("h2");

let tagPre = "<pre>";
let cont1 = 0;
let cont2 = 0;
for(let node of document.body.getElementsByTagName("*")){
    if(node.tagName == "H1"){
        cont1++;
        cont2=0;
        tagPre += cont1+".- "+node.textContent+"\n";
    }
    if(node.tagName == "H2"){
        cont2++;
        tagPre += "\t"+cont1+"."+cont2+".- "+node.textContent+"\n";
    }
}
tagPre += "</pre>";

document.getElementById("mostrar").innerHTML = tagPre;