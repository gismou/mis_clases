const padre = document.getElementById("divPadre");
const hijos = document.getElementsByClassName("divHijo");

for(let hijo of hijos){
    hijo.addEventListener("click",()=>{
        let color = hijo.style.backgroundColor;
        padre.setAttribute("style","background-color: "+color);
    })
}