const allP = document.getElementsByTagName("p");
for(let p of allP){
    p.addEventListener("click",esconder);
    p.addEventListener('contextmenu',eliminar);
}

function esconder(){
    this.hidden = true;
}

function eliminar(evento){
    evento.preventDefault();
    alert("Borrado");
    document.body.removeChild(this);
}

function reaparecer(){
    for(let p of allP){
        p.hidden = false;
    }
}