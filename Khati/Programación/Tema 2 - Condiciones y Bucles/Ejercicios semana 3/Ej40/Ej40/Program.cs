﻿using System;
public class Ej39
{
    static void Main()
    {
        /*
         * 40. Crea una versión mejorada del ejercicio anterior: 
            Escribe un programa que le pida al usuario dos números a y b, 
            así como un límite inferior y un límite superior, 
            y muestre todos los números entre ambos límites (incluidos) 
            que sean a la vez múltiplos de a y de b, 
            o la palabra "Ninguno" si corresponde
        */

        int num1, num2, inferior, superior;
        int multiplos = 0;

        Console.Write("Introduce un número: ");
        num1 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Introduce otro número: ");
        num2 = Convert.ToInt32(Console.ReadLine());

        Console.Write("¿A partir de qué número buscamos? ");
        inferior = Convert.ToInt32(Console.ReadLine());

        Console.Write("¿Hasta que número buscamos? ");
        superior = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine();

        for (int i = inferior; i <= superior; i++)
        {
            if (i % num1 == 0 && i % num2 == 0)
            {
                multiplos++;
                if (multiplos == 1)
                {
                    Console.Write("He encontrado los siguientes múltiplos comunes de ambos números: ");
                }

                Console.Write("{0} ", i);
            }
        }

        if(multiplos == 0)
        {
            Console.Write("He encontrado los siguientes múltiplos comunes de ambos números: Ninguno");
        }

    }
}