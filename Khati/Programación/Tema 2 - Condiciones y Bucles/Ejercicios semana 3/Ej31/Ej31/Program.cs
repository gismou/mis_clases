﻿using System;
public class Ej31
{
    static void Main()
    {
        /*31.Muestra los números del 10 al 20, ambos inclusive, descendiendo,
        separados por un espacio, sin avanzar de línea, usando "for".*/

        for(int i = 20; i >= 10; i--)
        {
            Console.Write("{0} ", i);
        }
    }
}   