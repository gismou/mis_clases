﻿using System;
public class Ej37
{
    static void Main()
    {
        /*
         * 37. Haz un programa que pida al usuario un alto y un ancho y que 
            muestre un rectángulo formado por asteriscos, 
            con ese alto y ancho, como en este ejemplo:

            Alto? 5
            Ancho? 3

            ***
            ***
            ***
            ***
            ***

        */

        int alto, ancho;

        Console.Write("Alto? ");
        alto = Convert.ToInt32(Console.ReadLine());

        Console.Write("Ancho? ");
        ancho = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine();

        for (int i = 0; i < alto; i++)
        {
            for(int j = 0; j < ancho; j++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }

    }
}