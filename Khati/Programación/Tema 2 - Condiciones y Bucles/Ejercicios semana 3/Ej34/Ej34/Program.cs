﻿using System;
public class ej34{ 
    static void Main()
    {
        /*
         * 34. Muestra la tabla de multiplicar del número que escoja el usuario,
               usando "for".
        */

        int num;

        Console.Write("Elige un número: ");
        num = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("\nTabla del {0}",num);
        Console.WriteLine("-----------------------------------");
        for(int i = 1; i <= 10; i++)
        {
            Console.WriteLine("{0} x {1} = {2}",num,i,num*i);
        }
    }
}