﻿using System;
public class Ej38
{
    static void Main()
    {
        /*
         * 38. Muestra un triángulo decreciente alineado a la izquierda, con 
            letras X, con el tamaño indicado por el usuario, usando "for":

            Tamaño? 5

            XXXXX
            XXXX
            XXX
            XX
            X

        */

        int size;

        Console.Write("Tamaño? ");
        size = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine();

        do
        {
            for (int i = 0; i < size; i++)
            {
                Console.Write("X");
            }
            Console.WriteLine();
            size--;
        } while (size > 0);

    }
}