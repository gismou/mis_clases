﻿using System;

public class Ej33
{
    static void Main()
    {
        /*
         *  33. Pide dos números enteros al usuario y responde cuántos numeros
            hay entre ellos (ambos inclusive) que sean a la vez pares y
            múltiplos de 3, así:

            Número inicial? 11
            Número final? 21
            Números pares y a la vez múltiplos de 3 encontrados: 2

            Nota: el programa debe comportarse correctamente si el primer número
            es menor que el segundo. No funcionará si el segundo es menor que 
            el primero, pero no es importante todavía.
        */

        int num1, num2;
        int res = 0;

        Console.Write("Introduce el primer número: ");
        num1 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Introduce el segundo número: ");
        num2 = Convert.ToInt32(Console.ReadLine());

        for(int i = num1; i <= num2; i++)
        {
            if(i % 2 == 0 && i % 3 == 0)
            {
                res++;
            }
        }

        Console.WriteLine("Número inicial? {0}", num1);
        Console.WriteLine("Número final? {0}", num2);
        Console.WriteLine("Números pares y a la vez múltiplos de 3 encontrados: {0}", res);

    }
}