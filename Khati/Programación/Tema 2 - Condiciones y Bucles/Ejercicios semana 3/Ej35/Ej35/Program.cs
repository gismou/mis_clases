﻿using System;
public class ej35
{
    static void Main()
    {
        /*
         * 35. Pregunta al usuario dos números n1 y n2 (por ejemplo, 0 y 10). 
            Muestra las tablas de multiplicar de todos lo números comprendidos
            entre n1 y n2, utilizando "for". 
            Debe haber una línea en blanco separando cada tabla de multiplicar
            de la siguiente. 
            El programa debe comportarse correctamente si el primer número es 
            menor que el segundo 
            (es decir: si introduce 10 y 0, aun así se mostrarán desde 
            la tabla del 0 hasta la del 10).
        */

        int num1,num2,mayor,menor;

        Console.Write("Elige un número: ");
        num1 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Elige otro número: ");
        num2 = Convert.ToInt32(Console.ReadLine());

        // Comprobar cual número es el mayor y cual el menor
        if(num1 > num2)
        {
            mayor = num1;
            menor = num2;
        }
        else
        {
            mayor = num2;
            menor = num1;
        }

        // Recorrer y mostar las tablas de multiplicar
        for (int i = menor; i <= mayor; i++)
        {
            Console.WriteLine("\nTabla del {0}", i);
            Console.WriteLine("-----------------------------------");
            for (int j = 1; j <= 10; j++)
            {
                Console.WriteLine("{0} x {1} = {2}", i, j, i * j);
            }
        }
    }
}