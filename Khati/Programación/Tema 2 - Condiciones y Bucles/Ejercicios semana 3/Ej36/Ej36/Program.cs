﻿using System;
public class Ej36
{
    static void Main()
    {
        /*
         * 36. Crea un programa que pida al usuario dos números enteros y muestre
               su producto, empleando sumas repetitivas. 
               Recuerda que 3 * 4 = 3 + 3 + 3 + 3 (4 sumandos) = 12.
        */

        int num1, num2;
        int contador = 0;

        Console.Write("Introduce un número: ");
        num1 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Introduce otro número: ");
        num2 = Convert.ToInt32(Console.ReadLine());

        Console.Write("{0} * {1} = ", num1, num2);

        for (int i = 1; i <= num2; i++)
        {
            contador++;

            if(i != num2)
            {
                Console.Write("{0} + ",num1);
            }
            else
            {
                Console.Write("{0} ({1} sumandos) = {2}.", num1, contador, num1 * num2);
            }
        };
    }
}