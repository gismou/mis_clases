﻿using System;
public class Ej28
{
    static void Main()
    {
        /*
           Escribe un programa en C# que pida al usuario un número entero y muestre su tabla de multiplicar, usando "while".
         */

        int num;
        int mult = 1;
        Console.Write("Introduce una tabla de multiplicar: ");
        num = Convert.ToInt32(Console.ReadLine());
        while (mult <= 10)
        {
            Console.WriteLine("{0} x {1} = {2}", num, mult, num * mult);
            mult++;
        }
    }
}