﻿using System;
public class Ej30
{
    static void Main()
    {
        /*
            Crea una versión mejorada del ejercicio anterior, en la que el usuario sólo tenga 3 intentos. 
            Si al cabo de 3 intentos no ha indicado el login y la contraseña correctos, se le responderá con "Acceso denegado"
            y terminará el programa. Si introduce ambos datos de forma correcta en 3 intentos o menos, se le dirá "Acceso concedido" 
            y terminará el programa. Esta versión hazla sólo una vez, empleando "while" o "do-while", como consideres más razonable.
         */

        int pass, user;
        int err = 0;

        do
        {
            Console.Write("Introduce tu usuario: ");
            user = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introduce tu contraseña: ");
            pass = Convert.ToInt32(Console.ReadLine());

            if (pass != 1234 && user != 1000)
            {
                err++;
                if (err == 3)
                {
                    Console.WriteLine("Acceso denegado");
                }
                else
                {
                    Console.WriteLine("Error, usuario o contraseña incorrecta. Errores {0}",err);
                }
            }
        } while (pass != 1234 && user != 1000 && err != 3);

        if(err != 3)
        {
            Console.WriteLine("Acceso concedido");
        }
    }
}