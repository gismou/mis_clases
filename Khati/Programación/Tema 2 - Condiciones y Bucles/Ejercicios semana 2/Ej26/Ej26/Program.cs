﻿using System;
public class Ej26
{
    static void Main()
    {
        /*
         Crea una variante del ejercicio anterior (contraseña numérica), 
        empleando en esta ocasión "do-while" (no "while").
         */

        int pass;
        do
        {
            Console.Write("Introduce un número: ");
            pass = Convert.ToInt32(Console.ReadLine());
            if(pass != 1111){
                Console.WriteLine("Error, contraseña incorrecta");
            }
        } while (pass != 1111);
    }
}