﻿using System;
public class Ej23
{
    static void Main()
    {
        /*
         Crea una variante del ejercicio anterior, en la que la variable se llamará "impar y positivo", y recibirá el valor 1 si el número
         introducido por el usuario es positivo además de impar. En caso contrario (si no es positivo o no es impar), tendrá el valor 0.
         */

        int num;
        int imparPositivo;

        Console.WriteLine("Escriba un número: ");
        num = Convert.ToInt32(Console.ReadLine());

        if(num > 0 && num % 2 == 0)
        {
            imparPositivo = 1;
        }
        else
        {
            imparPositivo = 0;
        }
        Console.WriteLine("CON IF");
        Console.WriteLine("El número {0} da como valor a la variable imparPositivo {1}\n", num, imparPositivo);

        imparPositivo = (num % 2 == 0 && num > 0) ? 1 : 0;
        Console.WriteLine("CON TERNARIO");
        Console.WriteLine("El número {0} da como valor a la variable imparPositivo {1}\n", num, imparPositivo);
    }
}