﻿using System;
public class Ej29
{
    static void Main()
    {
        /*
           Crea una versión mejorada del ejercicio de la contraseña numérica, en la que se pida al usuario tanto su login
           (que será el número 1000) como su contraseña (que será 1234). 
           No se le permitirá seguir hasta que introduce ambos datos correctos. 
           Esta versión hazla sólo una vez, empleando "while" o "do-while", como consideres más razonable.
         */

        int pass, user;
        do
        {
            Console.Write("Introduce tu usuario: ");
            user = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introduce tu contraseña: ");
            pass = Convert.ToInt32(Console.ReadLine());

            if(pass != 1234 && user != 1000)
            {
                Console.WriteLine("Error, usuario o contraseña incorrecta");
            }

        } while (pass != 1234 && user != 1000);
    }
}