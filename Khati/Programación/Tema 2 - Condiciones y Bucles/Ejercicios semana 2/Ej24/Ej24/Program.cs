﻿using System;
public class Ej23
{
    static void Main()
    {
        /*
         Haz un programa que diga al usuario "Introduce un número negativo", y se lo pida tantas veces como sea necesario,
         repitiendo en caso de que introduzca un número no válido. Hazlo dos veces como parte del mismo programa: primero usando "while"
         y luego empleando "do-while".
         */

        int num;
        
        Console.WriteLine("CON WHILE");
        Console.Write("Introduce un número negativo: ");
        num = Convert.ToInt32(Console.ReadLine());

        while (num > 0)
        {
            if(num > 0)
            {
                Console.WriteLine("El número no es negativo");
            }
            Console.Write("Introduce un número negativo: ");
            num = Convert.ToInt32(Console.ReadLine());
        }

        Console.WriteLine("\nCON DO WHILE");
        do
        {
            Console.Write("Introduce un número negativo: ");
            num = Convert.ToInt32(Console.ReadLine());
            if (num > 0)
            {
                Console.WriteLine("El número no es negativo");
            }
        } while (num > 0);
    }
}