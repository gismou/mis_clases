﻿using System;
public class Ej27
{
    static void Main()
    {
        /*
            Crea un programa que muestre los números del 10 al 20, separados por un espacio, sin avanzar de línea, usando "while".
         */

        int num = 1;
        while(num <= 20)
        {
            Console.Write("{0} ",num);
            num++;
        }
    }
}