﻿using System;
public class Ej25
{
    static void Main()
    {
        /*
         Escribe un programa en C# que pida al usuario una contraseña numérica. 
        Se le dirá "Acceso concedido" cuando acierte la contraseña correcta, 1111. 
        Si no la acierta, se le volverá a pedir tantas veces como sea necesario. 
        Hazlo con "while" (no con "do-while").
         */

        int pass;

        Console.Write("Introduce un número: ");
        pass = Convert.ToInt32(Console.ReadLine());
        while(pass != 1111)
        {
            if(pass != 1111)
            {
                Console.WriteLine("Error, contraseña incorrecta");
            }
            Console.Write("Introduce un número: ");
            pass = Convert.ToInt32(Console.ReadLine());
        }
    }
}