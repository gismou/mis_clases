﻿using System;

public class Ej32
{
    static void Main()
    {
        /*
         * Escribe un programa que calcule (y muestre) el valor absoluto de un número x: 
         * si el número es positivo (o cero), su valor absoluto es exactamente el número x; 
         * en caso contrario, su valor absoluto es -x. 
         * Hazlo de dos maneras diferentes en el mismo programa: usando "if" y usando el
         * "operador condicional" u "operador ternario" (?). Tu programa pedirá un dato "x" 
         * una única vez y mostrará la respuesta dos veces (una vez con "if" y otra vez con el "operador ternario").
         */

        int numero;
        int valorAbsoluto;

        Console.WriteLine("Introduce un número");
        numero = Convert.ToInt32(Console.ReadLine());
        if (numero >= 0 )
        {
            valorAbsoluto = numero * 1;
        }
        else
        {
            valorAbsoluto = numero * -1;
        }
        Console.WriteLine("CON IF");
        Console.WriteLine("El valor absoluto del número {0} introducido es: {1}\n", numero, valorAbsoluto);

        valorAbsoluto = numero >= 0 ? numero : numero * -1;
        Console.WriteLine("CON OPERADOR TERNARIO");
        Console.WriteLine("El valor absoluto del número {0} introducido es: {1}", numero, valorAbsoluto);

    }
}

