﻿// Khaty Lopez  
using System;
class Divisores
{
    static void Main()
    {
        // Declaramos las varibales iniciales dandoles un valor inicial
        int numero = 0;
        int vueltas = 0;

        // Bucle de la repetición del programa, se ejecuta al menos una vez
        do
        {
            // Preguntar por un número al usuario (si pone 0 o negativo acabará)
            try
            {
                Console.Write("Introduce un número: ");
                numero = Convert.ToInt32(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.Write("Se ha introducido un valor incorrecto: {0}",e.ToString());
            }

            // Seguimos si el número no es negativo o 0
            if(numero > 0)
            {
                // Obtener los divisores del número introducido y contar cuantos tiene mientras mostramos la cadena de texto
                int divisores = 0;             
                Console.Write("Los divisores del {0} son: ", numero);
                for (int i = 1; i <= numero; i++)
                {
                    if(numero % i == 0)
                    {
                        divisores++;
                        Console.Write("{0} ", i);
                    }
                }

                // Comprobar si es primo
                if(divisores == 2)
                {
                    Console.WriteLine("El número {0} es primo.",numero);
                }
                else
                {
                    Console.WriteLine("El número {0} tiene {1} divisores.",numero,divisores);
                }
            }
            else
            {
                // Se acaba el programa, se indican las busquedadas de divisores realizadas y se cierra
                Console.WriteLine("El programa ha realizado {0} búsquedas de divisores",vueltas);
                Console.WriteLine("Hasta pronto.");
            }

            Console.WriteLine();

            // Sumamos una vuelta mas al contador de vueltas
            vueltas++; // es lo mismo que poner vueltas = vueltas + 1;
        }
        while (numero > 0);

    }
}