﻿public class EJ63
{
    public static void Main()
    {
        /* 
        Realiza una nueva versión del programa de la contraseña de acceso con 3 intentos, 
        pero esta vez pidiendo un login (que será una cadena de texto) y también una clave (otra cadena de texto). 
        Usa una variable booleana llamada "acertado" y otra llamada "intentosAgotados".
         */

        bool acertada = false;
        bool intentosAgotados = false;
        int fallos = 0;
        string user, pass;
        string userCorrecto = "Jon";
        string passCorrecto = "123";

        while(acertada == false && !intentosAgotados)
        {
            Console.Write("Usuario: ");
            user = Console.ReadLine();

            Console.Write("Password: ");
            pass = Console.ReadLine();

            if(user == userCorrecto && pass == "123")
            {
                acertada = true;
            }
            else
            {
                fallos++;
                Console.WriteLine("Error, intentos consumidos {0}",fallos);
            }

            if(fallos == 3)
            {
                intentosAgotados = true;
            }
        }

        if (intentosAgotados)
        {
            
            Console.WriteLine("Demasiados errores, expulsado/a del programa");
        }
        else
        {
            Console.WriteLine("Acceso correcto");
        }
        
        
    }
}