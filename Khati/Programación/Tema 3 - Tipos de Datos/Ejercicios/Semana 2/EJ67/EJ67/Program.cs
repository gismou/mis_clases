﻿public class EJ67
{
    public static void Main()
    {
        /* 
             Crea una nueva versión del programa anterior, que no utilice 
            "números mágicos" en la orden "switch", sino constantes.
         */

        byte opcion;

        Console.WriteLine("1. Ver datos existentes");
        Console.WriteLine("2. Añadir un nuevo dato");
        Console.WriteLine("3. Buscar");
        Console.WriteLine("0. Salir");
        opcion = Convert.ToByte(Console.ReadLine());

        switch (opcion)
        {
            case 0:
                Console.WriteLine("Ha escogido la opcion \"{0}\": \"{1}\"", opcion, "Salir");
                break;
            case 1:
                Console.WriteLine("Ha escogido la opcion \"{0}\": \"{1}\"", opcion, "Ver datos existentes");
                break;
            case 2:
                Console.WriteLine("Ha escogido la opcion \"{0}\": \"{1}\"", opcion, "Añadir un nuevo dato");
                break;
            case 3:
                Console.WriteLine("Ha escogido la opcion \"{0}\": \"{1}\"", opcion, "Buscar");
                break;
            default:
                Console.WriteLine("Opción incorrecta");
                break;
        }

    }
}