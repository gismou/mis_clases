﻿public class EJ65
{
    public static void Main()
    {
        /* 
        Escribe un programa que pida un ancho y un alto (ambos serán "byte"), así como un carácter, y 
        muestre un trapecio creciente como este:

        Introduzca el ancho menor deseado: 5
        Introduzca el alto deseado: 3
        Introduzca el carácter: *
          *****
         *******
        *********
         */

        byte alto, ancho;
        char caracter;

        Console.Write("Introduzca el ancho menor deseado: ");
        ancho = Convert.ToByte(Console.ReadLine());
        Console.Write("Introduzca el alto deseado: ");
        alto = Convert.ToByte(Console.ReadLine());
        Console.Write("Introduzca el carácter: ");
        caracter = Convert.ToChar(Console.ReadLine());

        for (int i = 0; i < alto; i++)
        {
            for(int j = (ancho+i); j >= 0; j--)
            {
                Console.Write(caracter);
            }
            Console.WriteLine();
        }

        
    }
}