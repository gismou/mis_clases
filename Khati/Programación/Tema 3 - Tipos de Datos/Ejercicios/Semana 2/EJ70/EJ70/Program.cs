﻿using System;
class EJ70
{
    public static void Main()
    {
        /*
         * 70. Crea un programa en C# que pida al usuario una cadena
         * y la muestre encriptada de dos maneras diferentes: 
         * primero sumando 1 a cada carácter, luego con la operación XOR 1.
         */

        String cadena;
        String cadenaEncriptada1 = "";
        String cadenaEncriptada2 = "";

        Console.WriteLine("Introduce una cadena: ");
        cadena = Console.ReadLine();

        foreach(char caracter in cadena)
        {
            cadenaEncriptada1 += caracter + 1;
            cadenaEncriptada2 += caracter ^ 1;
        }

        Console.WriteLine("la cadena original : {0}", cadena);
        Console.WriteLine("la cadena encripada con suma de 1: {0}", cadenaEncriptada1);
        Console.WriteLine("la cadena encripada con XOR de 1: {0}", cadenaEncriptada2);
    }
}