﻿using System;
class EJ69
{
    public static void Main()
    {
        /*Escribe un programa que pida al usuario números reales de doble precisión y 
         * muestre su mínimo, máximo, suma y media después de cada paso. Terminará cuando introduzca la palabra "fin":
        Dato: 5
        Min = 5, Max = 5, Suma = 5, Media = 5
        Dato: 2.2
        Min = 2.2, Max = 5, Suma = 7.2, Media = 3.6
        Dato: fin
        ¡Hasta otra!
        Pista: deberás leer lo que introduzca el usuario como string, y convertir a dato numérico sólo en caso de que no sea la palabra "fin".
         */

        bool salir = false;
        String dato;
        double media, datoDouble;
        double suma = 0;
        double min = 99999999999999;
        double max = -99999999999999;

        int numerosIntroducidos = 0;

        while (!salir)
        {
            Console.Write("Dato: ");
            dato = Console.ReadLine();
            if (dato == "fin")
            {
                salir = true;
            }
            else
            {
                numerosIntroducidos++;
                datoDouble = Convert.ToDouble(dato);
                suma += datoDouble;
                if(datoDouble < min)
                {
                    min = datoDouble;
                }
                if(datoDouble > max)
                {
                    max = datoDouble;
                }
                media = suma / numerosIntroducidos;
                Console.WriteLine("Min = {0}, Max = {1}, Suma = {2}, Media = {3}", min, max, suma, media);
            };
        }
        Console.WriteLine("¡Hasta otra!");
    }
}