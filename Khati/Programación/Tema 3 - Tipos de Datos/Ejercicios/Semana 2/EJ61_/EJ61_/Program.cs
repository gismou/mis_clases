﻿using System;
public class Ej61
{
    static void Main()
    {
        /*
         Crea un programa que pida al usuario un símbolo y responda si se trata de un operador (+ - * / %),
        un símbolo de puntuación (. , ; : ), un dígito (0 al 9), o algún otro símbolo. 
        Debes emplear el tipo de datos "char" y hacerlo de dos formas distintas (en un mismo programa): primero usando "if" y 
        después empleando "switch". Recuerda agrupar casos cuando sea posible.
        */


        Console.Write("Escribe un simbolo: ");
        char simbolo = Convert.ToChar(Console.ReadLine());

        // CON IF
        if(simbolo == '+' || simbolo == '-' || simbolo == '*' || simbolo == '/' || simbolo == '%'){
            Console.WriteLine("El simbolo es un operador");
        }else{
            if(simbolo == '.' || simbolo == ',' || simbolo == ';' || simbolo == ':'){
                Console.WriteLine("El simbolo es un signo de puntuación");
            }else{
                if(simbolo >= '0' && simbolo <= '9'){
                    Console.WriteLine("El simbolo es un digito");
                }
                else {
                    Console.WriteLine("No se reconoce el simbolo");
                }
            }
        }

        // CON SWITCH
        switch (simbolo)
        {
            case '+':
            case '-':
            case '*':
            case '/':
            case '%':
                Console.WriteLine("El simbolo es un operador");
                break;
            case '.':
            case ',':
            case ';':
            case ':':
                Console.WriteLine("El simbolo es un signo de puntuación");
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                Console.WriteLine("El simbolo es un dígito");
                break;
            default:
                Console.WriteLine("No se reconoce el simbolo");
                break;
        }
    }
}