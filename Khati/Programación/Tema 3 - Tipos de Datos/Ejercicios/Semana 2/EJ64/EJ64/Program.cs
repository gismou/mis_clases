﻿public class EJ64
{
    public static void Main()
    {
        /* 
        Crea un programa que le pida al usuario dos números (reales de simple precisión)
        y una operación para realizar en ellos (+, -, *, x, ·, /) y 
        muestre el resultado de esa operación, como en este ejemplo:
        Introduzca el primer número: 5
        Introduzca la operación: ·
        Introduzca el segundo número: 7
        5 · 7 = 35
        Es decir, la multiplicación se podrá indicar con un asterisco, con una x o con un punto a media altura (que está disponible en la tecla del número 3).
         */

        float int1, int2;
        char operando;

        Console.WriteLine("Numero 1: ");
        int1 = Convert.ToSingle(Console.ReadLine());

        Console.WriteLine("Operando: ");
        operando = Convert.ToChar(Console.ReadLine());

        Console.WriteLine("Numero 2: ");
        int2 = Convert.ToSingle(Console.ReadLine());

        switch (operando)
        {
            case '*':
            case 'x':
            case '·':
                Console.WriteLine("{0} {1} {2} = {3}", int1, operando, int2, int1 * int2);
                break;
            case '-':
                Console.WriteLine("{0} {1} {2} = {3}", int1, operando, int2, int1 - int2);
                break;
            case '+':
                Console.WriteLine("{0} {1} {2} = {3}", int1, operando, int2, int1 + int2);
                break;
            case '/':
                Console.WriteLine("{0} {1} {2} = {3}", int1, operando, int2, int1 / int2);
                break;
            case '%':
                Console.WriteLine("{0} {1} {2} = {3}", int1, operando, int2, int1 % int2);
                break;
            default:
                Console.WriteLine("No se reconoce el operando");
                break;
        }
    }
}
