﻿using System;

public class EJ62
{
    public static void Main()
    {
        /*
         * Escribe un programa que pida al usuario una frase y muestre las cifras numéricas que contenga.
         * Por ejemplo, si el usuario introduce "12 es más que 3 y que 4", 
         * el programa escribirá "1234". Hazlo usando "foreach".
         */

        String cadena;

        Console.Write("Escribe una cadena: ");
        cadena = Console.ReadLine();

        foreach (char caracter in cadena)
        {
            if (caracter >= '0' && caracter <= '9') Console.Write(caracter);
        }
    }
}