﻿public class EJ66
{
    public static void Main()
    {
        /* 
             Crea un menú como el siguiente. El usuario deberá escoger la opción 0, 1 o 2, y después se le mostrará la opción que ha escogido, 
            usando "switch" y datos de tipo "byte":

            1. Ver datos existentes
            2. Añadir un nuevo dato
            3. Buscar
            0. Salir
            1
            Ha escogido la opción "1": "Ver datos existentes"
         */

        byte opcion;

        Console.WriteLine("1. Ver datos existentes");
        Console.WriteLine("2. Añadir un nuevo dato");
        Console.WriteLine("3. Buscar");
        Console.WriteLine("0. Salir");
        opcion = Convert.ToByte(Console.ReadLine());

        switch (opcion)
        {
            case 0:
                Console.WriteLine("Ha escogido la opcion \"{0}\": \"{1}\"",opcion,"Salir");
                break;
            case 1:
                Console.WriteLine("Ha escogido la opcion \"{0}\": \"{1}\"", opcion, "Ver datos existentes");
                break;
            case 2:
                Console.WriteLine("Ha escogido la opcion \"{0}\": \"{1}\"", opcion, "Añadir un nuevo dato");
                break;
            case 3:
                Console.WriteLine("Ha escogido la opcion \"{0}\": \"{1}\"", opcion, "Buscar");
                break;
            default:
                Console.WriteLine("Opción incorrecta");
                break;
        }

    }
}