﻿using System;

public class Ej55
{
    static void Main()
    {
        /*
         * Pide al usuario dos números enteros cortos y muestra todos los números entre ellos
         * en hexadecimal, en la misma línea, separados por un espacio. 
         * Por ejemplo, si introduce 7 y 12, deberás mostrar "7 8 9 a b c". 
         * El programa se debe comportar correctamente si introduce los números en orden contrario, 
         * es decir, si primero indica 12 y 7 en vez de 7 y 12.
         */

        short n1, n2, mayor, menor;

        Console.Write("N1: ");
        n1 = Convert.ToInt16(Console.ReadLine());
        Console.Write("N2: ");
        n2 = Convert.ToInt16(Console.ReadLine());

        if(n1 > n2)
        {
            mayor = n1;
            menor = n2;
        }
        else
        {
            mayor = n2;
            menor = n1;
        }

        for(int i = menor; i <= mayor; i++)
        {
            Console.Write("{0} ", Convert.ToString(i, 16));
        }

    }
}