﻿using System;
public class Ej52
{
    static void Main()
    {
        /*
         * Crea una versión mejorada del ejercicio 8: un programa que pida al usuario una 
         * cantidad de pies y muestre su equivalencia en metros (1 pie = 0,305 metros). 
         * Debe emplear tres variables: pies, metros, metrosPorPie, 
         * todas ellas números reales de simple precisión (float). 
         * Debe mostrar toda la información en una línea, algo como "2 pies son 0,61 m".
         */

        float pies, metros;
        float metrosPorPie = 0.305f;

        System.Console.Write("Pies: ");
        pies = Convert.ToSingle(Console.ReadLine());

        metros = pies * metrosPorPie;

        Console.WriteLine("{0} pies son {1} m", pies, metros);
    }
}

