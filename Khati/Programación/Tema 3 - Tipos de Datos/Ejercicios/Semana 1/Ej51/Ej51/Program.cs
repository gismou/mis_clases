﻿using System;
class Ej51
{
    /*
     * Crea un programa en C# que pida al usuario su edad, su año de nacimiento,
     * su estatura en centímetros, cuántos euros (sin céntimos) ahorró el último mes,
     * la población de su ciudad y la población estimada del mundo.
     * Debes optimizar los tipos de datos usados (todos ellos serán números enteros).
     */

    /*
     * 1 Byte = sbyte, byte
     * 2 Byte = short, ushort
     * 4 Byte = int, uint
     * 8 Byte = long, ulong
     */

    /*
     * Para datos de tipo "byte" usaremos Convert.ToByte (sin signo) y ToSByte (con 
       Para datos de 2 bytes (short) tenemos ToInt16 (con signo) y ToUInt16 (sin signo),
       Para datos de 4 bytes tenemos ToInt32,
       Para los de 8 bytes (long) existen ToInt64 (con signo) y ToUInt64 (sin signo). 
       Para los enteros de 32 bits sin signo se empleará ToUInt32
       Para los números reales simples (float) Convert.ToSingle()
       Para los números reales dobles (double) Concvert.ToDouble()
     */
    static void Main()
    {
        byte edad; // 1 Byte
        ushort nacimiento; // 2 Byte
        float estatura; // Coma Flotante
        double ahorros; // Numero real double
        uint poblacionCiudad; // 4 Byte
        ulong poblacionMundial; // 8 Byte

        try
        {
            System.Console.Write("Edad: ");
            edad = Convert.ToByte(Console.ReadLine());
            System.Console.Write("Año Nacimiento: ");
            nacimiento = Convert.ToUInt16(Console.ReadLine());
            System.Console.Write("Estatura en cm: ");
            estatura = Convert.ToSingle(Console.ReadLine());
            System.Console.Write("Euros ahorrados: ");
            ahorros = Convert.ToDouble(Console.ReadLine());
            System.Console.Write("Población de la ciudad: ");
            poblacionCiudad = Convert.ToUInt32(Console.ReadLine());
            System.Console.Write("Población del mundo: ");
            poblacionMundial = Convert.ToUInt64(Console.ReadLine());

            Console.WriteLine("\nDatos Obtenidos");
            Console.WriteLine("Edad: {0}", edad);
            Console.WriteLine("Nacimiento: {0}", nacimiento);
            Console.WriteLine("Estatura: {0}", estatura);
            Console.WriteLine("Ahorros: {0}", ahorros);
            Console.WriteLine("Población Ciudad: {0}", poblacionCiudad);
            Console.WriteLine("Población Mundial: {0}", poblacionMundial);
        }
        catch (Exception ex) { Console.WriteLine(ex.ToString()); }

    }
}