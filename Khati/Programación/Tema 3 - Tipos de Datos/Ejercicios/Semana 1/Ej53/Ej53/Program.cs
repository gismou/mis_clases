﻿using System;
public class Ej52
{
    static void Main()
    {
        /*
         * Calcula la superficie de un círculo, a partir de su radio, que introducirá el usuario en una
         * variable real de doble precisión llamada "radio". 
         * (Recuerda que la fórmula es: area = pi * radio al cuadrado). 
         * Tanto el valor de "pi" como el resultado (la longitud) deben estar almacenados en variables,
         * que también serán números reales de doble precisión. 
         * Recuerda que para elevar x al cuadrado, basta con hacer x*x.
         */

        double radio, area;
        double pi = 3.14159265;

        System.Console.Write("Radio: ");
        radio = Convert.ToDouble(Console.ReadLine());

        area = pi * (radio*radio);

        Console.WriteLine("El área de un circulo de radio {0} es {1}", radio, area);
    }
}