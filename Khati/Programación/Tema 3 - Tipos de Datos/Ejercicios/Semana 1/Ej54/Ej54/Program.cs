﻿using System;

public class Ej54
{
    static void Main()
    {
        /*
         * Calcula el volumen de un cono, a partir de su radio y su altura, que introducirá el usuario.
         * El volumen será "pi por el radio al cuadrado, multiplicado por la altura y dividido entre 3".
         * Debes utilizar variables "pi", "radio" y "volumen", de tipo "float" y 
         * mostrar los resultados con dos decimales.
         */

        float radio, volumen, altura;
        float pi = 3.14159265f;

        Console.Write("Radio: ");
        radio = Convert.ToSingle(Console.ReadLine());
        Console.Write("Altura: ");
        altura = Convert.ToSingle(Console.ReadLine());

        volumen = pi * (radio * radio) * altura / 3;

        Console.WriteLine("El volumen de un cono de radio {0} y altura {1} es de {2}", radio, altura, volumen.ToString("N2"));
    }
}