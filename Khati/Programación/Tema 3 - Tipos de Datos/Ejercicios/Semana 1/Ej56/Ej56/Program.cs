﻿using System;

public class Ej56
{
    static void Main()
    {
        /*
         Pide al usuario un número entero largo y respóndele cuántas cifras tiene. 
         Lo puedes conseguir dividiendo entre 10 tantas veces como sea necesaria hasta que el número 
         se convierta en 0.
         */

        short cifras = 1;
        long n1, res;

        Console.Write("N1: ");
        n1 = Convert.ToInt32(Console.ReadLine());

        res = n1;

        while(res / 10 > 0)
        {
            cifras++;
            res /= 10;
        }

        Console.WriteLine("El número {0} tiene {1} cifras",n1,cifras);

    }
}