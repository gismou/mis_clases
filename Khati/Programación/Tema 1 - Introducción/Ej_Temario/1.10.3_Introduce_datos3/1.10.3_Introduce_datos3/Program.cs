﻿class IntroducirDatos3
{
    static void Main()
    {
        int n1;
        int n2;
        int n3;
        int res;

        System.Console.WriteLine("Introduce el primer número: ");
        n1 = System.Convert.ToInt32(System.Console.ReadLine());
        System.Console.WriteLine("Introduce el segundo número: ");
        n2 = System.Convert.ToInt32(System.Console.ReadLine());
        System.Console.WriteLine("Introduce el tercer número: ");
        n3 = System.Convert.ToInt32(System.Console.ReadLine());
        res = n1 + n2 + n3;
        System.Console.WriteLine("El resultado de {0} mas {1} y mas {2} es {3}", n1, n2, n3, res);
    }
}   