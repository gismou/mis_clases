﻿class IntroducirDatos2
{
    static void Main()
    {
        int n1;
        int n2;
        int res;
        int res2;

        System.Console.WriteLine("Introduce el primer número: ");
        n1 = System.Convert.ToInt32(System.Console.ReadLine());
        System.Console.WriteLine("Introduce el segundo número: ");
        n2 = System.Convert.ToInt32(System.Console.ReadLine());
        res = n1 / n2;
        res2 = n1 % n2;
        System.Console.WriteLine("El resultado de {0} dividido por {1} es {2} y su resto es {3}", n1, n2, res, res2);
    }
}