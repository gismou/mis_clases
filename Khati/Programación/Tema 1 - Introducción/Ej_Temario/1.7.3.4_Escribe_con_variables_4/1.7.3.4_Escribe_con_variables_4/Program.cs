﻿class EscribeConVariables4
{
    static void Main()
    {
        int n1 = 285;
        int n2 = 1396;
        int n3 = 5;
        int res = n1 + n2 * n3;
        System.Console.WriteLine("La suma de {0} y {1} multiplicada por {3} es {2}", n1, n2, res, n3);
    }
}