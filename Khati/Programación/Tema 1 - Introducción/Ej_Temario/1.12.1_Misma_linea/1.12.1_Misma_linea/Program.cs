﻿class MismaLinea
{
    static void Main()
    {
        int n1;
        int n2;
        int res1;
        int res2;


        System.Console.Write("Introduce el primer número: ");
        n1 = System.Convert.ToInt32(System.Console.ReadLine());
        System.Console.Write("Introduce el segundo número: ");
        n2 = System.Convert.ToInt32(System.Console.ReadLine());

        res1 = (n1 + n2) * (n1 - n2);
        res2 = n1 * n1 - n2 * n2;

        System.Console.Write("El resultado de ({0}+{1})*({0}-{1}) es {2} y el resultado de {0} al cuadrado menos {1} al cuadrado es {3}", n1, n2, res1, res2);
    }
}