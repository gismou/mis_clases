﻿class IntroducirDatos
{
    static void Main()
    {
        int n1;
        int n2;
        int res;

        System.Console.WriteLine("Introduce el primer número: ");
        n1 = System.Convert.ToInt32(System.Console.ReadLine());
        System.Console.WriteLine("Introduce el segundo número: ");
        n2 = System.Convert.ToInt32(System.Console.ReadLine());
        res = n1 * n2;
        System.Console.WriteLine("El resultado de {0} por {1} es {2}",n1,n2,res);
    }
}