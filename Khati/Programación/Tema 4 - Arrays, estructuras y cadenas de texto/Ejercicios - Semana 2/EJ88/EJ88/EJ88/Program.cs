﻿using System;
public class EJ88
{
    struct fecha
    {
        public int mes;
        public int anyo;
    }

    struct videojuego
    {
        public string titulo;
        public string plataforma;
        public int mb;
        public fecha fechaEstreno;
    }
    static void Main()
    {
        /*
         * 
            Crea un array que permita almacenar hasta 1000 juegos de ordenador o consola.
            De cada juego se guardará: título (cadena de texto), plataforma (cadena de texto), espacio ocupado 
            (en MB, número entero) y fecha de lanzamiento (mes y año, en forma de struct anidado).
            El programa debe permitir al usuario realizar las siguientes operaciones:
            1-  Añadir datos de un nuevo juego (Se debe comprobar si cabe, y avisar en caso contrario).
            2-  Mostrar los nombres y plataformas de todos los juegos almacenados 
                (ambos datos en una misma línea, con el formato "The last of us (PS3)". 
                Cada juego debe aparecer en una línea distinta, precedido por el número de registro (empezando a contar desde 1). 
                Si hay más de 20 juegos, se deberá hacer una pausa tras mostrar cada bloque de 20 juegos, 
                y esperar a que el usuario pulse Intro antes de seguir. 
                Se deberá avisar si no hay datos.
            3-  Ver todos los datos de un cierto programa (a partir de su número de registro, contando desde 1). 
                Se deberá avisar (pero no volver a pedir) si el número no es válido.
            4-  Modificar una ficha (se pedirá el número y se volverá a introducir el valor de todos los campos. 
                Se debe avisar (pero no volver a pedir) si introduce un número de ficha incorrecto. 
                No hará falta volver a teclear todos los datos, sino que se debe permitir que el usuario pulse 
                Intro sin introducir nada cuando desee conservar el valor previo de un campo, 
                y sólo sea necesario que vuelva a teclear los campos que desee modificar.
            5-  Borrar un cierto dato, a partir del número de registro que indique el usuario. 
                Se debe avisar (pero no volver a pedir) si introduce un número incorrecto.
            T-  Terminar (como no sabemos almacenar la información, los datos se perderán).
         */

        videojuego videojuego = new videojuego();
        const int TOTAL_POSICIONES = 5;
        videojuego[] videojuegos = new videojuego[TOTAL_POSICIONES];
        int opcion;
        int posicionesOcupadas = 0;

        do
        {
            Console.WriteLine("Opciones\n---------------------------------");
            Console.WriteLine("1. Añadir datos de un nuevo juego");
            Console.WriteLine("2. Mostrar los nombres y plataformas de todos los juegos almacenados");
            Console.WriteLine("3. Ver todos los datos de un cierto programa a partir de su número de registro");
            Console.WriteLine("4. Modificar una ficha a partir de un número de registro");
            Console.WriteLine("5. Borrar un dato a partir del numero de registro");
            Console.WriteLine("0. Salir");
            opcion = Convert.ToInt32(Console.ReadLine());

            switch (opcion)
            {
                case 1:
                    // Datos del nuevo juego
                    if(posicionesOcupadas+1 > TOTAL_POSICIONES)
                    {
                        Console.WriteLine("No caben mas juegos en la lista");
                    }
                    else
                    {
                        Console.Write("\nTítulo: ");
                        videojuego.titulo = Console.ReadLine();
                        Console.Write("Plataforma: ");
                        videojuego.plataforma = Console.ReadLine();
                        Console.Write("MB: ");
                        videojuego.mb = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Mes Lanzamiento: ");
                        videojuego.fechaEstreno.mes = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Año Lanzamiento: ");
                        videojuego.fechaEstreno.anyo = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine();
                        videojuegos[posicionesOcupadas] = videojuego;
                        posicionesOcupadas++;
                    }
                    break;
                case 2:
                    // Mostrar titulos
                    Console.WriteLine();
                    if (posicionesOcupadas != 0)
                    {
                        int cont = 1;
                        for (int i = 0; i < posicionesOcupadas; i++)
                        {
                            Console.WriteLine("{0}. {1} ({2})", i + 1, videojuegos[i].titulo, videojuegos[i].plataforma);
                            if (cont == 20)
                            {
                                Console.Write("Pusa enter para mostrar mas ... ");
                                Console.ReadLine();
                                cont = 0;
                            }
                            cont++;
                        }
                    }
                    else
                    {
                        Console.Write("No hay juegos en la lista");
                    }
                    Console.WriteLine();
                    break;
                case 3:
                    // Datos completos del juego
                    Console.Write("\nRegistro a visualizar: ");
                    int regVis = Convert.ToInt32(Console.ReadLine());
                    if (regVis > 0 && regVis <= posicionesOcupadas)
                    {
                        Console.WriteLine("\n------------------");
                        Console.WriteLine("- Titulo: {0}", videojuegos[regVis - 1].titulo);
                        Console.WriteLine("- Plataforma: {0}", videojuegos[regVis - 1].plataforma);
                        Console.WriteLine("- MB: {0}", videojuegos[regVis - 1].mb);
                        Console.WriteLine("- Fecha Lanzamiento: {0}-{1}", videojuegos[regVis - 1].fechaEstreno.mes, videojuegos[regVis - 1].fechaEstreno.anyo); ;
                        Console.WriteLine("------------------\n");
                    }
                    else
                    {
                        Console.WriteLine("Registro inexistente");
                    }
                    break;
                case 4:
                    // Modificar Registro
                    Console.Write("\nRegistro a modificar: ");
                    int regMod = Convert.ToInt32(Console.ReadLine());
                    if (regMod > 0 && regMod <= posicionesOcupadas)
                    {
                        Console.Write("\nTítulo: ");
                        string titulo = Console.ReadLine();
                        if(titulo != "")
                        {
                            videojuegos[regMod - 1].titulo = titulo;
                        }
                        Console.Write("Plataforma: ");
                        string plataforma = Console.ReadLine();
                        if(plataforma != "")
                        {
                            videojuegos[regMod - 1].plataforma = plataforma;
                        }
                        Console.Write("MB: ");
                        string mb = Console.ReadLine();
                        if(mb != "")
                        {
                            videojuegos[regMod - 1].mb = Convert.ToInt32(mb); ;
                        }
                        Console.Write("Mes Lanzamiento: ");
                        string mes = Console.ReadLine();
                        if(mes != "")
                        {
                            videojuegos[regMod - 1].fechaEstreno.mes = Convert.ToInt32(mes);
                        }
                        Console.Write("Año Lanzamiento: ");
                        string anyo = Console.ReadLine();
                        if(anyo != "")
                        {
                            videojuegos[regMod - 1].fechaEstreno.anyo = Convert.ToInt32(anyo);
                        }
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine("Registro inexistente");
                    }                
                    break;
                case 5:
                    // Borrar registro
                    Console.Write("\nRegistro a borrar: ");
                    int regBorrar = Convert.ToInt32(Console.ReadLine());
                    if (regBorrar > 0 && regBorrar <= posicionesOcupadas)
                    {
                        for (int i = regBorrar - 1; i < posicionesOcupadas - 1; i++)
                        {
                            videojuegos[i] = videojuegos[i + 1];
                        }
                        posicionesOcupadas--;
                    }
                    else
                    {
                        Console.WriteLine("Registro inexistente");
                    }                 
                    break;
                case 0:
                    Console.WriteLine("\nHasta otra!");
                    break;
            }
        } while (opcion != 0);
    }
}