﻿using System;
public class EJ82
{
    /*
     * 82. Crea una variante del programa anterior, que pregunte al usuario cuántos datos guardará en un primer bloque de números reales de simple precisión,
     * luego cuántos datos guardará en un segundo bloque, y finalmente pida los datos en sí. 
     * Los debe guardar en un array de arrays. 
     * Después calculará y mostrará el promedio de los valores que hay guardados en el primer subarray, 
     * luego el promedio de los valores en el segundo subarray 
     * y finalmente el promedio global.
     */
    static void Main()
    {
        // Declarar variables
        float numUsuario, suma1, suma2, sumaTotal, promedio1, promedio2, promedioTotal;
        int numeroFilas1, numeroFilas2;
        float[][] matrizNumeros = new float[2][];

        // Preguntar datos a guardar del primer bloque
        Console.Write("Numero de datos del primer bloque: ");
        numeroFilas1 = Convert.ToInt32(Console.ReadLine());

        // Declarar tamaño de primer bloque de la matriz y darle valores
        matrizNumeros[0] = new float[numeroFilas1];
        for (int i = 0; i < numeroFilas1; i++)
        {
            Console.Write("{0}. Numero:  ", i);
            numUsuario = Convert.ToInt32(Console.ReadLine());
            matrizNumeros[0][i] = numUsuario;
        }

        // Preguntar datos a guardar del segundo bloque
        Console.Write("Numero de datos del segundo bloque: ");
        numeroFilas2 = Convert.ToInt32(Console.ReadLine());

        // Declarar tamaño de primer bloque de la matriz y darle valores
        matrizNumeros[1] = new float[numeroFilas2];
        for (int i = 0; i < numeroFilas2; i++)
        {
            Console.Write("{0}. Numero:  ", i);
            numUsuario = Convert.ToInt32(Console.ReadLine());
            matrizNumeros[1][i] = numUsuario;
        }

        // Recorremos el array y obtenemos las sumas
        suma1 = 0;
        suma2 = 0;
        for(int i = 0;  i < matrizNumeros.Length; i++)
        {
            for(int j = 0; j < matrizNumeros[i].Length; j++)
            {
                Console.Write("{0} ", matrizNumeros[i][j]);
                if(i == 0)
                {
                    suma1 += matrizNumeros[i][j];
                }
                else
                {
                    suma2 += matrizNumeros[i][j];
                }
            }
            Console.WriteLine();
        }

        // Calcular promedios y mostrarlos
        promedio1 = suma1 / numeroFilas1;
        promedio2 = suma2 / numeroFilas2;
        promedioTotal = (suma1 + suma2) / (numeroFilas1 + numeroFilas2);

        Console.WriteLine("El promedio de la fila 1 es {0}", promedio1);
        Console.WriteLine("El promedio de la fila 2 es {0}", promedio2);
        Console.WriteLine("El promedio total es {0}", promedioTotal);

    }
}