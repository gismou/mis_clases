﻿using System;
public class EJ81
{
    /*
     * 81. Crea un programa que pida al usuario 12 números reales de simple precisión, 
     * los guarde en una matriz bidimensional de 2x6 datos, 
     * y luego calcule y muestre el promedio de los valores que hay guardados en la primera mitad de la matriz, 
     * luego el promedio de los valores en la segunda mitad de la matriz y finalmente el promedio global.
     */
    static void Main()
    {
        float numeroUsuario;
        float promedio1, promedio2, promedioTotal, suma1, suma2, sumaTotal;

        // Declarar la matriz
        float[][] matrizNumeros = new float[2][];
        matrizNumeros[0] = new float[6];
        matrizNumeros[1] = new float[6];

        // Rellenar la matriz 
        for (int i = 0; i < matrizNumeros.Length; i++)
        {
            for(int j = 0; j < matrizNumeros[i].Length; j++)
            {
                Console.Write("Introduce un numero: ");
                numeroUsuario = Convert.ToSingle(Console.ReadLine());
                matrizNumeros[i][j] = numeroUsuario;
            } 
        }

        // Recorrer la matriz para obtener la suma de cada fila
        suma1 = 0;
        suma2 = 0;
        for(int i = 0;i < matrizNumeros.Length;i++)
        {
            for (int j = 0; j < matrizNumeros[i].Length; j++)
            {
                Console.Write("{0} ", matrizNumeros[i][j]);
                if (i == 0)
                {
                    suma1 += matrizNumeros[i][j];
                }
                else
                {
                    suma2 += matrizNumeros[i][j];
                }
            }
            Console.WriteLine();
        }

        // Calculamos los promedios
        promedio1 = suma1 / 6;
        promedio2 = suma2 / 6;
        promedioTotal = (suma1 + suma2) / 12;

        Console.WriteLine();
        Console.WriteLine("El promedio de la fila 1 es {0}", promedio1);
        Console.WriteLine("El promedio de la fila 2 es {0}", promedio2);
        Console.WriteLine("El promedio total es {0}", promedioTotal);

    }
}