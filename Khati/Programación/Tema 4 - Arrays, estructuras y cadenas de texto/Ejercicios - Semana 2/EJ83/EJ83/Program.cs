﻿using System;
public class EJ82
{
    /*
      83. Crea un "struct" para almacenar algunos datos de juegos de ordenador o consola, 
      de momento sólo: título (cadena de texto), plataforma (cadena de texto), espacio ocupado (en MB, número entero). 
      Pide al usuario datos de 2 juegos (sin usar todavía ningún array) y luego muéstralos.
     */

    struct videojuego
    {
        public string titulo;
        public string plataforma;
        public int mb;
    }

    static void Main()
    {
        // Declarar objeto videojuego 1
        videojuego vid1 = new videojuego();
        Console.Write("Titulo juego 1: ");
        vid1.titulo = Console.ReadLine();
        Console.Write("Plataforma juego 1: ");
        vid1.plataforma = Console.ReadLine();
        Console.Write("MB juego 1: ");
        vid1.mb = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine();

        // Declarar objeto videojuego 2
        videojuego vid2 = new videojuego();
        Console.Write("Titulo juego 2: ");
        vid2.titulo = Console.ReadLine();
        Console.Write("Plataforma juego 2: ");
        vid2.plataforma = Console.ReadLine();
        Console.Write("MB juego 2: ");
        vid2.mb = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine();

        // Datos de videojuegos 1 y 2
        Console.WriteLine("Videojuego 1\n------------------");
        Console.WriteLine("Titulo: {0}",vid1.titulo);
        Console.WriteLine("Plataforma: {0}", vid1.plataforma);
        Console.WriteLine("MB: {0}", vid1.mb);

        Console.WriteLine();

        Console.WriteLine("Videojuego 2\n------------------");
        Console.WriteLine("Titulo: {0}", vid2.titulo);
        Console.WriteLine("Plataforma: {0}", vid2.plataforma);
        Console.WriteLine("MB: {0}", vid2.mb);
    }
}