﻿using System;
using System.Text; // Usaremos un System.Text.StringBuilder
class Ejemplo_04_04_09a
{
    static void Main()
    {
        StringBuilder cadenaModificable = new StringBuilder("Hola");
        cadenaModificable[0] = 'M';
        Console.WriteLine("Cadena modificada: {0}",
        cadenaModificable);
        string cadenaNormal;
        cadenaNormal = cadenaModificable.ToString();
        Console.WriteLine("Cadena normal a partir de ella: {0}",
        cadenaNormal);
    }
}
