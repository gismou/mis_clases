﻿using System;
public class EJ73
{
    static void Main()
    {
        /*
        73. Pide al usuario 10 enteros cortos sin signo y guárdalos en un array. Luego pide uno más
        y dile si estaba entre esos 10 datos iniciales, de 2 formas distintas: primero usando un booleano 
        y luego usando un contador, para, en la segunda ocasión, responderle cuántas veces aparecía 
        (ambas respuestas serán parte del mismo programa, no dos programas independientes).
         */

        short MAXIMO = 10;
        short[] numeros = new short[MAXIMO];
        short numUs;
        short numBuscar;
        Boolean encontrado;
        short cont = 0;

        for(int i = 1; i<=10; i++)
        {
            Console.Write("Escribe un numero ({0}): ",i);
            numUs = Convert.ToInt16(Console.ReadLine());
            numeros[i - 1] = numUs;
        }

        Console.WriteLine();

        foreach (short num in numeros)
        {
            Console.Write("{0} ", num);
        }

        Console.Write("\nNumero a buscar: ");
        numBuscar = Convert.ToInt16(Console.ReadLine());

        encontrado = false;
        foreach (short num in numeros)
        {
            if (num == numBuscar)
            {
                encontrado = true;
                cont++;
            }
        }

        if (encontrado)
        {
            Console.WriteLine("El numero {0} se ha encontado {1} veces", numBuscar, cont);
        }
        else
        {
            Console.WriteLine("El numero {0} NO se ha encontrado", numBuscar);
        }

    }
}