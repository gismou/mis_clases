﻿using System;
public class EJ75
{
    static void Main()
    {
        /*
         * 75. Crea un array de números reales de simple precisión, con espacio para 10 datos.
         * Pide al usuario esos 10 datos y luego muestra un menú que le permita: Ver todos los datos en el orden en que se habían
         * introducido, calcular y mostrar el máximo de los datos, calcular y mostrar el mínimo de los datos, buscar (ver si está almacenado) 
         * un cierto dato, salir. La opción de Buscar preguntará el dato que se quiere localizar y responderá si era parte de los 10 
         * datos iniciales o no lo era.
         */

        float[] numeros = new float[10];
        float numUsuario;
        float numBuscar;
        int opcion;
        float x;
        Boolean encontrado;

        for (int i = 0; i < numeros.Length; i++)
        {
            Console.Write("Introduce un numero ({0}): ", i + 1);
            numUsuario = Convert.ToSingle(Console.ReadLine());
            numeros[i] = numUsuario;
        }

        do
        {

            Console.WriteLine("\n0. Ver datos");
            Console.WriteLine("1. Maximo de los datos");
            Console.WriteLine("2. Minimo de los datos");
            Console.WriteLine("3. Buscar dato");
            Console.WriteLine("4. Salir");
            Console.Write("Elige opcion: ");
            opcion = Convert.ToInt32(Console.ReadLine());

            switch (opcion)
            {
                case 0:
                    // Mostar numeros del array
                    foreach (float numero in numeros)
                    {
                        Console.Write("{0} ", numero);
                    }
                    Console.WriteLine();
                    break;
                case 1:
                    // Mayor numero
                    x = numeros[0];
                    foreach (float numero in numeros)
                    {
                        if (numero > x)
                        {
                            x = numero;
                        }
                    }
                    Console.WriteLine("\nEl mayor numero es {0}\n", x);
                    break;
                case 2:
                    // Menor numero
                    x = numeros[0];
                    foreach (float numero in numeros)
                    {
                        if (numero < x)
                        {
                            x = numero;
                        }
                    }
                    Console.WriteLine("\nEl menor numero es {0}\n", x);
                    break;
                case 3:
                    // Buscar numero
                    encontrado = false;
                    Console.Write("Numero a buscar: ");
                    numBuscar = Convert.ToSingle(Console.ReadLine());
                    foreach (float numero in numeros)
                    {
                        if (numero == numBuscar)
                        {
                            encontrado = true;
                        }
                    }
                    if (encontrado)
                    {
                        Console.WriteLine("\nSe ha encontrado el numero\n");
                    }
                    else
                    {
                        Console.WriteLine("\nNO se ha encontrado el numero\n");
                    }
                    break;
                case 4:
                    Console.WriteLine("\nHasta pronto!\n");
                    break;
                default:
                    Console.WriteLine("\nNo existe la opcion elegida\n");         
                    break;
            }

        }while(opcion != 4);


    }    
}