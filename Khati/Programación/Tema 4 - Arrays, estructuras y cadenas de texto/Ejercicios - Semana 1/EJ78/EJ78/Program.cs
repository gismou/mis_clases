﻿using System;
public class EJ78
{
    static void Main()
    {
        /*
         78. Crea un variante mejorada del programa que devuelve el cambio de una compra, utilizando siempre en primer lugar monedas
        (o billetes) del valor más grande posible. 
        Supondremos que tenemos una cantidad ilimitada de monedas (o billetes) de 100, 50, 10, 5 y 1, y que no hay decimales. 
        La ejecución podría ser algo como:

        Precio? 222
        Pagado? 500
        Tu cambio es 378: 100 100 100 50 10 10 5 1 1 1 
        En este caso, las monedas (o billetes) disponibles estarán prefijados en un array, para que resulte menos repetitivo.
         */

        int[] monedasBilletes = { 100, 50, 10, 5, 1 };
        int precio, pagado, cambio, monedaBillete, total;

        Console.Write("Precio?: ");
        precio = Convert.ToInt32(Console.ReadLine());

        Console.Write("Pagado?: ");
        pagado = Convert.ToInt32(Console.ReadLine());

        cambio = pagado - precio;
        total = cambio;

        Console.Write("Tu cambio es {0}: ",cambio);
        for(int i = 0; i<monedasBilletes.Length; i++)
        {
            // monedaBilleta recorrido
            monedaBillete = monedasBilletes[i];
            while(total >= monedaBillete)
            {
                Console.Write("{0} ", monedaBillete);
                total -= monedaBillete;
            }
        }
    }
}