﻿using System;
public class EJ76
{
    static void Main()
    {
        /*
         76. Crea una variante del ejercicio anterior en la que reserves espacio para 1000 datos (reales de simple precisión),
             aunque el usuario típicamente introducirá muchos menos. El menú permitirá: Añadir un nuevo dato, ver todos los datos, 
             calcular y mostrar el máximo de los datos, calcular y mostrar el mínimo de los datos, 
             buscar un cierto dato, salir. 
             La opción de Buscar preguntará el dato que se quiere localizar y responderá si está o no en el array.
         */

        float[] numeros = new float[1000];
        int numerosIntroducir;
        float numUsuario;
        int opcion;
        float x;
        Boolean encontrado;

        Console.Write("Numeros a introducir: ");
        numerosIntroducir = Convert.ToInt32(Console.ReadLine());

        for(int i = 0; i < numerosIntroducir; i++)
        {
            Console.Write("Numero {0}: ",i+1);
            numeros[i] = Convert.ToSingle(Console.ReadLine());
        }

        Console.WriteLine();

        do
        {
            Console.WriteLine("0. Ver numeros");
            Console.WriteLine("1. Maximo numero");
            Console.WriteLine("2. Minimo numero");
            Console.WriteLine("3. Buscar");
            Console.WriteLine("4. Agregar numero");
            Console.WriteLine("5. Salir");
            opcion = Convert.ToInt32(Console.ReadLine());

            switch (opcion)
            {
                case 0: 
                    Console.WriteLine();
                    foreach(float numero in numeros)
                    {
                        Console.Write("{0} ", numero);
                    }
                    Console.WriteLine("\n");
                    break;
                case 1:
                    x = numeros[0];
                    foreach(float numero in numeros)
                    {
                        if(numero > x)
                        {
                            x = numero;
                        }
                    }
                    Console.WriteLine("\nEl mayor es {0}\n",x);
                    break;
                case 2:
                    x = numeros[0];
                    foreach (float numero in numeros)
                    {
                        if(numero < x)
                        {
                            x = numero;
                        }
                    }
                    Console.WriteLine("\nEl menor es {0}\n", x);
                    break;
                case 3:
                    encontrado = false;
                    Console.Write("\nNumero a buscar: ");
                    numUsuario = Convert.ToSingle(Console.ReadLine());
                    foreach(float numero in numeros)
                    {
                        if(numUsuario == numero)
                        {
                            encontrado = true;
                        }
                    }
                    if (encontrado)
                    {
                        Console.WriteLine("Se ha encontrado\n");
                    }
                    else
                    {
                        Console.WriteLine("NO se ha encontrado\n");
                    }
                    break;
                case 4:
                    Console.Write("\nNuevo numero a introducir: \n");
                    numUsuario = Convert.ToSingle(Console.ReadLine());
                    numeros[numerosIntroducir - 1] = numUsuario;
                    numerosIntroducir++;
                    break;
                case 5:
                    Console.WriteLine("\nHasta pronto!\n");
                    break;
                default:
                    Console.WriteLine("\nOpcion incorrecta\n");
                    break;
            }
        } while (opcion != 5);

    }
}