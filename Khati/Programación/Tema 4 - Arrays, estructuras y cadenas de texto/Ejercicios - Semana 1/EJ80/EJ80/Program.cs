﻿using System;
public class EJ80
{
    static void Main()
    {
        /*
         * 80. Crea una versión mejorada del programa anterior: permitirá guardar hasta 1.000 nombres de videojuegos y un menú que 
         * permita: Añadir un nuevo dato (al final de los existentes), 
         * insertar un dato (en una cierta posición que se preguntará al usuario), 
         * borrar un dato (a partir de su número de posición), 
         * ver todos los datos, 
         * buscar un cierto dato, 
         * salir.
         */

        string[] videojuegos = new string[100];
        int opcion, cont, posicionesOcupadas, posicionAgregar, posicionBorrar;
        string nuevoJuego, juegoBuscar;
        bool encontrado;

        do
        {

            Console.WriteLine("MENU\n---------------------------------");
            Console.WriteLine("0. Agregar nuevo juego");
            Console.WriteLine("1. Insertar juego en una posición");
            Console.WriteLine("2. Borrar un juego a partir de su posicion");
            Console.WriteLine("3. Ver todos los juegos");
            Console.WriteLine("4. Buscar un juego por su nombre");
            Console.WriteLine("5. Salir");
            Console.Write("Opcion: ");
            opcion = Convert.ToInt32(Console.ReadLine());

            // Vemos cuantas posiciones hay ocupadas
            posicionesOcupadas = 0;
            foreach (string videojuego in videojuegos)
            {
                if (videojuego != null)
                {
                    posicionesOcupadas++;
                }
            }

            switch (opcion)
            {
                case 0:
                    // Agregar nuevo juego -------------------------------
                    Console.Write("\nTitulo del nuevo juego: ");
                    nuevoJuego = Console.ReadLine();
                    Console.WriteLine();
                    videojuegos[posicionesOcupadas] = nuevoJuego;
                    break;
                case 1:
                    // Insertar juego en una posicion -------------------
                    Console.Write("\nPosicion donde agregar titulo: ");
                    posicionAgregar = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Juego a insertar: ");
                    nuevoJuego = Console.ReadLine();
                    // Recorremos todas las posiciones, desde la ultima ocupada hasta la posición a agregar
                    for(int i = posicionesOcupadas; i > posicionAgregar; i--)
                    {
                        // mandamos una posición hacia atras cada valor, para así ir dejando espacio a la posicion que vamos a insertar
                        videojuegos[i] = videojuegos[i - 1];
                    }
                    // Agregamos en la posicion que queremos insertar, una vez ya se ha dejado sitio y atrasado uno cada posición anterior a esta
                    videojuegos[posicionAgregar] = nuevoJuego;
                    Console.WriteLine();
                    break;
                case 2:
                    // Borrar juego a partir de su posicion -------------
                    Console.Write("\nPosicion del juego a borrar: ");
                    posicionBorrar = Convert.ToInt32(Console.ReadLine());
                    // Recorremos desde la posicion que queremos borrar hasta la anterior de las posiciones ocupadas
                    for(int i = posicionBorrar; i < posicionesOcupadas-1; i++)
                    {
                        // Vamos avanzando una posicion cada juego hacia adelante
                        videojuegos[i] = videojuegos[i + 1];
                    }
                    // Borramos el juego de su posición, asignandolo a null
                    videojuegos[posicionBorrar] = null;
                    Console.WriteLine();
                    break;
                case 3:
                    // Ver todos los juegos -----------------------------
                    cont = 0;
                    Console.WriteLine("\nLista de juegos\n------------------");
                    foreach (string videojuego in videojuegos)
                    {
                        if(videojuego != null)
                        {
                            Console.WriteLine("{0}.  {1}", cont, videojuego);
                            cont++;
                        }                     
                    }
                    Console.WriteLine();
                    break;
                case 4:
                    // Buscar un juego por su nombre --------------------
                    Console.Write("\nJuego a buscar: ");
                    juegoBuscar = Console.ReadLine();
                    encontrado = false;
                    foreach(string videojuego in videojuegos)
                    {
                        if(videojuego == juegoBuscar)
                        {
                            encontrado = true;
                        }
                    }
                    if (encontrado)
                    {
                        Console.WriteLine("El juego \"{0}\" SI existe en la lista\n", juegoBuscar);
                    }
                    else
                    {
                        Console.WriteLine("El juego \"{0}\" NO existe en la lista\n", juegoBuscar);
                    }
                    break;
                case 5:
                    // Salir --------------------------------------------
                    Console.WriteLine("\nHasta pronto!\n");
                    break;
                default:
                    Console.WriteLine("\nOpcion incorrecta\n");
                    break;
            }
        } while (opcion != 5);
        
    }
}