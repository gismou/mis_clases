﻿using System;
public class EJ71
{
    static void Main()
    {
        /*
         * 71. Crea un nueva versión del menú (ejercicio 66), que muestra una serie de opciones
         * y después dice cuál ha escogido. En esta versión, 
         * los nombres de las opciones estarán en un array, 
         * por lo que no será necesario ningún "switch".
         * La apariencia será como ésta:

            1. Ver datos existentes
            2. Añadir un nuevo dato
            3. Buscar
            0. Salir
            1
            Ha escogido la opción "1": "Ver datos existentes"
         */

        int opcion;
        String[] opciones = { "Salir", "Ver datos existentes", "Añadir un nuevo dato", "Buscar" };

        do
        {
            Console.WriteLine("1. Ver datos existentes");
            Console.WriteLine("2. Añadir un nuevo dato");
            Console.WriteLine("3. Buscar");
            Console.WriteLine("0. Salir");
            opcion = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ha escogido la opcion \"" + opcion + "\": \"" + opciones[opcion] + "\"");
        } while (opcion != 0);
        Console.WriteLine("¡Hasta pronto!");
    }
}