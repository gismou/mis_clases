﻿using System;
public class EJ74
{
    static void Main()
    {
        /*
        Crea un programa en C# que pida al usuario el número de un mes (por ejemplo, 3 para marzo) y el número de un día (por ejemplo, 10) 
        y muestre de qué número de día dentro del año se trata, en un año no bisiesto. 
        Por ejemplo: como enero tiene 31 días y febrero 28, el 10 de marzo sería el día número 69 del año (31+28+10). 
        Debes usar un array para guardar la cantidad de días que tiene cada mes.
         */

        int mes;
        int dia;
        int[] diasMeses = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        int diasTranscurridos = 0;

        Console.Write("Intruduce un numero de mes: ");
        mes = Convert.ToInt32(Console.ReadLine());

        Console.Write("Intruduce un dia del mes elegido: ");
        dia = Convert.ToInt32(Console.ReadLine());

        // Recorremos con un for hasta el mes anterior al elegido (empezando por 0 al ser array)
        for(int i = 0; i < mes-1; i++)
        {
            diasTranscurridos += diasMeses[i];
        }

        // Una vez hemos sumado los dias de todos los meses hasta el elegido, le sumamos los días elegidos
        diasTranscurridos += dia;

        Console.WriteLine("Dias transcurridos: {0}", diasTranscurridos);
    }
}