﻿using System;
public class EJ77
{
    static void Main()
    {
        /*
        77. Crea un programa que pregunte al usuario cuántos datos (enteros largos) va a introducir, reserve espacio para todos ellos, 
        se los pida al usuario y finamente muestre los números que estén duplicados. 
        Por ejemplo, si los números son 12 23 36 23 45, la respuesta sería 
        "Duplicados: 23". 
        Si no hubiera ningún duplicado, la respuesta deberá ser "Duplicados: Ninguno". 
        Si algún dato aparece más de dos veces (por ejemplo, 12 23 36 23 45 23) puede que la respuesta sea "fea": "Duplicados: 23 23", 
        pero eso no debe preocuparte.
         */

        long numIntroducir;
        long numeroUsuario;
        long[] numeros;
        Boolean sinRepetidos = true;

        Console.Write("Numeros a introducir: ");
        numIntroducir = Convert.ToInt64(Console.ReadLine());
        numeros = new long[numIntroducir];

        for (int i=0; i<numIntroducir; i++)
        {
            Console.Write("Numero {0}: ", i + 1);
            numeroUsuario = Convert.ToInt64(Console.ReadLine());
            numeros[i] = numeroUsuario;
        }

        Console.WriteLine();

        foreach(long numero in numeros)
        {
            Console.Write("{0} ", numero);
        }

        Console.WriteLine();


        Console.Write("Duplicados: ");
        for(int i=0; i<numeros.Length;i++)
        {
            // Recorremos el array de numeros y en cada vuelta vemos si el actual está entre los
            // que recorremos del mismo array en una segunda vuelta. Si está lo ponemos en el array
            // de numerosRepetidos
            long numeroRecorrido = numeros[i];
            for(int j=i+1; j<numeros.Length;j++)
            {
                if (numeroRecorrido == numeros[j])
                {
                    Console.Write("{0} ", numeroRecorrido);
                    sinRepetidos = false;
                }
            }
        }

        if (sinRepetidos)
        {
            Console.Write("Ninguno");
        }

    }
}