﻿using System;
public class EJ79
{
    static void Main()
    {
        /*
         79. Como proyecto que irá creciendo a lo largo del curso, vamos a hacer un programa que nos permitirá gestionar una colección de videojuegos.
        Como primera aproximación, crea un array que permita almacenar hasta 100 nombres de videojuegos. 
        Deberás mostrar un menú que permita: Añadir un nuevo dato (al final de los existentes), 
        ver todos los datos, buscar un juego, salir. 
        La opción de Buscar preguntará el nombre a buscar y responderá si es parte de nuestra colección.
         */

        string[] videojuegos = new string[100];
        string juegoNuevo, juegoBuscar;
        int opcion;
        int espaciosOcupados = 0;
        Boolean encontrado;

        do
        {
            Console.WriteLine("0. Agregar juego");
            Console.WriteLine("1. Ver juegos");
            Console.WriteLine("2. Buscar juego");
            Console.WriteLine("3. Salir");
            Console.Write("Opcion: ");
            opcion = Convert.ToInt32(Console.ReadLine());

            switch (opcion)
            {
                case 0:
                    // --- Agregar juego ---
                    // vemos el total de espacios ocupados (distintos de vacio)
                    foreach (string videojuego in videojuegos)
                    {
                        if (videojuego != null)
                        {
                            espaciosOcupados++;
                        }
                    }
                    // agregamos en el siguiente espacio al ultimo ocupado
                    Console.WriteLine(espaciosOcupados);
                    Console.Write("\nNuevo juego a agregar: ");
                    juegoNuevo = Console.ReadLine();
                    videojuegos[espaciosOcupados] = juegoNuevo;
                    Console.WriteLine("\n");
                    break;
                case 1:
                    // --- Ver juegos ---
                    Console.WriteLine("\nMis Videojuegos");
                    Console.WriteLine("-------------------------");
                    foreach (string videojuego in videojuegos)
                    {
                        if (videojuego != null)
                        {
                            Console.WriteLine("- \"{0}\"", videojuego);
                        }
                    }
                    Console.WriteLine("\n");
                    break;
                case 2:
                    // --- Buscar juego ---
                    Console.Write("\nVideojuego a buscar: ");
                    juegoBuscar = Console.ReadLine();
                    encontrado = false;
                    foreach (string videojuego in videojuegos)
                    {
                        if (juegoBuscar == videojuego)
                        {
                            encontrado = true;
                        }
                    }
                    if (encontrado)
                    {
                        Console.WriteLine("El juego \"{0}\" SI existe en la lista\n", juegoBuscar);
                    }
                    else
                    {
                        Console.WriteLine("El juego \"{0}\" NO existe en la lista\n", juegoBuscar);
                    }
                    break;
                case 3:
                    Console.WriteLine("\nHasta pronto!\n");
                    break;
                default:
                    Console.WriteLine("\nOpcion no valida\n");
                    break;
            }
        } while (opcion != 3);

    }
}