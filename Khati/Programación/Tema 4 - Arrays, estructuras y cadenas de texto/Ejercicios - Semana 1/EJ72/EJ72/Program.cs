﻿using System;
public class EJ72
{
    static void Main()
    {
        /*
        72. Crea un array que contenga los nombres de los meses, prefijando sus datos entre llaves.
        Muestra todos los meses en pantalla, desde el primero (enero) hasta el último (diciembre), 
        en una misma línea y separados por espacios, usando "foreach". En la siguiente línea, muéstralos en orden inverso 
        (de diciembre a enero), empleando "for". Finalmente, 
        pide al usuario un número de mes (por ejemplo, el 3) y 
        muestra su nombre (el 2 sería febrero).
         */

        String[] meses = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
        int opcion;

        foreach (String mes in meses)
        {
            Console.Write(mes + " ");
        }

        Console.WriteLine();

        for (int i = meses.Length-1; i >= 0; i--)
        {
            Console.Write("{0} ", meses[i]);
        }

        Console.Write("\nElige un numero de mes: ");
        opcion = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Mes elegido: {0}", meses[opcion-1]);
    }
} 