﻿using System;
using System.Text;

public class EJ90
{
    /*
     *  Crea un programa que solicite al usuario una cadena y:
        - La convierta a minúsculas (almacenando el resultado en una nueva cadena, que mostrará)
        - La convierta a mayúsculas (almacenando el resultado en una nueva cadena, que mostrará)
        - Si tiene más de 5 letras, elimine la segunda letra y la tercera letra (almacenando el resultado en una nueva cadena, que mostrará)
        - Si tiene más de 4 letras, inserte "yo" después de la tercera letra (almacenando el resultado en una nueva cadena, que mostrará)
        - Reemplace todos los espacios (" ") por guiones bajos ("_", almacenando el resultado en una nueva cadena, que mostrará)
        - Elimine los espacios iniciales y finales (almacenando el resultado en otra cadena, que mostrará)
        - Divida el texto en un array de strings, usando los espacios como separadores, y muestre las subcadenas resultantes, cada una en una
          línea.
        - Reemplace todos los espacios (" ") por guiones ("-"), con la ayuda de un StringBuilder 
          (almacenando el resultado en una nueva cadena, que mostrará)
     */
    static void Main()
    {
        Console.Write("Escribe una cadena: ");
        string cadena = Console.ReadLine();
 
        // Convertir a minuscula
        string cadenaLower = cadena.ToLower();
        Console.WriteLine("\nCadena escogida: " + cadena);
        Console.WriteLine("En minusculas: "+cadenaLower);

        // Convertir a mayusculas
        string cadenaUpper = cadena.ToUpper();
        Console.WriteLine("En mayusculas: " + cadenaUpper);

        // Si tiene mas de 5 letras, quitar segunda letra y tercera
        if(cadena.Length > 5){
            string cadena5char = cadena.Remove(2, 2);
            Console.WriteLine("Mas de 5 letras: " + cadena5char);
        }

        // Si tiene mas de 4 letras, inserte yo despues de la tercera letra
        if(cadena.Length > 4)
        {
            string cadenaYo = cadena.Insert(3, " yo ");
            Console.WriteLine("Cadena 'yo': " + cadenaYo);
        }

        // Reemplazar espacios por guiones bajos
        string cadenaGuiones = cadena.Replace(" ", "_");
        Console.WriteLine("Cadena con guiones bajos: "+cadenaGuiones);

        // Eliminar espacios iniciales y al final
        string cadenaSinEspacios = cadena.Trim();
        Console.WriteLine("Cadena sin espacios iniciales ni finales: " + cadenaSinEspacios);

        // Divida el texto en un array de strings y muestre las subcadenas resultantes, cada una en unalínea.
        Console.WriteLine("Cadena dividida en array: ");
        string[] arrayCadena = cadena.Split(" ");
        foreach(string subcadena in arrayCadena)
        {
            Console.WriteLine("- "+subcadena);
        }

        // Cambiar espacios por - con stringBuilder
        StringBuilder cadenaModificada = new StringBuilder(cadena);
        for(int i=0; i<cadenaModificada.Length; i++)
        {
            if (cadenaModificada[i] == ' ')
            {
                cadenaModificada[i] = '-';
            }
        }
        Console.WriteLine("Cadena con StringBuilder: " + cadenaModificada);

    }
}