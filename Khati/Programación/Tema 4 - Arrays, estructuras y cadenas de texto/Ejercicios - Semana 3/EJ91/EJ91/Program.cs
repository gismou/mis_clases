﻿using System;
public class EJ91
{
    /*
     * Crea un programa que pida al usuario 10 números enteros largos, los guarde en un array llamados "datosOriginales", y luego:
    - Los copie a un array "datos1", ordene este array mediante intercambio directo (ya sea ascendente, descendente o burbuja) y muestre el contenido de "datos1".
    - Los copie a un array "datos2", ordene este array mediante selección directa y muestre el contenido de "datos2".
    - Los copie a un array "datos3", ordene este array mediante inserción directa y muestre el contenido de "datos3".
    - Compruebe si el dato 50 aparece en "datosOriginales", mediante búsqueda lineal.
    - Compruebe si el dato 50 aparece en "datos3", mediante búsqueda binaria.
     */
    static void Main()
    {

    }
}